/*
MySQL Data Transfer
Source Host: localhost
Source Database: nutzcode
Target Host: localhost
Target Database: nutzcode
Date: 2015/7/2 10:54:51
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for t_base_code
-- ----------------------------
CREATE TABLE `t_base_code` (
  `CODE_ID` varchar(30) NOT NULL,
  `UP_CODE_ID` varchar(30) DEFAULT NULL,
  `CODE_CODE` varchar(30) NOT NULL,
  `CODE_VAL` varchar(100) DEFAULT NULL,
  `REMARK` varchar(500) DEFAULT NULL,
  `LOCATION` int(32) NOT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(30) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `VALID` varchar(2) NOT NULL,
  PRIMARY KEY (`CODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_company
-- ----------------------------
CREATE TABLE `t_base_company` (
  `COM_ID` varchar(30) NOT NULL,
  `UP_COM_ID` varchar(30) NOT NULL,
  `COM_NAME` varchar(100) DEFAULT NULL,
  `COM_TYPE` varchar(30) DEFAULT NULL,
  `COM_TRADE` varchar(100) DEFAULT NULL,
  `ADDRESS` varchar(30) DEFAULT NULL,
  `TELEPHONE` varchar(50) DEFAULT NULL,
  `MAIL` varchar(50) DEFAULT NULL,
  `LOCATION` int(32) NOT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(30) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `VALID` varchar(2) NOT NULL,
  PRIMARY KEY (`COM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_config
-- ----------------------------
CREATE TABLE `t_base_config` (
  `CONFIG_CODE` varchar(30) NOT NULL,
  `CONFIG_NAME` varchar(50) DEFAULT NULL,
  `REMARK` varchar(100) DEFAULT NULL,
  `VALID` varchar(2) NOT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(30) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`CONFIG_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_config_val
-- ----------------------------
CREATE TABLE `t_base_config_val` (
  `CONFIG_CODE` varchar(30) NOT NULL,
  `COM_ID` varchar(30) NOT NULL,
  `CONFIG_VAL` varchar(50) DEFAULT NULL,
  `VALID` varchar(2) NOT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(30) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`CONFIG_CODE`,`COM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_file
-- ----------------------------
CREATE TABLE `t_base_file` (
  `FILE_ID` varchar(30) NOT NULL,
  `FILE_NAME` varchar(100) NOT NULL,
  `FILE_PATH` varchar(1000) NOT NULL,
  `FILE_SIZE` double(18,8) NOT NULL,
  `FILE_EXT` varchar(30) DEFAULT NULL,
  `FILE_SAVE_TYPE` varchar(30) DEFAULT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(30) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `VALID` varchar(2) NOT NULL,
  PRIMARY KEY (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_form
-- ----------------------------
CREATE TABLE `t_base_form` (
  `FORM_ID` varchar(30) NOT NULL,
  `FORM_NAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(30) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `VALID` varchar(2) NOT NULL,
  PRIMARY KEY (`FORM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_grade
-- ----------------------------
CREATE TABLE `t_base_grade` (
  `GRADE_ID` varchar(30) NOT NULL,
  `GRADE_NAME` varchar(100) NOT NULL,
  `COM_ID` varchar(30) NOT NULL,
  `LOCATION` varchar(30) NOT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(30) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `VALID` varchar(2) NOT NULL,
  PRIMARY KEY (`GRADE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_grade_res
-- ----------------------------
CREATE TABLE `t_base_grade_res` (
  `GRADE_ID` varchar(30) NOT NULL,
  `RES_ID` varchar(30) NOT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`GRADE_ID`,`RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_res
-- ----------------------------
CREATE TABLE `t_base_res` (
  `RES_ID` varchar(30) NOT NULL,
  `UP_RES_ID` varchar(30) NOT NULL,
  `RES_NAME` varchar(100) DEFAULT NULL,
  `RES_TYPE` varchar(30) DEFAULT NULL,
  `TARGET` varchar(100) DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  `SELECTOR` varchar(200) DEFAULT NULL,
  `RES_ICON` varchar(100) DEFAULT NULL,
  `LOCATION` int(32) NOT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(30) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `VALID` varchar(2) NOT NULL,
  PRIMARY KEY (`RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_user
-- ----------------------------
CREATE TABLE `t_base_user` (
  `USER_ID` varchar(30) NOT NULL,
  `USER_CODE` varchar(30) NOT NULL,
  `COM_ID` varchar(30) NOT NULL,
  `DEPT_ID` varchar(30) DEFAULT NULL,
  `PASSWORD` varchar(50) DEFAULT NULL,
  `SALE` varchar(50) DEFAULT NULL,
  `NAME` varchar(50) NOT NULL,
  `USER_JOB` varchar(50) DEFAULT NULL,
  `USER_PIC` varchar(100) DEFAULT NULL,
  `SEX` varchar(50) NOT NULL,
  `BIRTHDAY` datetime DEFAULT NULL,
  `MOBILE` varchar(50) DEFAULT NULL,
  `MAIL` varchar(50) DEFAULT NULL,
  `STATE` varchar(50) NOT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(30) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `VALID` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_user_company
-- ----------------------------
CREATE TABLE `t_base_user_company` (
  `USER_ID` varchar(30) NOT NULL,
  `COM_ID` varchar(30) NOT NULL,
  PRIMARY KEY (`USER_ID`,`COM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_base_user_grade
-- ----------------------------
CREATE TABLE `t_base_user_grade` (
  `USER_ID` varchar(30) NOT NULL,
  `GRADE_ID` varchar(30) NOT NULL,
  PRIMARY KEY (`USER_ID`,`GRADE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_code_db_config
-- ----------------------------
CREATE TABLE `t_code_db_config` (
  `DB_CONFIG_ID` varchar(50) NOT NULL,
  `CONFIG_NAME` varchar(100) DEFAULT NULL,
  `DB_TYPE` varchar(100) DEFAULT NULL,
  `DB_DRIVER` varchar(100) DEFAULT NULL,
  `DB_ADDRESS` varchar(100) DEFAULT NULL,
  `DB_USER` varchar(100) DEFAULT NULL,
  `DB_PASS` varchar(100) DEFAULT NULL,
  `VALID` varchar(2) DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`DB_CONFIG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_code_db_mapping
-- ----------------------------
CREATE TABLE `t_code_db_mapping` (
  `DB_MAP_ID` varchar(50) NOT NULL,
  `MAP_TYPE_ID` varchar(50) DEFAULT NULL,
  `COVER_NAME` varchar(50) NOT NULL,
  `COVER_TYPE` varchar(50) DEFAULT NULL,
  `COVER_IN` varchar(50) DEFAULT NULL,
  `COVER_VAL` varchar(50) DEFAULT NULL,
  `COVER_OUT` varchar(100) NOT NULL,
  `COVER_LEVEL` int(5) DEFAULT NULL,
  `VALID` varchar(2) DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`DB_MAP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_code_map_type
-- ----------------------------
CREATE TABLE `t_code_map_type` (
  `MAP_TYPE_ID` varchar(50) NOT NULL,
  `DB_TYPE` varchar(50) NOT NULL,
  `PLAN_ID` varchar(50) NOT NULL,
  `MAP_NAME` varchar(500) DEFAULT NULL,
  `MAP_TARGET` varchar(50) DEFAULT NULL,
  `VALID` varchar(2) DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`MAP_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_code_plan
-- ----------------------------
CREATE TABLE `t_code_plan` (
  `PLAN_ID` varchar(50) NOT NULL,
  `PLAN_NAME` varchar(500) NOT NULL,
  `DB_MAP` varchar(500) DEFAULT NULL,
  `PACKET_NAME` varchar(50) DEFAULT NULL,
  `GEN_TYPE` varchar(50) DEFAULT NULL,
  `SHARE_FLAG` varchar(2) DEFAULT NULL,
  `VALID` varchar(2) DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`PLAN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_code_plan_item
-- ----------------------------
CREATE TABLE `t_code_plan_item` (
  `PLAN_ITEM_ID` varchar(50) NOT NULL,
  `PLAN_ID` varchar(50) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `FILE_PATH` varchar(100) DEFAULT NULL,
  `FILE_NAME` varchar(50) DEFAULT NULL,
  `FILE_CONTEXT` text,
  `FILE_EXT` varchar(50) DEFAULT NULL,
  `MAP_TARGET` varchar(50) DEFAULT NULL,
  `VALID` varchar(2) DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`PLAN_ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_test_table
-- ----------------------------
CREATE TABLE `t_test_table` (
  `ID` varchar(30) NOT NULL,
  `CODE` varchar(30) DEFAULT NULL,
  `NAME` varchar(500) DEFAULT NULL,
  `INPUT_DATE` datetime DEFAULT NULL,
  `INPUT_DOUBLE` bigint(48) DEFAULT NULL,
  `INPUT_INT` int(32) DEFAULT NULL,
  `CREATE_BY` varchar(30) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(30) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `t_base_code` VALUES ('0001', '0000', 'comType', '部门类型', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00010001', '0001', '1', '职能部门', '', '1', '', null, '1430627530532', '2015-05-15 18:45:01', '1');
INSERT INTO `t_base_code` VALUES ('00010002', '0001', '2', '业务部门', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('0002', '0000', 'comTrade', '所属行业', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00020001', '0002', '0', 'IT', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00020002', '0002', '1', '互联网', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00020003', '0002', '2', '销售行业', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('0003', '0000', 'valid', '是否有效', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00030001', '0003', 'Y', '是', null, '0', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00030002', '0003', 'N', '否', null, '1', null, null, null, '2015-05-02 23:24:00', '1');
INSERT INTO `t_base_code` VALUES ('0004', '0000', 'sex', '性别', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00040001', '0004', '0', '男', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00040002', '0004', '1', '女', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('0005', '0000', 'userstate', '在职状态', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00050001', '0005', '0', '在职', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00050002', '0005', '1', '离职', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('0006', '0000', 'dbType', '数据库类型', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00060001', '0006', 'MYSQL', 'MYSQL', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00060002', '0006', 'ORACLE', 'ORACLE', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('0007', '0000', 'genType', '代码生成类型', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00070001', '0007', 'PDM', 'PDM生成', null, '0', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00070002', '0007', 'DB', '数据库生成', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('0008', '0000', 'mapTarget', '转换对象', null, '0', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00080001', '0008', 'column', '数据库属性', null, '0', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00080002', '0008', 'table', '针对单个表', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00080003', '0008', 'system', '针对整个系统', null, '2', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('0009', '0000', 'coverType', '转换累i系那个', null, '0', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00090001', '0009', 'EQU', '全文匹配', null, '0', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00090002', '0009', 'X2TU', '下划线转驼峰大写首字母', null, '1', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00090003', '0009', 'T2X', '驼峰转下划线', null, '2', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00090004', '0009', 'X2TL', '下划线转驼峰小写首字母', null, '4', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00090005', '0009', 'Static', '写死的值', null, '6', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00090006', '0009', 'REPLACE', '替换关键字', null, '5', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00090007', '0009', 'Upper', '转大写', null, '7', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00090008', '0009', 'Lower', '转小写', null, '8', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('00090009', '0009', 'NotNull', '不为空', null, '9', null, null, null, null, '1');
INSERT INTO `t_base_code` VALUES ('1433471094562', '0000', 'sysResType', '系统资源类型', '', '15', '1430627530532', '2015-06-05 10:24:54', '1430627530532', '2015-06-05 10:24:54', 'Y');
INSERT INTO `t_base_code` VALUES ('1433471151541', '1433471094562', 'M', '菜单', '', '0', '1430627530532', '2015-06-05 10:25:51', '1430627530532', '2015-06-05 10:25:51', 'Y');
INSERT INTO `t_base_code` VALUES ('1433471164195', '1433471094562', 'B', '按钮', '', '1', '1430627530532', '2015-06-05 00:00:00', '1430627530532', '2015-06-05 10:26:13', 'Y');
INSERT INTO `t_base_code` VALUES ('1433471343600', '0000', 'resTarget', '资源指向', '', '9', '1430627530532', '2015-06-05 10:29:03', '1430627530532', '2015-06-05 10:29:03', 'Y');
INSERT INTO `t_base_code` VALUES ('1433471559277', '1433471343600', 'navtab', 'NavTab', '新标签', '0', '1430627530532', '2015-06-05 10:32:39', '1430627530532', '2015-06-05 10:32:39', 'Y');
INSERT INTO `t_base_code` VALUES ('1433471606619', '1433471343600', 'dialog', 'DiaLog', '弹出页面', '1', '1430627530532', '2015-06-05 10:33:26', '1430627530532', '2015-06-05 10:33:26', 'Y');
INSERT INTO `t_base_code` VALUES ('1434639026258', '1434639011482', '0', '文字', '', '0', '1430627530532', '2015-06-18 22:50:26', '1430627530532', '2015-06-18 22:50:26', 'Y');
INSERT INTO `t_base_code` VALUES ('1434639036932', '1434639011482', '1', '图片', '', '1', '1430627530532', '2015-06-18 22:50:36', '1430627530532', '2015-06-18 22:50:36', 'Y');
INSERT INTO `t_base_code` VALUES ('1434646491268', '1434639011482', '2', '按钮', '', '2', '1430627530532', '2015-06-19 00:54:51', '1430627530532', '2015-06-19 00:54:51', 'Y');
INSERT INTO `t_base_code` VALUES ('1435312812239', '0000', 'diqu', '地区信息', '', '12', '1430627530532', '2015-06-26 18:00:12', '1430627530532', '2015-06-26 18:00:12', 'Y');
INSERT INTO `t_base_code` VALUES ('1435312828665', '1435312812239', 'bj', '北京', '', '0', '1430627530532', '2015-06-26 18:00:28', '1430627530532', '2015-06-26 18:00:28', 'Y');
INSERT INTO `t_base_code` VALUES ('1435312847720', '1435312828665', 'xc', '西城区', '', '0', '1430627530532', '2015-06-26 18:00:47', '1430627530532', '2015-06-26 18:00:47', 'Y');
INSERT INTO `t_base_company` VALUES ('01', '01', '总公司', '1', '0', '', '18601938265', '', '1', '', null, '1430627530532', '2015-06-05 10:11:31', 'Y');
INSERT INTO `t_base_company` VALUES ('1430625974123', '01', '测试机构1', '1', '0', '北京市海淀区', '18601938265', 'yqw8912@163.com', '1', '127.0.0.1', '2015-05-03 12:06:14', '127.0.0.1', '2015-05-03 12:06:14', 'Y');
INSERT INTO `t_base_company` VALUES ('1430633720452', '1430625974123', '测试机构11', '1', '0', '北京市海淀区', '', '', '2', '127.0.0.1', '2015-05-03 14:15:20', '127.0.0.1', '2015-05-03 14:15:20', 'Y');
INSERT INTO `t_base_company` VALUES ('1431006477246', '1430633720452', '测试机构111', '1', '0', '123456', '123456', '', '1', '1430627530532', '2015-05-07 21:47:57', '1430627530532', '2015-05-07 21:47:57', 'Y');
INSERT INTO `t_base_company` VALUES ('1433470333237', '01', '测试机构', '1', '0', '北京市', '13729817963', '', '0', '1430627530532', '2015-06-05 10:12:13', '1430627530532', '2015-06-05 10:12:13', 'Y');
INSERT INTO `t_base_config` VALUES ('data_power', '数据权限', '配置机构的数据权限', 'Y', '1430627530532', '2015-05-05 16:19:14', '1430627530532', '2015-05-05 16:19:14');
INSERT INTO `t_base_config_val` VALUES ('data_power', '01', '0', 'Y', '1430627530532', '2015-05-05 16:49:00', '1430627530532', '2015-05-05 16:49:00');
INSERT INTO `t_base_file` VALUES ('1435311978540', 'test', '1430627530532.pdm.2015.6.26.', '0.05079460', 'pdm', null, '1430627530532', '2015-06-26 17:46:18', '1430627530532', '2015-06-26 17:46:18', '1');
INSERT INTO `t_base_file` VALUES ('1435312006081', 'Nutz-Code', '1430627530532.zip.2015.6.26.', '0.00000000', 'zip', null, '1430627530532', '2015-06-26 17:46:46', '1430627530532', '2015-06-26 17:46:46', '1');
INSERT INTO `t_base_file` VALUES ('1435312170132', 'Nutz-Code', '1430627530532.zip.2015.6.26.', '0.00000000', 'zip', null, '1430627530532', '2015-06-26 17:49:30', '1430627530532', '2015-06-26 17:49:30', '1');
INSERT INTO `t_base_file` VALUES ('1435312355840', 'test', '1430627530532.pdm.2015.6.26.', '0.05079460', 'pdm', null, '1430627530532', '2015-06-26 17:52:35', '1430627530532', '2015-06-26 17:52:35', '1');
INSERT INTO `t_base_form` VALUES ('1435132909186', '123', '123', '1430627530532', '2015-06-24 16:01:49', '1430627530532', '2015-06-24 16:01:49', 'Y');
INSERT INTO `t_base_grade` VALUES ('0', '超级管理员', '01', '1', '127.0.0.1', '2015-05-03 00:00:00', '1430627530532', '2015-06-05 13:50:37', 'Y');
INSERT INTO `t_base_grade` VALUES ('1433483399759', '代码生成岗', '01', '0', '1430627530532', '2015-06-05 00:00:00', '1430627530532', '2015-06-26 18:03:54', 'Y');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '01', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1433471978563', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1433478355046', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1433478403217', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1433478471903', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1433478515406', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1433478684367', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1433478765354', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1433478796630', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1433494678232', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1435307022444', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_grade_res` VALUES ('1433483399759', '1435312955160', '1430627530532', '2015-06-26 18:03:55');
INSERT INTO `t_base_res` VALUES ('01', '0', 'NutzShop系统', '', '', '', '', null, '0', '', '2015-06-05 10:21:42', '1430627530532', '2015-06-05 10:38:01', 'Y');
INSERT INTO `t_base_res` VALUES ('1433471960228', '01', '系统设置', 'M', 'navtab', '', '', 'fa fa-cog', '11', '', '2015-06-05 10:39:20', '1430627530532', '2015-06-05 12:37:49', 'Y');
INSERT INTO `t_base_res` VALUES ('1433471978563', '01', '代码生成', '', '', '', '', 'fa fa-coffee', '9', '', '2015-06-05 10:39:38', '1430627530532', '2015-06-05 11:05:15', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478355046', '1433494678232', '用户管理', 'M', 'navtab', '/base/BaseUser', '', '', '0', '', '2015-06-05 12:25:55', '1430627530532', '2015-06-05 16:58:09', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478403217', '1433494678232', '机构管理', 'M', 'navtab', '/base/BaseCompany', '', '', '1', '', '2015-06-05 12:26:43', '1430627530532', '2015-06-05 16:58:13', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478471903', '1433494678232', '资源管理', 'M', 'navtab', '/base/BaseRes', '', '', '2', '', '2015-06-05 12:27:51', '1430627530532', '2015-06-26 16:39:12', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478515406', '1433494678232', '岗位管理', 'M', 'navtab', '/base/BaseGrade', '', '', '4', '', '2015-06-05 12:28:35', '1430627530532', '2015-06-05 16:58:18', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478562050', '1433471960228', '基础配置', 'M', 'navtab', '/base/BaseConfig', '', null, '5', '1430627530532', '2015-06-05 12:29:22', '1430627530532', '2015-06-05 12:29:22', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478607689', '1433471960228', '基础配置值', 'M', 'navtab', '/base/BaseConfigVal', '', null, '6', '1430627530532', '2015-06-05 12:30:07', '1430627530532', '2015-06-05 12:30:07', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478646053', '1433471960228', '数据权限', 'M', 'navtab', '/base/BaseUserCompany', '', null, '7', '1430627530532', '2015-06-05 12:30:46', '1430627530532', '2015-06-05 12:30:46', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478684367', '1433494678232', '数据字典', 'M', 'navtab', '/base/BaseCode', '', '', '8', '', '2015-06-05 12:31:24', '1430627530532', '2015-06-05 17:31:58', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478722318', '1433471960228', '自定义表单', 'M', 'navtab', '/base/BaseForm', '', null, '8', '1430627530532', '2015-06-05 12:32:02', '1430627530532', '2015-06-05 12:32:02', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478765354', '1433471978563', '数据库配置', 'M', 'navtab', '/code/CodeDbConfig', '', null, '0', '1430627530532', '2015-06-05 12:32:45', '1430627530532', '2015-06-05 12:32:45', 'Y');
INSERT INTO `t_base_res` VALUES ('1433478796630', '1433471978563', '代码方案', 'M', 'navtab', '/code/CodePlan', '', null, '2', '1430627530532', '2015-06-05 12:33:16', '1430627530532', '2015-06-05 12:33:16', 'Y');
INSERT INTO `t_base_res` VALUES ('1433494678232', '01', '基础设置', 'M', 'navtab', '', '', 'fa  fa-group', '10', '1430627530532', '2015-06-05 16:57:58', '1430627530532', '2015-06-05 16:57:58', 'Y');
INSERT INTO `t_base_res` VALUES ('1435307022444', '1433471978563', '测试表', 'M', 'navtab', '/test/Testable', '', '', '3', '1430627530532', '2015-06-26 16:23:42', '1430627530532', '2015-06-26 16:23:42', 'Y');
INSERT INTO `t_base_user` VALUES ('1435557001847', 'admin', '01', '01', '21232F297A57A5A743894A0E4A801FC3', '', '超级管理员', '', '', '0', null, '', '', '0', '1430627530532', '2015-06-29 13:50:01', '1430627530532', '2015-06-29 13:50:01', 'Y');
INSERT INTO `t_base_user` VALUES ('1435557101493', 'test', '01', '01', '098F6BCD4621D373CADE4E832627B4F6', '', '代码生成', 'test', '', '0', null, '', '', '0', '1435557001847', '2015-06-29 13:51:41', '1435557001847', '2015-06-29 13:51:41', 'Y');
INSERT INTO `t_base_user_company` VALUES ('1430627530532', '01');
INSERT INTO `t_base_user_company` VALUES ('1430627530532', '1430625974123');
INSERT INTO `t_base_user_company` VALUES ('1430627530532', '1430633720452');
INSERT INTO `t_base_user_company` VALUES ('1430627530532', '1431006477246');
INSERT INTO `t_base_user_company` VALUES ('1430627530532', '1433470333237');
INSERT INTO `t_base_user_company` VALUES ('1430628028162', '01');
INSERT INTO `t_base_user_grade` VALUES ('1435557001847', '0');
INSERT INTO `t_base_user_grade` VALUES ('1435557001847', '01');
INSERT INTO `t_base_user_grade` VALUES ('1435557001847', '1430625974123');
INSERT INTO `t_base_user_grade` VALUES ('1435557001847', '1433483399759');
INSERT INTO `t_base_user_grade` VALUES ('1435557101493', '1433483399759');
INSERT INTO `t_code_db_config` VALUES ('1432110295927', '合众-Core', 'ORACLE', 'oracle.jdbc.driver.OracleDriver', 'jdbc:oracle:thin:@10.129.1.66:1521:devdb', 'upiccore', 'upiccheck66', 'Y', '1430627530532', '2015-05-20 16:24:55', '1430627530532', '2015-05-20 16:24:55');
INSERT INTO `t_code_db_config` VALUES ('1432643354958', 'local_mysql', 'MYSQL', 'com.mysql.jdbc.Driver', 'jdbc:mysql://127.0.0.1:3306/nutzcode?useUnicode=true&characterEncoding=utf8', 'root', 'SinoSoft!123', 'Y', '1430627530532', '2015-05-26 00:00:00', '1430627530532', '2015-06-26 16:34:33');
INSERT INTO `t_code_db_config` VALUES ('1432689537301', '合众-Eshop', 'ORACLE', 'oracle.jdbc.driver.OracleDriver', 'jdbc:oracle:thin:@10.129.1.66:1521:devdb', 'upiceshop', 'upiceshop', 'Y', '1430627530532', '2015-05-27 09:18:57', '1430627530532', '2015-05-27 09:18:57');
INSERT INTO `t_code_db_config` VALUES ('1432779802259', 'webqueryDataSource', 'ORACLE', 'oracle.jdbc.driver.OracleDriver', 'jdbc:oracle:thin:@10.129.1.66:1521:devdb', 'upiceshop', 'upiceshop', 'Y', '1432777701604', '2015-05-28 00:00:00', '1432777701604', '2015-05-28 10:26:03');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481427', '1432191008900', '', 'EQU', '${C_DataTypeExt}', 'DATE', 'java.util.Date', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481428', '1432191008900', '', 'EQU', '${C_DataTypeExt}', 'DECIMAL', 'java.lang.Double', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481429', '1432191008900', '', 'EQU', '${C_DataTypeExt}', 'INT', 'java.lang.Integer', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481430', '1432191008900', '', 'EQU', '${C_DataTypeExt}', 'INTEGER', 'java.lang.Integer', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481431', '1432191008900', '', 'EQU', '${C_DataTypeExt}', 'NUMBER', 'java.lang.Long', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481432', '1432191008900', '', 'EQU', '${C_DataTypeExt}', 'VARCHAR', 'String', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481433', '1432191008900', '', 'EQU', '${C_DataTypeExt}', 'VARCHAR2', 'String', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481451', '1432193017556', '', 'EQU', '${C_DataTypeExt}', 'DATE', 'date', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481452', '1432193017556', '', 'EQU', '${C_DataTypeExt}', 'DECIMAL', 'doubles', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481453', '1432193017556', '', 'EQU', '${C_DataTypeExt}', 'INT', 'digits', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481454', '1432193017556', '', 'EQU', '${C_DataTypeExt}', 'INTEGER', 'digits', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481455', '1432193017556', '', 'EQU', '${C_DataTypeExt}', 'NUMBER', 'digits', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432107481461', '1432191089429', '', 'EQU', '${C_DataTypeExt}', 'DATE', 'datepicker', '0', 'Y', '1430627530532', '2015-05-20 15:38:01', '1430627530532', '2015-05-20 15:38:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432194390905', '1432191184635', 'BeanName', 'X2TU', '${T_RealCode}', '', '', '2', 'Y', '1430627530532', '2015-05-21 00:00:00', '1430627530532', '2015-05-24 12:29:37');
INSERT INTO `t_code_db_mapping` VALUES ('1432196054052', '1432195983481', 'Packet', 'Static', '', '', 'com.sinosoft', '0', 'Y', '1430627530532', '2015-05-21 00:00:00', '1430627530532', '2015-05-23 16:32:52');
INSERT INTO `t_code_db_mapping` VALUES ('1432212626997', '1432191184635', 'LowerBeanName', 'X2TL', '${T_RealCode}', '', '', '1', 'Y', '1430627530532', '2015-05-21 00:00:00', '1430627530532', '2015-05-24 12:29:31');
INSERT INTO `t_code_db_mapping` VALUES ('1432213213176', '1432195983481', 'User', 'Static', '', '', 'yangqunwei', '0', 'Y', '1430627530532', '2015-05-21 00:00:00', '1430627530532', '2015-05-23 16:32:18');
INSERT INTO `t_code_db_mapping` VALUES ('1432368776728', '1432195983481', 'TableHead', 'Static', '', '', 't_', '1', 'Y', '1430627530532', '2015-05-23 00:00:00', '1430627530532', '2015-05-24 12:28:57');
INSERT INTO `t_code_db_mapping` VALUES ('1432369087444', '1432191184635', 'RealCode', 'REPLACE', '${T_Code}', '${A_TableHead}', '', '0', 'Y', '1430627530532', '2015-05-23 00:00:00', '1430627530532', '2015-05-24 12:29:23');
INSERT INTO `t_code_db_mapping` VALUES ('1432370032124', '1432195983481', 'Modules', 'Static', '', '', 'test', '1', 'Y', '1430627530532', '2015-05-23 00:00:00', '1430627530532', '2015-06-26 16:10:10');
INSERT INTO `t_code_db_mapping` VALUES ('1432413573846', '1432195983481', 'ActionPacket', 'Static', '', '', '${A_Packet}.modules.${A_Modules}', '2', 'Y', '1430627530532', '2015-05-24 00:00:00', '1430627530532', '2015-05-24 04:47:04');
INSERT INTO `t_code_db_mapping` VALUES ('1432413972249', '1432195983481', 'DtoPacket', 'Static', '', '', '${A_Packet}.domain.${A_Modules}', '3', 'Y', '1430627530532', '2015-05-24 00:00:00', '1430627530532', '2015-05-24 04:46:56');
INSERT INTO `t_code_db_mapping` VALUES ('1432415008104', '1432195983481', 'Mail', 'Static', '', '', 'yqw8912@163.com', '0', 'Y', '1430627530532', '2015-05-24 05:03:28', '1430627530532', '2015-05-24 05:03:28');
INSERT INTO `t_code_db_mapping` VALUES ('1432417359105', '1432417233729', 'ColumnName', 'X2TU', '${C_Code}', '', '', '1', 'Y', '1430627530532', '2015-05-24 00:00:00', '1430627530532', '2015-05-24 12:30:32');
INSERT INTO `t_code_db_mapping` VALUES ('1432417393674', '1432417233729', 'LowerColumnName', 'X2TL', '${C_Code}', '', '', '1', 'Y', '1430627530532', '2015-05-24 00:00:00', '1430627530532', '2015-05-24 12:42:33');
INSERT INTO `t_code_db_mapping` VALUES ('1432441800555', '1432417233729', 'UpperCode', 'Upper', '${C_Code}', '', '', '1', 'Y', '1430627530532', '2015-05-24 00:00:00', '1430627530532', '2015-05-24 12:31:54');
INSERT INTO `t_code_db_mapping` VALUES ('1432441938699', '1432191184635', 'UpperCode', 'Upper', '${T_Code}', '', '', '0', 'Y', '1430627530532', '2015-05-24 12:32:18', '1430627530532', '2015-05-24 12:32:18');
INSERT INTO `t_code_db_mapping` VALUES ('1432450008517', '1432458507762', 'ColType', 'EQU', '${C_DataTypeExt}', 'CLOB', 'TEXT', '0', 'Y', '1430627530532', '2015-05-24 14:46:48', '1430627530532', '2015-05-24 14:46:48');
INSERT INTO `t_code_db_mapping` VALUES ('1432450039013', '1432458507762', 'ColType', 'EQU', '${C_DataTypeExt}', 'DATE', 'DATETIME', '1', 'Y', '1430627530532', '2015-05-24 14:47:19', '1430627530532', '2015-05-24 14:47:19');
INSERT INTO `t_code_db_mapping` VALUES ('1432450071072', '1432458507762', 'ColType', 'EQU', '${C_DataTypeExt}', 'DECIMAL', 'INT', '1', 'Y', '1430627530532', '2015-05-24 14:47:51', '1430627530532', '2015-05-24 14:47:51');
INSERT INTO `t_code_db_mapping` VALUES ('1432450081569', '1432458507762', 'ColType', 'EQU', '${C_DataTypeExt}', 'INT', 'INT', '1', 'Y', '1430627530532', '2015-05-24 14:48:01', '1430627530532', '2015-05-24 14:48:01');
INSERT INTO `t_code_db_mapping` VALUES ('1432450107525', '1432458507762', 'ColType', 'EQU', '${C_DataTypeExt}', 'INTEGER', 'INT', '1', 'Y', '1430627530532', '2015-05-24 14:48:27', '1430627530532', '2015-05-24 14:48:27');
INSERT INTO `t_code_db_mapping` VALUES ('1432450128330', '1432458507762', 'ColType', 'EQU', '${C_DataTypeExt}', 'NUMBER', 'INT', '1', 'Y', '1430627530532', '2015-05-24 14:48:48', '1430627530532', '2015-05-24 14:48:48');
INSERT INTO `t_code_db_mapping` VALUES ('1432450148313', '1432458507762', 'ColType', 'EQU', '${C_DataTypeExt}', 'VARCHAR', 'VARCHAR', '1', 'Y', '1430627530532', '2015-05-24 14:49:08', '1430627530532', '2015-05-24 14:49:08');
INSERT INTO `t_code_db_mapping` VALUES ('1432450168509', '1432458507762', 'ColType', 'EQU', '${C_DataTypeExt}', 'VARCHAR2', 'VARCHAR', '1', 'Y', '1430627530532', '2015-05-24 14:49:28', '1430627530532', '2015-05-24 14:49:28');
INSERT INTO `t_code_db_mapping` VALUES ('1432458941703', '1432458507762', 'width', 'NotNull', '${C_Length}', '', ', width =${C_Length}', '1', 'Y', '1430627530532', '2015-05-24 17:15:41', '1430627530532', '2015-05-24 17:15:41');
INSERT INTO `t_code_db_mapping` VALUES ('1432459024809', '1432458507762', 'notNull', 'EQU', '${C_Mandatory}', '1', ', notNull = true', '2', 'Y', '1430627530532', '2015-05-24 17:17:04', '1430627530532', '2015-05-24 17:17:04');
INSERT INTO `t_code_db_mapping` VALUES ('1432459143650', '1432458507762', 'precision', 'NotNull', '${C_Precision}', '', ',precision=8', '1', 'Y', '1430627530532', '2015-05-24 17:19:03', '1430627530532', '2015-05-24 17:19:03');
INSERT INTO `t_code_db_mapping` VALUES ('1432462241803', '1432191184635', 'PKName', 'Static', '', '', 'PK_${T_UpperCode}', '3', 'Y', '1430627530532', '2015-05-24 00:00:00', '1430627530532', '2015-05-24 18:11:33');
INSERT INTO `t_code_db_mapping` VALUES ('1432463396070', '1432193017556', 'INPUT', 'NotNull', '${C_VALID}', '', 'data-rule = \"${C_VALID}\'\"', '3', 'Y', '1430627530532', '2015-05-24 00:00:00', '1430627530532', '2015-05-24 18:41:33');
INSERT INTO `t_code_db_mapping` VALUES ('1432463488924', '1432193017556', 'INPUT', 'EQU', '${C_Mandatory}', '1', 'data-rule = \"required\"', '5', 'Y', '1430627530532', '2015-05-24 00:00:00', '1430627530532', '2015-05-24 18:42:19');
INSERT INTO `t_code_db_mapping` VALUES ('1432463568897', '1432193017556', 'MaxLength', 'NotNull', '${C_Length}', '', 'maxlength= \"${C_Length}\"', '4', 'Y', '1430627530532', '2015-05-24 00:00:00', '1430627530532', '2015-05-24 18:41:52');
INSERT INTO `t_code_db_mapping` VALUES ('1435557211168', '1435557211101', '', 'EQU', '${C_DataTypeExt}', 'DATE', 'java.util.Date', '0', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_db_mapping` VALUES ('1435557211227', '1435557211101', '', 'EQU', '${C_DataTypeExt}', 'DECIMAL', 'java.lang.Double', '0', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_db_mapping` VALUES ('1435557211307', '1435557211101', '', 'EQU', '${C_DataTypeExt}', 'INT', 'java.lang.Integer', '0', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_db_mapping` VALUES ('1435557211335', '1435557211101', '', 'EQU', '${C_DataTypeExt}', 'INTEGER', 'java.lang.Integer', '0', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_db_mapping` VALUES ('1435557211552', '1435557211101', '', 'EQU', '${C_DataTypeExt}', 'NUMBER', 'java.lang.Long', '0', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_db_mapping` VALUES ('1435557211702', '1435557211101', '', 'EQU', '${C_DataTypeExt}', 'VARCHAR', 'String', '0', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_db_mapping` VALUES ('1435557211769', '1435557211101', '', 'EQU', '${C_DataTypeExt}', 'VARCHAR2', 'String', '0', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_db_mapping` VALUES ('1435557211856', '1435557211810', '', 'EQU', '${C_DataTypeExt}', 'DATE', 'datepicker', '0', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_db_mapping` VALUES ('1435557211956', '1435557211910', 'BeanName', 'X2TU', '${T_RealCode}', '', '', '2', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212002', '1435557211910', 'LowerBeanName', 'X2TL', '${T_RealCode}', '', '', '1', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212060', '1435557211910', 'RealCode', 'REPLACE', '${T_Code}', '${A_TableHead}', '', '0', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212101', '1435557211910', 'UpperCode', 'Upper', '${T_Code}', '', '', '0', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212143', '1435557211910', 'PKName', 'Static', '', '', 'PK_${T_UpperCode}', '3', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212249', '1435557212185', '', 'EQU', '${C_DataTypeExt}', 'DATE', 'date', '0', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212310', '1435557212185', '', 'EQU', '${C_DataTypeExt}', 'DECIMAL', 'doubles', '0', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212351', '1435557212185', '', 'EQU', '${C_DataTypeExt}', 'INT', 'digits', '0', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212394', '1435557212185', '', 'EQU', '${C_DataTypeExt}', 'INTEGER', 'digits', '0', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212443', '1435557212185', '', 'EQU', '${C_DataTypeExt}', 'NUMBER', 'digits', '0', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212485', '1435557212185', 'INPUT', 'NotNull', '${C_VALID}', '', 'data-rule = \"${C_VALID}\'\"', '3', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212527', '1435557212185', 'INPUT', 'EQU', '${C_Mandatory}', '1', 'data-rule = \"required\"', '5', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212593', '1435557212185', 'MaxLength', 'NotNull', '${C_Length}', '', 'maxlength= \"${C_Length}\"', '4', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212712', '1435557212659', 'Packet', 'Static', '', '', 'com.sinosoft', '0', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212751', '1435557212659', 'User', 'Static', '', '', 'yangqunwei', '0', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212793', '1435557212659', 'TableHead', 'Static', '', '', 't_', '1', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212861', '1435557212659', 'Modules', 'Static', '', '', 'test', '1', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212910', '1435557212659', 'ActionPacket', 'Static', '', '', '${A_Packet}.modules.${A_Modules}', '2', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557212960', '1435557212659', 'DtoPacket', 'Static', '', '', '${A_Packet}.domain.${A_Modules}', '3', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_db_mapping` VALUES ('1435557213001', '1435557212659', 'Mail', 'Static', '', '', 'yqw8912@163.com', '0', 'Y', '1435557101493', '2015-06-29 13:53:33', '1435557101493', '2015-06-29 13:53:33');
INSERT INTO `t_code_map_type` VALUES ('1432191008900', 'ORACLE', '1432365957145', 'C_JAVA', 'column', 'Y', '1430627530532', '2015-05-21 00:00:00', '1430627530532', '2015-05-21 14:50:19');
INSERT INTO `t_code_map_type` VALUES ('1432191040346', 'ORACLE', '1432115188458', 'C_XML', 'column', 'Y', '1430627530532', '2015-05-21 14:50:40', '1430627530532', '2015-05-21 14:50:40');
INSERT INTO `t_code_map_type` VALUES ('1432191089429', 'ORACLE', '1432365957145', 'C_TOGGLE', 'column', 'Y', '1430627530532', '2015-05-21 14:51:29', '1430627530532', '2015-05-21 14:51:29');
INSERT INTO `t_code_map_type` VALUES ('1432191184635', '', '1432365957145', 'T', 'table', 'Y', '1430627530532', '2015-05-21 00:00:00', '1430627530532', '2015-05-22 08:31:39');
INSERT INTO `t_code_map_type` VALUES ('1432193017556', 'ORACLE', '1432365957145', 'C_VALID', 'column', 'Y', '1430627530532', '2015-05-21 15:23:37', '1430627530532', '2015-05-21 15:23:37');
INSERT INTO `t_code_map_type` VALUES ('1432195983481', '', '1432365957145', 'A', 'system', 'Y', '1430627530532', '2015-05-21 00:00:00', '1430627530532', '2015-05-23 15:12:21');
INSERT INTO `t_code_map_type` VALUES ('1432417233729', 'ORACLE', '1432365957145', 'C', 'column', 'Y', '1430627530532', '2015-05-24 05:40:33', '1430627530532', '2015-05-24 05:40:33');
INSERT INTO `t_code_map_type` VALUES ('1432458507762', 'ORACLE', '1432365957145', 'C_ColDefine', 'column', 'Y', '1430627530532', '2015-05-24 17:08:27', '1430627530532', '2015-05-24 17:08:27');
INSERT INTO `t_code_map_type` VALUES ('1432471420189', 'ORACLE', '1432471391610', 'C_XML', 'column', 'Y', '1430628028162', '2015-05-24 20:43:40', '1430628028162', '2015-05-24 20:43:40');
INSERT INTO `t_code_map_type` VALUES ('1432471448406', 'ORACLE', '1432471448013', 'C_JAVA', 'column', 'Y', '1430628028162', '2015-05-24 20:44:08', '1430628028162', '2015-05-24 20:44:08');
INSERT INTO `t_code_map_type` VALUES ('1432471448760', 'ORACLE', '1432471448013', 'C_TOGGLE', 'column', 'Y', '1430628028162', '2015-05-24 20:44:08', '1430628028162', '2015-05-24 20:44:08');
INSERT INTO `t_code_map_type` VALUES ('1432471448860', '', '1432471448013', 'T', 'table', 'Y', '1430628028162', '2015-05-24 20:44:08', '1430628028162', '2015-05-24 20:44:08');
INSERT INTO `t_code_map_type` VALUES ('1432471449144', 'ORACLE', '1432471448013', 'C_VALID', 'column', 'Y', '1430628028162', '2015-05-24 20:44:09', '1430628028162', '2015-05-24 20:44:09');
INSERT INTO `t_code_map_type` VALUES ('1432471449494', '', '1432471448013', 'A', 'system', 'Y', '1430628028162', '2015-05-24 20:44:09', '1430628028162', '2015-05-24 20:44:09');
INSERT INTO `t_code_map_type` VALUES ('1432471449802', 'ORACLE', '1432471448013', 'C', 'column', 'Y', '1430628028162', '2015-05-24 20:44:09', '1430628028162', '2015-05-24 20:44:09');
INSERT INTO `t_code_map_type` VALUES ('1432471449964', 'ORACLE', '1432471448013', 'C_ColDefine', 'column', 'Y', '1430628028162', '2015-05-24 20:44:09', '1430628028162', '2015-05-24 20:44:09');
INSERT INTO `t_code_map_type` VALUES ('1432518083568', 'ORACLE', '1432518083307', 'C_XML', 'column', 'Y', '1430627530532', '2015-05-25 09:41:23', '1430627530532', '2015-05-25 09:41:23');
INSERT INTO `t_code_map_type` VALUES ('1432518740681', 'ORACLE', '1432518740602', 'C_JAVA', 'column', 'Y', '1430627530532', '2015-05-25 09:52:20', '1430627530532', '2015-05-25 09:52:20');
INSERT INTO `t_code_map_type` VALUES ('1432518740803', '', '1432518740602', 'T', 'table', 'Y', '1430627530532', '2015-05-25 09:52:20', '1430627530532', '2015-05-25 09:52:20');
INSERT INTO `t_code_map_type` VALUES ('1432518740880', 'ORACLE', '1432518740602', 'C_VALID', 'column', 'Y', '1430627530532', '2015-05-25 09:52:20', '1430627530532', '2015-05-25 09:52:20');
INSERT INTO `t_code_map_type` VALUES ('1432518740988', '', '1432518740602', 'A', 'system', 'Y', '1430627530532', '2015-05-25 09:52:20', '1430627530532', '2015-05-25 09:52:20');
INSERT INTO `t_code_map_type` VALUES ('1432518741099', 'ORACLE', '1432518740602', 'C', 'column', 'Y', '1430627530532', '2015-05-25 09:52:21', '1430627530532', '2015-05-25 09:52:21');
INSERT INTO `t_code_map_type` VALUES ('1432779128988', 'ORACLE', '1432779128868', 'C_JAVA', 'column', 'Y', '1432777701604', '2015-05-28 10:12:08', '1432777701604', '2015-05-28 10:12:08');
INSERT INTO `t_code_map_type` VALUES ('1432779129099', '', '1432779128868', 'T', 'table', 'Y', '1432777701604', '2015-05-28 10:12:09', '1432777701604', '2015-05-28 10:12:09');
INSERT INTO `t_code_map_type` VALUES ('1432779129173', 'ORACLE', '1432779128868', 'C_VALID', 'column', 'Y', '1432777701604', '2015-05-28 10:12:09', '1432777701604', '2015-05-28 10:12:09');
INSERT INTO `t_code_map_type` VALUES ('1432779129283', '', '1432779128868', 'A', 'system', 'Y', '1432777701604', '2015-05-28 10:12:09', '1432777701604', '2015-05-28 10:12:09');
INSERT INTO `t_code_map_type` VALUES ('1432779129392', 'ORACLE', '1432779128868', 'C', 'column', 'Y', '1432777701604', '2015-05-28 10:12:09', '1432777701604', '2015-05-28 10:12:09');
INSERT INTO `t_code_map_type` VALUES ('1432791305757', 'ORACLE', '1432791305614', 'C_JAVA', 'column', 'Y', '1430627530532', '2015-05-28 13:35:05', '1430627530532', '2015-05-28 13:35:05');
INSERT INTO `t_code_map_type` VALUES ('1432791305870', '', '1432791305614', 'T', 'table', 'Y', '1430627530532', '2015-05-28 13:35:05', '1430627530532', '2015-05-28 13:35:05');
INSERT INTO `t_code_map_type` VALUES ('1432791306046', '', '1432791305614', 'A', 'system', 'Y', '1430627530532', '2015-05-28 13:35:06', '1430627530532', '2015-05-28 13:35:06');
INSERT INTO `t_code_map_type` VALUES ('1432791306132', 'ORACLE', '1432791305614', 'C', 'column', 'Y', '1430627530532', '2015-05-28 13:35:06', '1430627530532', '2015-05-28 13:35:06');
INSERT INTO `t_code_map_type` VALUES ('1435557211101', 'ORACLE', '1435557210738', 'C_JAVA', 'column', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_map_type` VALUES ('1435557211810', 'ORACLE', '1435557210738', 'C_TOGGLE', 'column', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_map_type` VALUES ('1435557211910', '', '1435557210738', 'T', 'table', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_map_type` VALUES ('1435557212185', 'ORACLE', '1435557210738', 'C_VALID', 'column', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_map_type` VALUES ('1435557212659', '', '1435557210738', 'A', 'system', 'Y', '1435557101493', '2015-06-29 13:53:32', '1435557101493', '2015-06-29 13:53:32');
INSERT INTO `t_code_map_type` VALUES ('1435557213049', 'ORACLE', '1435557210738', 'C', 'column', 'Y', '1435557101493', '2015-06-29 13:53:33', '1435557101493', '2015-06-29 13:53:33');
INSERT INTO `t_code_map_type` VALUES ('1435557213096', 'ORACLE', '1435557210738', 'C_ColDefine', 'column', 'Y', '127.0.0.1', '2015-06-29 13:53:33', '127.0.0.1', '2015-06-29 13:53:33');
INSERT INTO `t_code_plan` VALUES ('1432365957145', 'Nutz-Code', '1432191008900,1432191089429,1432191184635,1432193017556,1432195983481,1432417233729,1432458507762', null, 'PDM', 'Y', 'Y', '1435557001847', '2015-05-23 00:00:00', '1430627530532', '2015-06-24 15:49:34');
INSERT INTO `t_code_plan` VALUES ('1435557210738', 'Nutz-Code', '1432191008900,1432191089429,1432191184635,1432193017556,1432195983481,1432417233729,1432458507762', null, 'PDM', 'Y', 'Y', '1435557101493', '2015-06-29 13:53:30', '1435557101493', '2015-06-29 13:53:30');
INSERT INTO `t_code_plan_item` VALUES ('1432366012722', '1432365957145', 'Action', 'src.${A_ActionPacket}', '${T_BeanName}Action', 'package ${A_ActionPacket};\r\n\r\nimport java.util.List;\r\nimport javax.servlet.http.HttpServletRequest;\r\nimport javax.servlet.http.HttpServletResponse;\r\nimport org.nutz.ioc.loader.annotation.IocBean;\r\nimport org.nutz.mvc.annotation.At;\r\nimport org.nutz.mvc.annotation.By;\r\nimport org.nutz.mvc.annotation.Filters;\r\nimport org.nutz.mvc.annotation.Ok;\r\nimport org.nutz.mvc.annotation.Param;\r\nimport com.sinosoft.common.action.BaseAction;\r\nimport com.sinosoft.common.bean.BaseCondition;\r\nimport com.sinosoft.common.web.filter.BaseFilter;\r\nimport com.sinosoft.common.web.filter.UserLoginFilter;\r\nimport ${A_DtoPacket}.${T_BeanName};\r\n\r\n/**\r\n * @author ${A_User}\r\n * @mail ${A_Mail}\r\n * @time  ${A_CurrentTime} \r\n * ${T_Name}Action\r\n */\r\n@IocBean\r\n@At(\"/${A_Modules}/${T_BeanName}\")\r\n@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})\r\npublic class ${T_BeanName}Action extends BaseAction {\r\n	//查询列表\r\n	@At(\"\")\r\n	@Ok(\"jsp:jsp.${A_Modules}.list${T_BeanName}\")\r\n	public void list(@Param(\"..\")BaseCondition baseCondition,@Param(\"..\")${T_BeanName} ${T_LowerBeanName},HttpServletRequest req) {\r\n		dao.create(${T_BeanName}.class, false);\r\n		List<${T_BeanName}> ${T_LowerBeanName}List = daoUtil.queryList(${T_LowerBeanName},baseCondition);\r\n		req.setAttribute(\"${T_LowerBeanName}List\", ${T_LowerBeanName}List);//查询结果返回页面 \r\n		req.setAttribute(\"baseCondition\", baseCondition);//总记录数返回页面\r\n		req.setAttribute(\"${T_LowerBeanName}\", ${T_LowerBeanName});//返回查询条件鞭面页面跳转后查询条件丢失\r\n	}\r\n	//准备新增\r\n	@At\r\n	@Ok(\"jsp:jsp.${A_Modules}.edit${T_BeanName}\")\r\n	public void toadd(HttpServletRequest req) {\r\n		req.setAttribute(\"${T_LowerBeanName}\", new ${T_BeanName}());\r\n		req.setAttribute(\"actionType\", \"add\");\r\n	}	\r\n	/**\r\n	 * 新增数据\r\n	 * @param req\r\n	 */\r\n	@At\r\n	public void add(HttpServletRequest req,HttpServletResponse response,@Param(\"..\")${T_BeanName} ${T_LowerBeanName}) {\r\n		try {\r\n			daoUtil.add(${T_LowerBeanName});\r\n		} catch (Exception e) {\r\n			e.printStackTrace();\r\n			ajaxDoneError(response,e.getMessage());		\r\n		}	\r\n		ajaxDoneSuccess(response,successMessage,true);\r\n	}	\r\n	//准备更新\r\n	@At\r\n	@Ok(\"jsp:jsp.${A_Modules}.edit${T_BeanName}\")\r\n	public void toedit(HttpServletRequest req,@Param(\"id\") String id) {\r\n		req.setAttribute(\"${T_LowerBeanName}\",daoUtil.detailByPK(${T_BeanName}.class,id));\r\n		req.setAttribute(\"actionType\", \"edit\");\r\n	}\r\n	/**\r\n	 * 新增数据\r\n	 * @param req\r\n	 */\r\n	@At	\r\n	public void edit(HttpServletRequest req,HttpServletResponse response,@Param(\"..\")${T_BeanName} ${T_LowerBeanName}) {\r\n		try {\r\n			daoUtil.update(${T_LowerBeanName});\r\n		} catch (Exception e) {\r\n			e.printStackTrace();\r\n			ajaxDoneError(response,e.getMessage());		\r\n		}	\r\n		ajaxDoneSuccess(response,successMessage,true);\r\n	}\r\n	//批量删除\r\n	@At	\r\n	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param(\"ids\") String[] idArray) {\r\n		try {\r\n			for(int i=0;i<idArray.length;i++){\r\n				daoUtil.deleteByPK(${T_BeanName}.class,idArray[i]);\r\n			}\r\n		} catch (Exception e) {\r\n			e.printStackTrace();\r\n			ajaxDoneError(response,e.getMessage());		\r\n		}		\r\n		ajaxDoneSuccess(response,successMessage);\r\n	}\r\n	//批量删除\r\n	@At	\r\n	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param(\"id\") String id) {\r\n		try {\r\n				daoUtil.deleteByPK(${T_BeanName}.class,id);\r\n		} catch (Exception e) {\r\n			e.printStackTrace();\r\n			ajaxDoneError(response,e.getMessage());		\r\n		}		\r\n		ajaxDoneSuccess(response,successMessage);\r\n	}	\r\n	@At	\r\n	@Ok(\"jsp:jsp.${A_Modules}.edit${T_BeanName}\")\r\n	public void view(HttpServletRequest req,@Param(\"id\") String id) {\r\n		req.setAttribute(\"${T_LowerBeanName}\",daoUtil.detailByPK(${T_BeanName}.class,id));\r\n		req.setAttribute(\"actionType\", \"view\");\r\n	}\r\n}', 'java', 'table', 'Y', '1430627530532', '2015-05-23 00:00:00', '1430627530532', '2015-05-24 05:12:46');
INSERT INTO `t_code_plan_item` VALUES ('1432366039822', '1432365957145', 'Dto', 'src.${A_DtoPacket}', '${T_BeanName}', 'package ${A_DtoPacket};\r\n\r\nimport org.nutz.dao.entity.annotation.Table;\r\nimport org.nutz.dao.entity.annotation.PK;\r\nimport org.nutz.dao.entity.annotation.Column;\r\nimport org.nutz.dao.entity.annotation.ColDefine;\r\nimport org.nutz.dao.entity.annotation.ColType;\r\nimport com.sinosoft.common.bean.BaseDto;\r\n/**\r\n * @author ${A_User}\r\n * @mail ${A_Mail}\r\n * @time  ${A_CurrentTime} \r\n * ${T_Name}javaBean\r\n */\r\n@Table(\"${T_UpperCode}\")\r\n@PK(name=\"${T_PKName}\",value={<#list T_KeyList  as key>\"${key.C_LowerColumnName}\"<#if key_has_next>,</#if></#list>})\r\npublic class ${T_BeanName} extends BaseDto {\r\n	private static final long serialVersionUID = 1L;\r\n	<#list T_ColumnList  as colum>\r\n	/**\r\n	*${colum.C_Name}\r\n	*/	\r\n	@Column(\"${colum.C_UpperCode}\")\r\n	@ColDefine(type = ColType.${colum.C_ColDefine_ColType}${colum.C_ColDefine_width}${colum.C_ColDefine_precision}${colum.C_ColDefine_notNull})\r\n	private ${colum.C_JAVA} ${colum.C_LowerColumnName} ;\r\n	</#list>\r\n	\r\n	<#list T_ColumnList  as colum>\r\n	/**\r\n	* ${colum.C_Name}\r\n	*/	\r\n	public ${colum.C_JAVA} get${colum.C_ColumnName}() {\r\n		return ${colum.C_LowerColumnName};\r\n	}\r\n	/**\r\n	* ${colum.C_Name}\r\n	*/	\r\n	public void set${colum.C_ColumnName}(${colum.C_JAVA} ${colum.C_LowerColumnName}) {\r\n		this.${colum.C_LowerColumnName} = ${colum.C_LowerColumnName};\r\n	} \r\n	</#list>	\r\n}\r\n', 'java', 'table', 'Y', '1430627530532', '2015-05-23 00:00:00', '1430627530532', '2015-05-24 18:12:26');
INSERT INTO `t_code_plan_item` VALUES ('1432366067502', '1432365957145', 'jsp_edit', 'WebRoot.WEB-INF.jsp.${A_Modules}', 'edit${T_BeanName}', '<%@ page language=\"java\" import=\"java.util.*\" pageEncoding=\"UTF-8\"%>\r\n<%@ include file=\"/common/jsp/taglibs.jsp\"%>\r\n<div class=\"bjui-pageContent\">\r\n    <form action=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/${\"$\"}{actionType}\" id=\"${\"$\"}{actionType}_${T_BeanName}_form\" data-toggle=\"validate\" data-alertmsg=\"false\" >   	\r\n        <table class=\"table table-condensed table-hover\" width=\"100%\">\r\n            <tbody>\r\n                <tr>\r\n					<#list T_ColumnList  as colum>\r\n                   <td>                   		\r\n                        <label for=\"${colum.C_LowerColumnName}\" class=\"control-label x85\">${colum.C_Name}:</label>                       \r\n                        <#if colum.C_DataType==\'DATE\'>\r\n                        <input type=\"text\" name=\"${colum.C_LowerColumnName}\" id=\"${colum.C_LowerColumnName}\" value=\'<fmt:formatDate value=\"${\"$\"}{${T_LowerBeanName}.${colum.C_LowerColumnName}}\"  pattern=\"yyyy-MM-dd\"></fmt:formatDate>\' data-toggle=\"${colum.C_TOGGLE}\" ${colum.inputType} size=\"15\">\r\n                        <#else>\r\n                        <input type=\"text\" name=\"${colum.C_LowerColumnName}\" id=\"${colum.C_LowerColumnName}\" value=\"${\"$\"}{${T_LowerBeanName}.${colum.C_LowerColumnName}}\" ${colum.C_VALID_INPUT} ${colum.C_VALID_MaxLength} size=\"15\">                        \r\n                        </#if>\r\n                   </td>\r\n                        <#if (colum_index+1)%4==0>                        \r\n                </tr>\r\n                <tr>\r\n                	    </#if>                       \r\n					</#list>\r\n                </tr> \r\n            </tbody>\r\n        </table>\r\n    </form>\r\n</div>\r\n<div class=\"bjui-pageFooter\">\r\n    <ul>\r\n    	<c:if test=\"${\"$\"}{actionType==\'view\'}\">\r\n   	        <li><button type=\"button\" class=\"btn-close\" data-icon=\"close\">关闭</button></li>\r\n<script type=\"text/javascript\">\r\n${\"$\"}(function(){   \r\n	readOnlyAll(\"${\"$\"}{actionType}_${T_BeanName}_form\");\r\n}); \r\n</script> 	\r\n    	</c:if>\r\n    	<c:if test=\"${\"$\"}{actionType!=\'view\'}\">\r\n   	        <li><button type=\"button\" class=\"btn-close\" data-icon=\"close\">取消</button></li>\r\n	        <li><button type=\"submit\" class=\"btn-default\" data-icon=\"save\">保存</button></li>\r\n    	</c:if>\r\n\r\n    </ul>\r\n</div>', 'jsp', 'table', 'Y', '1430627530532', '2015-05-23 00:00:00', '1430627530532', '2015-05-24 19:26:23');
INSERT INTO `t_code_plan_item` VALUES ('1432366088546', '1432365957145', 'jsp_list', 'WebRoot.WEB-INF.jsp.${A_Modules}', 'list${T_BeanName}', '<%@ page language=\"java\" import=\"java.util.*\" pageEncoding=\"UTF-8\"%>\r\n<%@ include file=\"/common/jsp/taglibs.jsp\"%>\r\n<div class=\"bjui-pageHeader\">\r\n    <form id=\"pagerForm\" data-toggle=\"ajaxsearch\" action=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}\" method=\"post\">\r\n    <jsp:include page=\"/common/jsp/pageinput.jsp\"></jsp:include>\r\n        <div class=\"bjui-searchBar\">\r\n					<#list T_ColumnList  as colum>\r\n                        <label for=\"${colum.C_LowerColumnName}\" class=\"control-label x85\">${colum.C_Name}:</label>                       \r\n                        <#if colum.C_DataType==\'DATE\'>\r\n                        <input type=\"text\" name=\"${colum.C_LowerColumnName}\" id=\"${colum.C_LowerColumnName}\" data-toggle=\"${colum.C_TOGGLE}\"  value=\'<fmt:formatDate value=\"${\"$\"}{${T_LowerBeanName}.${colum.C_LowerColumnName}}\"  pattern=\"yyyy-MM-dd\"></fmt:formatDate>\' size=\"15\">\r\n                        <#else>\r\n                        <input type=\"text\" name=\"${colum.C_LowerColumnName}\" id=\"${colum.C_LowerColumnName}\" value=\"${\"$\"}{${T_LowerBeanName}.${colum.C_LowerColumnName}}\"  size=\"15\">                        \r\n                        </#if>\r\n                        <#if (colum_index+1)%4==0> \r\n                        <br/>\r\n                	    </#if>              \r\n					</#list>		\r\n            <button type=\"submit\" class=\"btn-default\" data-icon=\"search\">${\"查询\"}</button>         \r\n	        <a href=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/toadd\" class=\"btn btn-green\" data-icon=\"plus\" data-toggle=\"navtab\" data-id=\"add_${T_BeanName}\" data-title=\"新增${T_Name}\">新增 </a>            \r\n            <div class=\"pull-right\">\r\n                <div class=\"btn-group\">\r\n                    <button type=\"button\" class=\"btn-default dropdown-toggle\" data-toggle=\"dropdown\" data-icon=\"copy\">批量操作<span class=\"caret\"></span></button>\r\n                    <ul class=\"dropdown-menu right\" role=\"menu\">\r\n                        <li><a href=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/deletes\" data-toggle=\"doajaxchecked\" data-confirm-msg=\"确定要删除所选项\" data-idname=\"ids\" data-group=\"${T_PKName}\">${\"删除选中\"}</a></li>\r\n                    </ul>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>\r\n<div class=\"bjui-pageContent\">\r\n    <table data-toggle=\"tablefixed\" data-width=\"100%\" data-nowrap=\"true\">\r\n        <thead>\r\n            <tr>\r\n                <th width=\"26\"><input type=\"checkbox\" class=\"checkboxCtrl\" data-group=\"${T_PKName}\" data-toggle=\"icheck\"></th>\r\n				<#list T_ColumnList  as colum>\r\n				     <th >${colum.C_Name}</th>         \r\n				</#list>\r\n                <th width=\"150\">操作</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n        	<c:forEach var=\"item\" items=\"${\"$\"}{${T_LowerBeanName}List}\" varStatus=\"status\">\r\n	            <tr data-id=\"${\"$\"}{status.current}\">\r\n	                <td><input type=\"checkbox\" name=\"${T_PKName}\" data-toggle=\"icheck\" value=\"${\"$\"}{item.PK}\"></td>\r\n					<#list T_ColumnList  as colum>\r\n                   <td>                   		                   \r\n                        <#if colum.C_DataType==\'DATE\'>\r\n                        <fmt:formatDate value=\"${\"$\"}{item.${colum.C_LowerColumnName}}\"  pattern=\"yyyy-MM-dd\"></fmt:formatDate>\r\n                        <#else>\r\n                        ${\"$\"}{item.${colum.C_LowerColumnName}}                        \r\n                        </#if>\r\n                   </td>                      \r\n					</#list>\r\n	                <td>\r\n	                  <a href=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/delete?id=${\"$\"}{item.PK}\" class=\"btn btn-red\" data-icon=\"trash-o\" data-toggle=\"doajax\" data-confirm-msg=\"确定要删除该记录?\"></a>\r\n	                  <a href=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/toedit?id=${\"$\"}{item.PK}\" class=\"btn btn-green\" data-icon=\"pencil\" data-toggle=\"navtab\" data-id=\"edit_${T_BeanName}\" data-reload-warn=\"本页已有打开的内容，确定将刷新本页内容，是否继续?\" data-title=\"编辑${T_Name}\"></a>\r\n	                    <a href=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/view?id=${\"$\"}{item.PK}\" class=\"btn btn-green\" data-icon=\"search-plus\" data-toggle=\"navtab\" data-id=\"view_${T_BeanName}\" data-reload-warn=\"本页已有打开的内容，确定将刷新本页内容，是否继续?\" data-title=\"查看${T_Name}\"></a>	                  	  \r\n	                </td>\r\n	            </tr>        		\r\n        	</c:forEach>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n<jsp:include page=\"/common/jsp/pageinfo.jsp\"></jsp:include>\r\n', 'jsp', 'table', 'Y', '1430627530532', '2015-05-23 00:00:00', '1430627530532', '2015-05-24 19:32:27');
INSERT INTO `t_code_plan_item` VALUES ('1435557210812', '1435557210738', 'Action', 'src.${A_ActionPacket}', '${T_BeanName}Action', 'package ${A_ActionPacket};\r\n\r\nimport java.util.List;\r\nimport javax.servlet.http.HttpServletRequest;\r\nimport javax.servlet.http.HttpServletResponse;\r\nimport org.nutz.ioc.loader.annotation.IocBean;\r\nimport org.nutz.mvc.annotation.At;\r\nimport org.nutz.mvc.annotation.By;\r\nimport org.nutz.mvc.annotation.Filters;\r\nimport org.nutz.mvc.annotation.Ok;\r\nimport org.nutz.mvc.annotation.Param;\r\nimport com.sinosoft.common.action.BaseAction;\r\nimport com.sinosoft.common.bean.BaseCondition;\r\nimport com.sinosoft.common.web.filter.BaseFilter;\r\nimport com.sinosoft.common.web.filter.UserLoginFilter;\r\nimport ${A_DtoPacket}.${T_BeanName};\r\n\r\n/**\r\n * @author ${A_User}\r\n * @mail ${A_Mail}\r\n * @time  ${A_CurrentTime} \r\n * ${T_Name}Action\r\n */\r\n@IocBean\r\n@At(\"/${A_Modules}/${T_BeanName}\")\r\n@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})\r\npublic class ${T_BeanName}Action extends BaseAction {\r\n	//查询列表\r\n	@At(\"\")\r\n	@Ok(\"jsp:jsp.${A_Modules}.list${T_BeanName}\")\r\n	public void list(@Param(\"..\")BaseCondition baseCondition,@Param(\"..\")${T_BeanName} ${T_LowerBeanName},HttpServletRequest req) {\r\n		dao.create(${T_BeanName}.class, false);\r\n		List<${T_BeanName}> ${T_LowerBeanName}List = daoUtil.queryList(${T_LowerBeanName},baseCondition);\r\n		req.setAttribute(\"${T_LowerBeanName}List\", ${T_LowerBeanName}List);//查询结果返回页面 \r\n		req.setAttribute(\"baseCondition\", baseCondition);//总记录数返回页面\r\n		req.setAttribute(\"${T_LowerBeanName}\", ${T_LowerBeanName});//返回查询条件鞭面页面跳转后查询条件丢失\r\n	}\r\n	//准备新增\r\n	@At\r\n	@Ok(\"jsp:jsp.${A_Modules}.edit${T_BeanName}\")\r\n	public void toadd(HttpServletRequest req) {\r\n		req.setAttribute(\"${T_LowerBeanName}\", new ${T_BeanName}());\r\n		req.setAttribute(\"actionType\", \"add\");\r\n	}	\r\n	/**\r\n	 * 新增数据\r\n	 * @param req\r\n	 */\r\n	@At\r\n	public void add(HttpServletRequest req,HttpServletResponse response,@Param(\"..\")${T_BeanName} ${T_LowerBeanName}) {\r\n		try {\r\n			daoUtil.add(${T_LowerBeanName});\r\n		} catch (Exception e) {\r\n			e.printStackTrace();\r\n			ajaxDoneError(response,e.getMessage());		\r\n		}	\r\n		ajaxDoneSuccess(response,successMessage,true);\r\n	}	\r\n	//准备更新\r\n	@At\r\n	@Ok(\"jsp:jsp.${A_Modules}.edit${T_BeanName}\")\r\n	public void toedit(HttpServletRequest req,@Param(\"id\") String id) {\r\n		req.setAttribute(\"${T_LowerBeanName}\",daoUtil.detailByPK(${T_BeanName}.class,id));\r\n		req.setAttribute(\"actionType\", \"edit\");\r\n	}\r\n	/**\r\n	 * 新增数据\r\n	 * @param req\r\n	 */\r\n	@At	\r\n	public void edit(HttpServletRequest req,HttpServletResponse response,@Param(\"..\")${T_BeanName} ${T_LowerBeanName}) {\r\n		try {\r\n			daoUtil.update(${T_LowerBeanName});\r\n		} catch (Exception e) {\r\n			e.printStackTrace();\r\n			ajaxDoneError(response,e.getMessage());		\r\n		}	\r\n		ajaxDoneSuccess(response,successMessage,true);\r\n	}\r\n	//批量删除\r\n	@At	\r\n	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param(\"ids\") String[] idArray) {\r\n		try {\r\n			for(int i=0;i<idArray.length;i++){\r\n				daoUtil.deleteByPK(${T_BeanName}.class,idArray[i]);\r\n			}\r\n		} catch (Exception e) {\r\n			e.printStackTrace();\r\n			ajaxDoneError(response,e.getMessage());		\r\n		}		\r\n		ajaxDoneSuccess(response,successMessage);\r\n	}\r\n	//批量删除\r\n	@At	\r\n	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param(\"id\") String id) {\r\n		try {\r\n				daoUtil.deleteByPK(${T_BeanName}.class,id);\r\n		} catch (Exception e) {\r\n			e.printStackTrace();\r\n			ajaxDoneError(response,e.getMessage());		\r\n		}		\r\n		ajaxDoneSuccess(response,successMessage);\r\n	}	\r\n	@At	\r\n	@Ok(\"jsp:jsp.${A_Modules}.edit${T_BeanName}\")\r\n	public void view(HttpServletRequest req,@Param(\"id\") String id) {\r\n		req.setAttribute(\"${T_LowerBeanName}\",daoUtil.detailByPK(${T_BeanName}.class,id));\r\n		req.setAttribute(\"actionType\", \"view\");\r\n	}\r\n}', 'java', 'table', 'Y', '1435557101493', '2015-06-29 13:53:30', '1435557101493', '2015-06-29 13:53:30');
INSERT INTO `t_code_plan_item` VALUES ('1435557210951', '1435557210738', 'Dto', 'src.${A_DtoPacket}', '${T_BeanName}', 'package ${A_DtoPacket};\r\n\r\nimport org.nutz.dao.entity.annotation.Table;\r\nimport org.nutz.dao.entity.annotation.PK;\r\nimport org.nutz.dao.entity.annotation.Column;\r\nimport org.nutz.dao.entity.annotation.ColDefine;\r\nimport org.nutz.dao.entity.annotation.ColType;\r\nimport com.sinosoft.common.bean.BaseDto;\r\n/**\r\n * @author ${A_User}\r\n * @mail ${A_Mail}\r\n * @time  ${A_CurrentTime} \r\n * ${T_Name}javaBean\r\n */\r\n@Table(\"${T_UpperCode}\")\r\n@PK(name=\"${T_PKName}\",value={<#list T_KeyList  as key>\"${key.C_LowerColumnName}\"<#if key_has_next>,</#if></#list>})\r\npublic class ${T_BeanName} extends BaseDto {\r\n	private static final long serialVersionUID = 1L;\r\n	<#list T_ColumnList  as colum>\r\n	/**\r\n	*${colum.C_Name}\r\n	*/	\r\n	@Column(\"${colum.C_UpperCode}\")\r\n	@ColDefine(type = ColType.${colum.C_ColDefine_ColType}${colum.C_ColDefine_width}${colum.C_ColDefine_precision}${colum.C_ColDefine_notNull})\r\n	private ${colum.C_JAVA} ${colum.C_LowerColumnName} ;\r\n	</#list>\r\n	\r\n	<#list T_ColumnList  as colum>\r\n	/**\r\n	* ${colum.C_Name}\r\n	*/	\r\n	public ${colum.C_JAVA} get${colum.C_ColumnName}() {\r\n		return ${colum.C_LowerColumnName};\r\n	}\r\n	/**\r\n	* ${colum.C_Name}\r\n	*/	\r\n	public void set${colum.C_ColumnName}(${colum.C_JAVA} ${colum.C_LowerColumnName}) {\r\n		this.${colum.C_LowerColumnName} = ${colum.C_LowerColumnName};\r\n	} \r\n	</#list>	\r\n}\r\n', 'java', 'table', 'Y', '1435557101493', '2015-06-29 13:53:30', '1435557101493', '2015-06-29 13:53:30');
INSERT INTO `t_code_plan_item` VALUES ('1435557211001', '1435557210738', 'jsp_edit', 'WebRoot.WEB-INF.jsp.${A_Modules}', 'edit${T_BeanName}', '<%@ page language=\"java\" import=\"java.util.*\" pageEncoding=\"UTF-8\"%>\r\n<%@ include file=\"/common/jsp/taglibs.jsp\"%>\r\n<div class=\"bjui-pageContent\">\r\n    <form action=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/${\"$\"}{actionType}\" id=\"${\"$\"}{actionType}_${T_BeanName}_form\" data-toggle=\"validate\" data-alertmsg=\"false\" >   	\r\n        <table class=\"table table-condensed table-hover\" width=\"100%\">\r\n            <tbody>\r\n                <tr>\r\n					<#list T_ColumnList  as colum>\r\n                   <td>                   		\r\n                        <label for=\"${colum.C_LowerColumnName}\" class=\"control-label x85\">${colum.C_Name}:</label>                       \r\n                        <#if colum.C_DataType==\'DATE\'>\r\n                        <input type=\"text\" name=\"${colum.C_LowerColumnName}\" id=\"${colum.C_LowerColumnName}\" value=\'<fmt:formatDate value=\"${\"$\"}{${T_LowerBeanName}.${colum.C_LowerColumnName}}\"  pattern=\"yyyy-MM-dd\"></fmt:formatDate>\' data-toggle=\"${colum.C_TOGGLE}\" ${colum.inputType} size=\"15\">\r\n                        <#else>\r\n                        <input type=\"text\" name=\"${colum.C_LowerColumnName}\" id=\"${colum.C_LowerColumnName}\" value=\"${\"$\"}{${T_LowerBeanName}.${colum.C_LowerColumnName}}\" ${colum.C_VALID_INPUT} ${colum.C_VALID_MaxLength} size=\"15\">                        \r\n                        </#if>\r\n                   </td>\r\n                        <#if (colum_index+1)%4==0>                        \r\n                </tr>\r\n                <tr>\r\n                	    </#if>                       \r\n					</#list>\r\n                </tr> \r\n            </tbody>\r\n        </table>\r\n    </form>\r\n</div>\r\n<div class=\"bjui-pageFooter\">\r\n    <ul>\r\n    	<c:if test=\"${\"$\"}{actionType==\'view\'}\">\r\n   	        <li><button type=\"button\" class=\"btn-close\" data-icon=\"close\">关闭</button></li>\r\n<script type=\"text/javascript\">\r\n${\"$\"}(function(){   \r\n	readOnlyAll(\"${\"$\"}{actionType}_${T_BeanName}_form\");\r\n}); \r\n</script> 	\r\n    	</c:if>\r\n    	<c:if test=\"${\"$\"}{actionType!=\'view\'}\">\r\n   	        <li><button type=\"button\" class=\"btn-close\" data-icon=\"close\">取消</button></li>\r\n	        <li><button type=\"submit\" class=\"btn-default\" data-icon=\"save\">保存</button></li>\r\n    	</c:if>\r\n\r\n    </ul>\r\n</div>', 'jsp', 'table', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_code_plan_item` VALUES ('1435557211051', '1435557210738', 'jsp_list', 'WebRoot.WEB-INF.jsp.${A_Modules}', 'list${T_BeanName}', '<%@ page language=\"java\" import=\"java.util.*\" pageEncoding=\"UTF-8\"%>\r\n<%@ include file=\"/common/jsp/taglibs.jsp\"%>\r\n<div class=\"bjui-pageHeader\">\r\n    <form id=\"pagerForm\" data-toggle=\"ajaxsearch\" action=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}\" method=\"post\">\r\n    <jsp:include page=\"/common/jsp/pageinput.jsp\"></jsp:include>\r\n        <div class=\"bjui-searchBar\">\r\n					<#list T_ColumnList  as colum>\r\n                        <label for=\"${colum.C_LowerColumnName}\" class=\"control-label x85\">${colum.C_Name}:</label>                       \r\n                        <#if colum.C_DataType==\'DATE\'>\r\n                        <input type=\"text\" name=\"${colum.C_LowerColumnName}\" id=\"${colum.C_LowerColumnName}\" data-toggle=\"${colum.C_TOGGLE}\"  value=\'<fmt:formatDate value=\"${\"$\"}{${T_LowerBeanName}.${colum.C_LowerColumnName}}\"  pattern=\"yyyy-MM-dd\"></fmt:formatDate>\' size=\"15\">\r\n                        <#else>\r\n                        <input type=\"text\" name=\"${colum.C_LowerColumnName}\" id=\"${colum.C_LowerColumnName}\" value=\"${\"$\"}{${T_LowerBeanName}.${colum.C_LowerColumnName}}\"  size=\"15\">                        \r\n                        </#if>\r\n                        <#if (colum_index+1)%4==0> \r\n                        <br/>\r\n                	    </#if>              \r\n					</#list>		\r\n            <button type=\"submit\" class=\"btn-default\" data-icon=\"search\">${\"查询\"}</button>         \r\n	        <a href=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/toadd\" class=\"btn btn-green\" data-icon=\"plus\" data-toggle=\"navtab\" data-id=\"add_${T_BeanName}\" data-title=\"新增${T_Name}\">新增 </a>            \r\n            <div class=\"pull-right\">\r\n                <div class=\"btn-group\">\r\n                    <button type=\"button\" class=\"btn-default dropdown-toggle\" data-toggle=\"dropdown\" data-icon=\"copy\">批量操作<span class=\"caret\"></span></button>\r\n                    <ul class=\"dropdown-menu right\" role=\"menu\">\r\n                        <li><a href=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/deletes\" data-toggle=\"doajaxchecked\" data-confirm-msg=\"确定要删除所选项\" data-idname=\"ids\" data-group=\"${T_PKName}\">${\"删除选中\"}</a></li>\r\n                    </ul>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>\r\n<div class=\"bjui-pageContent\">\r\n    <table data-toggle=\"tablefixed\" data-width=\"100%\" data-nowrap=\"true\">\r\n        <thead>\r\n            <tr>\r\n                <th width=\"26\"><input type=\"checkbox\" class=\"checkboxCtrl\" data-group=\"${T_PKName}\" data-toggle=\"icheck\"></th>\r\n				<#list T_ColumnList  as colum>\r\n				     <th >${colum.C_Name}</th>         \r\n				</#list>\r\n                <th width=\"150\">操作</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n        	<c:forEach var=\"item\" items=\"${\"$\"}{${T_LowerBeanName}List}\" varStatus=\"status\">\r\n	            <tr data-id=\"${\"$\"}{status.current}\">\r\n	                <td><input type=\"checkbox\" name=\"${T_PKName}\" data-toggle=\"icheck\" value=\"${\"$\"}{item.PK}\"></td>\r\n					<#list T_ColumnList  as colum>\r\n                   <td>                   		                   \r\n                        <#if colum.C_DataType==\'DATE\'>\r\n                        <fmt:formatDate value=\"${\"$\"}{item.${colum.C_LowerColumnName}}\"  pattern=\"yyyy-MM-dd\"></fmt:formatDate>\r\n                        <#else>\r\n                        ${\"$\"}{item.${colum.C_LowerColumnName}}                        \r\n                        </#if>\r\n                   </td>                      \r\n					</#list>\r\n	                <td>\r\n	                  <a href=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/delete?id=${\"$\"}{item.PK}\" class=\"btn btn-red\" data-icon=\"trash-o\" data-toggle=\"doajax\" data-confirm-msg=\"确定要删除该记录?\"></a>\r\n	                  <a href=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/toedit?id=${\"$\"}{item.PK}\" class=\"btn btn-green\" data-icon=\"pencil\" data-toggle=\"navtab\" data-id=\"edit_${T_BeanName}\" data-reload-warn=\"本页已有打开的内容，确定将刷新本页内容，是否继续?\" data-title=\"编辑${T_Name}\"></a>\r\n	                    <a href=\"${\"$\"}{base}/${A_Modules}/${T_BeanName}/view?id=${\"$\"}{item.PK}\" class=\"btn btn-green\" data-icon=\"search-plus\" data-toggle=\"navtab\" data-id=\"view_${T_BeanName}\" data-reload-warn=\"本页已有打开的内容，确定将刷新本页内容，是否继续?\" data-title=\"查看${T_Name}\"></a>	                  	  \r\n	                </td>\r\n	            </tr>        		\r\n        	</c:forEach>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n<jsp:include page=\"/common/jsp/pageinfo.jsp\"></jsp:include>\r\n', 'jsp', 'table', 'Y', '1435557101493', '2015-06-29 13:53:31', '1435557101493', '2015-06-29 13:53:31');
INSERT INTO `t_test_table` VALUES ('12', '123123', '12', '2015-06-15 00:00:00', '1312', '123', '1430627530532', '2015-06-26 00:00:00', '1430627530532', '2015-06-26 16:25:05');
INSERT INTO `t_test_table` VALUES ('123', '123', '123', '2015-06-17 00:00:00', '123123', '123', '1430627530532', '2015-06-26 17:51:17', '1430627530532', '2015-06-26 17:51:17');
