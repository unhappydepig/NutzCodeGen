package com.sinosoft.domain.test;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-06-26 16:14:34.115 
 * 测试表javaBean
 */
@Table("T_TEST_TABLE")
@PK(name="PK_T_TEST_TABLE",value={"id"})
public class Testable extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*主键
	*/	
	@Column("ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String id ;
	/**
	*代码
	*/	
	@Column("CODE")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String code ;
	/**
	*名称
	*/	
	@Column("NAME")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String name ;
	/**
	*日期
	*/	
	@Column("INPUT_DATE")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date inputDate ;
	/**
	*金额
	*/	
	@Column("INPUT_DOUBLE")
	@ColDefine(type = ColType.INT, width =12,precision=8)
	private java.lang.Double inputDouble ;
	/**
	*整数
	*/	
	@Column("INPUT_INT")
	@ColDefine(type = ColType.INT)
	private java.lang.Integer inputInt ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	
	/**
	* 主键
	*/	
	public String getId() {
		return id;
	}
	/**
	* 主键
	*/	
	public void setId(String id) {
		this.id = id;
	} 
	/**
	* 代码
	*/	
	public String getCode() {
		return code;
	}
	/**
	* 代码
	*/	
	public void setCode(String code) {
		this.code = code;
	} 
	/**
	* 名称
	*/	
	public String getName() {
		return name;
	}
	/**
	* 名称
	*/	
	public void setName(String name) {
		this.name = name;
	} 
	/**
	* 日期
	*/	
	public java.util.Date getInputDate() {
		return inputDate;
	}
	/**
	* 日期
	*/	
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	} 
	/**
	* 金额
	*/	
	public java.lang.Double getInputDouble() {
		return inputDouble;
	}
	/**
	* 金额
	*/	
	public void setInputDouble(java.lang.Double inputDouble) {
		this.inputDouble = inputDouble;
	} 
	/**
	* 整数
	*/	
	public java.lang.Integer getInputInt() {
		return inputInt;
	}
	/**
	* 整数
	*/	
	public void setInputInt(java.lang.Integer inputInt) {
		this.inputInt = inputInt;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
}
