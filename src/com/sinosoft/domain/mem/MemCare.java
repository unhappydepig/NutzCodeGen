package com.sinosoft.domain.mem;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-04-29 23:07:49 
 * 会员关怀-t_mem_care javaBean
 */
@Table("T_MEM_CARE")
@PK(name="PK_T_MEM_CARE",value={"careId"})
public class MemCare extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*关怀ID
	*/	
	@Column("CARE_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String careId ;
	/**
	*会员ID
	*/	
	@Column("MEM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String memId ;
	/**
	*关怀渠道
	*/	
	@Column("CARE_TYPE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String careType ;
	/**
	*渠道值mail-phone
	*/	
	@Column("CARE_VAL")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String careVal ;
	/**
	*时间
	*/	
	@Column("CARE_TIME")
	@ColDefine(type = ColType.DATETIME, notNull = true)
	private java.util.Date careTime ;
	/**
	*内容
	*/	
	@Column("CONTEXT")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String context ;
	/**
	*员工
	*/	
	@Column("USER_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String userId ;
	/**
	*机构代码
	*/	
	@Column("COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String comId ;
	/**
	*描述
	*/	
	@Column("DESCR")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String descr ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	
	/**
	* 关怀ID
	*/	
	public String getCareId() {
		return careId;
	}
	/**
	* 关怀ID
	*/	
	public void setCareId(String careId) {
		this.careId = careId;
	} 
	/**
	* 会员ID
	*/	
	public String getMemId() {
		return memId;
	}
	/**
	* 会员ID
	*/	
	public void setMemId(String memId) {
		this.memId = memId;
	} 
	/**
	* 关怀渠道
	*/	
	public String getCareType() {
		return careType;
	}
	/**
	* 关怀渠道
	*/	
	public void setCareType(String careType) {
		this.careType = careType;
	} 
	/**
	* 渠道值mail-phone
	*/	
	public String getCareVal() {
		return careVal;
	}
	/**
	* 渠道值mail-phone
	*/	
	public void setCareVal(String careVal) {
		this.careVal = careVal;
	} 
	/**
	* 时间
	*/	
	public java.util.Date getCareTime() {
		return careTime;
	}
	/**
	* 时间
	*/	
	public void setCareTime(java.util.Date careTime) {
		this.careTime = careTime;
	} 
	/**
	* 内容
	*/	
	public String getContext() {
		return context;
	}
	/**
	* 内容
	*/	
	public void setContext(String context) {
		this.context = context;
	} 
	/**
	* 员工
	*/	
	public String getUserId() {
		return userId;
	}
	/**
	* 员工
	*/	
	public void setUserId(String userId) {
		this.userId = userId;
	} 
	/**
	* 机构代码
	*/	
	public String getComId() {
		return comId;
	}
	/**
	* 机构代码
	*/	
	public void setComId(String comId) {
		this.comId = comId;
	} 
	/**
	* 描述
	*/	
	public String getDescr() {
		return descr;
	}
	/**
	* 描述
	*/	
	public void setDescr(String descr) {
		this.descr = descr;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
}
