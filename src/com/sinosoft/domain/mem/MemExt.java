package com.sinosoft.domain.mem;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-04-29 23:07:49 
 * 会员扩展信息-t_mem_ext javaBean
 */
@Table("T_MEM_EXT")
@PK(name="PK_T_MEM_EXT",value={"memId","attriId"})
public class MemExt extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*会员ID
	*/	
	@Column("MEM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String memId ;
	/**
	*扩展属性ID
	*/	
	@Column("ATTRI_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String attriId ;
	/**
	*扩展属性值
	*/	
	@Column("ATTRI_VAL")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String attriVal ;
	
	/**
	* 会员ID
	*/	
	public String getMemId() {
		return memId;
	}
	/**
	* 会员ID
	*/	
	public void setMemId(String memId) {
		this.memId = memId;
	} 
	/**
	* 扩展属性ID
	*/	
	public String getAttriId() {
		return attriId;
	}
	/**
	* 扩展属性ID
	*/	
	public void setAttriId(String attriId) {
		this.attriId = attriId;
	} 
	/**
	* 扩展属性值
	*/	
	public String getAttriVal() {
		return attriVal;
	}
	/**
	* 扩展属性值
	*/	
	public void setAttriVal(String attriVal) {
		this.attriVal = attriVal;
	} 
}
