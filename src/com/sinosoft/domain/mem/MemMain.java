package com.sinosoft.domain.mem;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-04-29 23:07:49 
 * 会员信息表-t_mem_main javaBean
 */
@Table("T_MEM_MAIN")
@PK(name="PK_T_MEM_MAIN",value={"memId"})
public class MemMain extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*会员ID
	*/	
	@Column("MEM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String memId ;
	/**
	*会员代码
	*/	
	@Column("MEM_CODE")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String memCode ;
	/**
	*机构代码
	*/	
	@Column("COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String comId ;
	/**
	*名称
	*/	
	@Column("NAME")
	@ColDefine(type = ColType.VARCHAR, width =100, notNull = true)
	private String name ;
	/**
	*会员等级
	*/	
	@Column("MEM_LEVEL_ID")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String memLevelId ;
	/**
	*性别
	*/	
	@Column("SEX")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String sex ;
	/**
	*生日
	*/	
	@Column("BIRTHDAY")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date birthday ;
	/**
	*手机
	*/	
	@Column("MOBILE")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String mobile ;
	/**
	*邮箱
	*/	
	@Column("MAIL")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String mail ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2, notNull = true)
	private String valid ;
	
	/**
	* 会员ID
	*/	
	public String getMemId() {
		return memId;
	}
	/**
	* 会员ID
	*/	
	public void setMemId(String memId) {
		this.memId = memId;
	} 
	/**
	* 会员代码
	*/	
	public String getMemCode() {
		return memCode;
	}
	/**
	* 会员代码
	*/	
	public void setMemCode(String memCode) {
		this.memCode = memCode;
	} 
	/**
	* 机构代码
	*/	
	public String getComId() {
		return comId;
	}
	/**
	* 机构代码
	*/	
	public void setComId(String comId) {
		this.comId = comId;
	} 
	/**
	* 名称
	*/	
	public String getName() {
		return name;
	}
	/**
	* 名称
	*/	
	public void setName(String name) {
		this.name = name;
	} 
	/**
	* 会员等级
	*/	
	public String getMemLevelId() {
		return memLevelId;
	}
	/**
	* 会员等级
	*/	
	public void setMemLevelId(String memLevelId) {
		this.memLevelId = memLevelId;
	} 
	/**
	* 性别
	*/	
	public String getSex() {
		return sex;
	}
	/**
	* 性别
	*/	
	public void setSex(String sex) {
		this.sex = sex;
	} 
	/**
	* 生日
	*/	
	public java.util.Date getBirthday() {
		return birthday;
	}
	/**
	* 生日
	*/	
	public void setBirthday(java.util.Date birthday) {
		this.birthday = birthday;
	} 
	/**
	* 手机
	*/	
	public String getMobile() {
		return mobile;
	}
	/**
	* 手机
	*/	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	} 
	/**
	* 邮箱
	*/	
	public String getMail() {
		return mail;
	}
	/**
	* 邮箱
	*/	
	public void setMail(String mail) {
		this.mail = mail;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
}
