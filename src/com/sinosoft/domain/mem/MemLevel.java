package com.sinosoft.domain.mem;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-04-29 23:07:49 
 * 会员等级信息表-t_mem_level javaBean
 */
@Table("T_MEM_LEVEL")
@PK(name="PK_T_MEM_LEVEL",value={"memLevelId"})
public class MemLevel extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*等级代码
	*/	
	@Column("MEM_LEVEL_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String memLevelId ;
	/**
	*机构代码
	*/	
	@Column("COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String comId ;
	/**
	*会员等级
	*/	
	@Column("LEVEL_CODE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String levelCode ;
	/**
	*等级名称
	*/	
	@Column("NAME")
	@ColDefine(type = ColType.VARCHAR, width =100, notNull = true)
	private String name ;
	/**
	*等级折扣
	*/	
	@Column("DISCOUNT")
	@ColDefine(type = ColType.INT)
	private java.lang.Integer discount ;
	/**
	*升级条件
	*/	
	@Column("UP_CONTION")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String upContion ;
	/**
	*条件数值
	*/	
	@Column("UP_VAR")
	@ColDefine(type = ColType.INT, width =18, precision =2)
	private java.lang.Double upVar ;
	/**
	*等级描述
	*/	
	@Column("DESCR")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String descr ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2)
	private String valid ;
	
	/**
	* 等级代码
	*/	
	public String getMemLevelId() {
		return memLevelId;
	}
	/**
	* 等级代码
	*/	
	public void setMemLevelId(String memLevelId) {
		this.memLevelId = memLevelId;
	} 
	/**
	* 机构代码
	*/	
	public String getComId() {
		return comId;
	}
	/**
	* 机构代码
	*/	
	public void setComId(String comId) {
		this.comId = comId;
	} 
	/**
	* 会员等级
	*/	
	public String getLevelCode() {
		return levelCode;
	}
	/**
	* 会员等级
	*/	
	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	} 
	/**
	* 等级名称
	*/	
	public String getName() {
		return name;
	}
	/**
	* 等级名称
	*/	
	public void setName(String name) {
		this.name = name;
	} 
	/**
	* 等级折扣
	*/	
	public java.lang.Integer getDiscount() {
		return discount;
	}
	/**
	* 等级折扣
	*/	
	public void setDiscount(java.lang.Integer discount) {
		this.discount = discount;
	} 
	/**
	* 升级条件
	*/	
	public String getUpContion() {
		return upContion;
	}
	/**
	* 升级条件
	*/	
	public void setUpContion(String upContion) {
		this.upContion = upContion;
	} 
	/**
	* 条件数值
	*/	
	public java.lang.Double getUpVar() {
		return upVar;
	}
	/**
	* 条件数值
	*/	
	public void setUpVar(java.lang.Double upVar) {
		this.upVar = upVar;
	} 
	/**
	* 等级描述
	*/	
	public String getDescr() {
		return descr;
	}
	/**
	* 等级描述
	*/	
	public void setDescr(String descr) {
		this.descr = descr;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
}
