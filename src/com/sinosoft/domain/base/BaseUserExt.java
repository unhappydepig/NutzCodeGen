package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:26 
 * 员工扩展信息-t_base_user_ext javaBean
 */
@Table("T_BASE_USER_EXT")
@PK(name="PK_T_BASE_USER_EXT",value={"userId","attriId"})
public class BaseUserExt extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*会员ID
	*/	
	@Column("USER_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String userId ;
	/**
	*扩展属性ID2
	*/	
	@Column("ATTRI_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String attriId ;
	/**
	*扩展属性值2
	*/	
	@Column("ATTRI_VAL")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String attriVal ;
	
	/**
	* 会员ID
	*/	
	public String getUserId() {
		return userId;
	}
	/**
	* 会员ID
	*/	
	public void setUserId(String userId) {
		this.userId = userId;
	} 
	/**
	* 扩展属性ID2
	*/	
	public String getAttriId() {
		return attriId;
	}
	/**
	* 扩展属性ID2
	*/	
	public void setAttriId(String attriId) {
		this.attriId = attriId;
	} 
	/**
	* 扩展属性值2
	*/	
	public String getAttriVal() {
		return attriVal;
	}
	/**
	* 扩展属性值2
	*/	
	public void setAttriVal(String attriVal) {
		this.attriVal = attriVal;
	} 
}
