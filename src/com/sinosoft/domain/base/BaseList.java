package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:25 
 * 自定义查询结果类型-t_base_list javaBean
 */
@Table("T_BASE_LIST")
@PK(name="PK_T_BASE_LIST",value={"listId"})
public class BaseList extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*列表模板ID
	*/	
	@Column("LIST_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String listId ;
	/**
	*列表名称
	*/	
	@Column("FORM_NAME")
	@ColDefine(type = ColType.VARCHAR, width =100, notNull = true)
	private String formName ;
	/**
	*关联表单ID
	*/	
	@Column("FORM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String formId ;
	/**
	*描述描述
	*/	
	@Column("DESCRIPTION")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String description ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2, notNull = true)
	private String valid ;
	
	/**
	* 列表模板ID
	*/	
	public String getListId() {
		return listId;
	}
	/**
	* 列表模板ID
	*/	
	public void setListId(String listId) {
		this.listId = listId;
	} 
	/**
	* 列表名称
	*/	
	public String getFormName() {
		return formName;
	}
	/**
	* 列表名称
	*/	
	public void setFormName(String formName) {
		this.formName = formName;
	} 
	/**
	* 关联表单ID
	*/	
	public String getFormId() {
		return formId;
	}
	/**
	* 关联表单ID
	*/	
	public void setFormId(String formId) {
		this.formId = formId;
	} 
	/**
	* 描述描述
	*/	
	public String getDescription() {
		return description;
	}
	/**
	* 描述描述
	*/	
	public void setDescription(String description) {
		this.description = description;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
}
