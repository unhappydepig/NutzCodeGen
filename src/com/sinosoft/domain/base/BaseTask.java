package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:24 
 * 功能代码-t_base_task javaBean
 */
@Table("T_BASE_TASK")
@PK(name="PK_T_BASE_TASK",value={"taskId"})
public class BaseTask extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*主键
	*/	
	@Column("TASK_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String taskId ;
	/**
	*名称
	*/	
	@Column("TAKS_NAME")
	@ColDefine(type = ColType.VARCHAR, width =100, notNull = true)
	private String taksName ;
	/**
	*地址
	*/	
	@Column("URL")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String url ;
	/**
	*指向地址
	*/	
	@Column("TARGET")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String target ;
	/**
	*顺序号
	*/	
	@Column("LOCATION")
	@ColDefine(type = ColType.INT)
	private java.lang.Integer location ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2, notNull = true)
	private String valid ;
	
	/**
	* 主键
	*/	
	public String getTaskId() {
		return taskId;
	}
	/**
	* 主键
	*/	
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	} 
	/**
	* 名称
	*/	
	public String getTaksName() {
		return taksName;
	}
	/**
	* 名称
	*/	
	public void setTaksName(String taksName) {
		this.taksName = taksName;
	} 
	/**
	* 地址
	*/	
	public String getUrl() {
		return url;
	}
	/**
	* 地址
	*/	
	public void setUrl(String url) {
		this.url = url;
	} 
	/**
	* 指向地址
	*/	
	public String getTarget() {
		return target;
	}
	/**
	* 指向地址
	*/	
	public void setTarget(String target) {
		this.target = target;
	} 
	/**
	* 顺序号
	*/	
	public java.lang.Integer getLocation() {
		return location;
	}
	/**
	* 顺序号
	*/	
	public void setLocation(java.lang.Integer location) {
		this.location = location;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
}
