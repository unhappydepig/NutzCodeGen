package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Table;

import com.sinosoft.common.bean.BaseDto;
import com.sinosoft.common.util.FileUtil;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-26 15:14:15.070 
 * 系统文件信息javaBean
 */
@Table("T_BASE_FILE")
@PK(name="PK_T_BASE_FILE",value={"fileId"})
public class BaseFile extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*主键
	*/	
	@Column("FILE_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String fileId ;
	/**
	*名称
	*/	
	@Column("FILE_NAME")
	@ColDefine(type = ColType.VARCHAR, width =100, notNull = true)
	private String fileName ;
	/**
	*地址
	*/	
	@Column("FILE_PATH")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String filePath ;
	/**
	*指向地址
	*/	
	@Column("FILE_SIZE")
	@ColDefine(type = ColType.INT, width =18,precision=8, notNull = true)
	private java.lang.Double fileSize ;
	/**
	*扩展名
	*/	
	@Column("FILE_EXT")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String fileExt ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2, notNull = true)
	private String valid ;
	
	/**
	* 主键
	*/	
	public String getFileId() {
		return fileId;
	}
	/**
	* 主键
	*/	
	public void setFileId(String fileId) {
		this.fileId = fileId;
	} 
	/**
	* 名称
	*/	
	public String getFileName() {
		return fileName;
	}
	/**
	* 名称
	*/	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	} 
	/**
	* 地址
	*/	
	public String getFilePath() {
		return filePath;
	}
	/**
	* 地址
	*/	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	} 
	/**
	* 指向地址
	*/	
	public java.lang.Double getFileSize() {
		return fileSize;
	}
	/**
	* 指向地址
	*/	
	public void setFileSize(java.lang.Double fileSize) {
		this.fileSize = fileSize;
	} 
	/**
	* 扩展名
	*/	
	public String getFileExt() {
		return fileExt;
	}
	/**
	* 扩展名
	*/	
	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
	/**
	 * 文件对应的物理路径
	 */
	private String realFilePath;

	public String getRealFilePath() {
        String fname = this.getFileId() + "." + this.getFileExt(); 
        String realPath = FileUtil.getFilePath(this.getFilePath())+fname;		
		return realPath;		
	}
	
}
