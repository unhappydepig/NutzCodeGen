package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:26 
 * 员工岗位-t_base_user_grade javaBean
 */
@Table("T_BASE_USER_GRADE")
@PK(name="PK_T_BASE_USER_GRADE",value={"userId","gradeId"})
public class BaseUserGrade extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*员工ID
	*/	
	@Column("USER_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String userId ;
	/**
	*岗位ID
	*/	
	@Column("GRADE_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String gradeId ;
	
	/**
	* 员工ID
	*/	
	public String getUserId() {
		return userId;
	}
	/**
	* 员工ID
	*/	
	public void setUserId(String userId) {
		this.userId = userId;
	} 
	/**
	* 岗位ID
	*/	
	public String getGradeId() {
		return gradeId;
	}
	/**
	* 岗位ID
	*/	
	public void setGradeId(String gradeId) {
		this.gradeId = gradeId;
	} 
}
