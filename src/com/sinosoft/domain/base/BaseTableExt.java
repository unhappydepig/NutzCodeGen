package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:25 
 * 数据增强表t_base_table_ext javaBean
 */
@Table("T_BASE_TABLE_EXT")
@PK(name="PK_T_BASE_TABLE_EXT",value={"extId"})
public class BaseTableExt extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*扩展属性id
	*/	
	@Column("EXT_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String extId ;
	/**
	*机构代码
	*/	
	@Column("COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String comId ;
	/**
	*主表名
	*/	
	@Column("TABLE_CODE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String tableCode ;
	/**
	*扩展表名
	*/	
	@Column("EXT_TABLE_CODE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String extTableCode ;
	/**
	*属性名称
	*/	
	@Column("ATTR_NAME")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String attrName ;
	/**
	*数据类型
	*/	
	@Column("DATA_TYPE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String dataType ;
	/**
	*是否必填
	*/	
	@Column("REQ_FLAG")
	@ColDefine(type = ColType.VARCHAR, width =2)
	private String reqFlag ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2, notNull = true)
	private String valid ;
	
	/**
	* 扩展属性id
	*/	
	public String getExtId() {
		return extId;
	}
	/**
	* 扩展属性id
	*/	
	public void setExtId(String extId) {
		this.extId = extId;
	} 
	/**
	* 机构代码
	*/	
	public String getComId() {
		return comId;
	}
	/**
	* 机构代码
	*/	
	public void setComId(String comId) {
		this.comId = comId;
	} 
	/**
	* 主表名
	*/	
	public String getTableCode() {
		return tableCode;
	}
	/**
	* 主表名
	*/	
	public void setTableCode(String tableCode) {
		this.tableCode = tableCode;
	} 
	/**
	* 扩展表名
	*/	
	public String getExtTableCode() {
		return extTableCode;
	}
	/**
	* 扩展表名
	*/	
	public void setExtTableCode(String extTableCode) {
		this.extTableCode = extTableCode;
	} 
	/**
	* 属性名称
	*/	
	public String getAttrName() {
		return attrName;
	}
	/**
	* 属性名称
	*/	
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	} 
	/**
	* 数据类型
	*/	
	public String getDataType() {
		return dataType;
	}
	/**
	* 数据类型
	*/	
	public void setDataType(String dataType) {
		this.dataType = dataType;
	} 
	/**
	* 是否必填
	*/	
	public String getReqFlag() {
		return reqFlag;
	}
	/**
	* 是否必填
	*/	
	public void setReqFlag(String reqFlag) {
		this.reqFlag = reqFlag;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
}
