package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:25 
 * 自定义表单-t_base_form_item javaBean
 */
@Table("T_BASE_FORM_ITEM")
@PK(name="PK_T_BASE_FORM_ITEM",value={"formItemId"})
public class BaseFormItem extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*表单明细关联表主键
	*/	
	@Column("FORM_ITEM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String formItemId ;
	/**
	*表单
	*/	
	@Column("FORM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String formId ;
	/**
	*机构代码
	*/	
	@Column("COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String comId ;
	/**
	*用户代码
	*/	
	@Column("USER_ID")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String userId ;
	/**
	*属性代码
	*/	
	@Column("ATTR_CODE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String attrCode ;
	/**
	*显示名称
	*/	
	@Column("ATTR_NAME")
	@ColDefine(type = ColType.VARCHAR, width =100, notNull = true)
	private String attrName ;
	/**
	*数据类型
	*/	
	@Column("DATA_TYPE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String dataType ;
	/**
	*是否可编辑
	*/	
	@Column("EDIT_FLAG")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String editFlag ;
	/**
	*显示状态
	*/	
	@Column("SHOW_FLAG")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String showFlag ;
	/**
	*是否必填
	*/	
	@Column("REQ_FLAG")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String reqFlag ;
	/**
	*顺序号
	*/	
	@Column("LOCATION")
	@ColDefine(type = ColType.INT)
	private java.lang.Integer location ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	
	/**
	* 表单明细关联表主键
	*/	
	public String getFormItemId() {
		return formItemId;
	}
	/**
	* 表单明细关联表主键
	*/	
	public void setFormItemId(String formItemId) {
		this.formItemId = formItemId;
	} 
	/**
	* 表单
	*/	
	public String getFormId() {
		return formId;
	}
	/**
	* 表单
	*/	
	public void setFormId(String formId) {
		this.formId = formId;
	} 
	/**
	* 机构代码
	*/	
	public String getComId() {
		return comId;
	}
	/**
	* 机构代码
	*/	
	public void setComId(String comId) {
		this.comId = comId;
	} 
	/**
	* 用户代码
	*/	
	public String getUserId() {
		return userId;
	}
	/**
	* 用户代码
	*/	
	public void setUserId(String userId) {
		this.userId = userId;
	} 
	/**
	* 属性代码
	*/	
	public String getAttrCode() {
		return attrCode;
	}
	/**
	* 属性代码
	*/	
	public void setAttrCode(String attrCode) {
		this.attrCode = attrCode;
	} 
	/**
	* 显示名称
	*/	
	public String getAttrName() {
		return attrName;
	}
	/**
	* 显示名称
	*/	
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	} 
	/**
	* 数据类型
	*/	
	public String getDataType() {
		return dataType;
	}
	/**
	* 数据类型
	*/	
	public void setDataType(String dataType) {
		this.dataType = dataType;
	} 
	/**
	* 是否可编辑
	*/	
	public String getEditFlag() {
		return editFlag;
	}
	/**
	* 是否可编辑
	*/	
	public void setEditFlag(String editFlag) {
		this.editFlag = editFlag;
	} 
	/**
	* 显示状态
	*/	
	public String getShowFlag() {
		return showFlag;
	}
	/**
	* 显示状态
	*/	
	public void setShowFlag(String showFlag) {
		this.showFlag = showFlag;
	} 
	/**
	* 是否必填
	*/	
	public String getReqFlag() {
		return reqFlag;
	}
	/**
	* 是否必填
	*/	
	public void setReqFlag(String reqFlag) {
		this.reqFlag = reqFlag;
	} 
	/**
	* 顺序号
	*/	
	public java.lang.Integer getLocation() {
		return location;
	}
	/**
	* 顺序号
	*/	
	public void setLocation(java.lang.Integer location) {
		this.location = location;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
}
