package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-06-05 09:56:59.578 
 * 岗位资源表javaBean
 */
@Table("T_BASE_GRADE_RES")
@PK(name="PK_T_BASE_GRADE_RES",value={"gradeId","resId"})
public class BaseGradeRes extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*岗位ID
	*/	
	@Column("GRADE_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String gradeId ;
	/**
	*资源ID
	*/	
	@Column("RES_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String resId ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	
	/**
	* 岗位ID
	*/	
	public String getGradeId() {
		return gradeId;
	}
	/**
	* 岗位ID
	*/	
	public void setGradeId(String gradeId) {
		this.gradeId = gradeId;
	} 
	/**
	* 资源ID
	*/	
	public String getResId() {
		return resId;
	}
	/**
	* 资源ID
	*/	
	public void setResId(String resId) {
		this.resId = resId;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
}
