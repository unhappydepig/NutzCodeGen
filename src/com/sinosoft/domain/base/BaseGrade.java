package com.sinosoft.domain.base;

import java.util.ArrayList;
import java.util.List;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:25 
 * 岗位信息表-t_base_grade javaBean
 */
@Table("T_BASE_GRADE")
@PK(name="PK_T_BASE_GRADE",value={"gradeId"})
public class BaseGrade extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*主键
	*/	
	@Column("GRADE_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String gradeId ;
	/**
	*岗位名称
	*/	
	@Column("GRADE_NAME")
	@ColDefine(type = ColType.VARCHAR, width =100, notNull = true)
	private String gradeName ;
	/**
	*机构代码
	*/	
	@Column("COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String comId ;
	/**
	*顺序号
	*/	
	@Column("LOCATION")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String location ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2, notNull = true)
	private String valid ;
	/**
	 * 资源代码
	 */
	private String resCode;
	/**
	 * 资源名称
	 */
	private String resVal;
	
	/**
	 * 岗位资源列表
	 */
	private List<BaseRes> resList = new ArrayList<BaseRes>();
	
	/**
	* 主键
	*/	
	public String getGradeId() {
		return gradeId;
	}
	/**
	* 主键
	*/	
	public void setGradeId(String gradeId) {
		this.gradeId = gradeId;
	} 
	/**
	* 岗位名称
	*/	
	public String getGradeName() {
		return gradeName;
	}
	/**
	* 岗位名称
	*/	
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	} 
	/**
	* 机构代码
	*/	
	public String getComId() {
		return comId;
	}
	/**
	* 机构代码
	*/	
	public void setComId(String comId) {
		this.comId = comId;
	} 
	/**
	* 顺序号
	*/	
	public String getLocation() {
		return location;
	}
	/**
	* 顺序号
	*/	
	public void setLocation(String location) {
		this.location = location;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	}
	public List<BaseRes> getResList() {
		return resList;
	}
	public void setResList(List<BaseRes> resList) {
		this.resList = resList;
	}
	public String getResCode() {
		return resCode;
	}
	public void setResCode(String resCode) {
		this.resCode = resCode;
	}
	public String getResVal() {
		return resVal;
	}
	public void setResVal(String resVal) {
		this.resVal = resVal;
	}
	
	
}
