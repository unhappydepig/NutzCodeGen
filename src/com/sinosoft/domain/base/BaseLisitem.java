package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:25 
 * 自定义查询结果-t_base_list_item javaBean
 */
@Table("T_BASE_LIST_ITEM")
@PK(name="PK_T_BASE_LIST_ITEM",value={"listItemId"})
public class BaseLisitem extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*关联表主键
	*/	
	@Column("LIST_ITEM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String listItemId ;
	/**
	*列表id
	*/	
	@Column("LIST_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String listId ;
	/**
	*机构代码
	*/	
	@Column("COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String comId ;
	/**
	*用户代码
	*/	
	@Column("USER_ID")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String userId ;
	/**
	*属性代码
	*/	
	@Column("ATTR_CODE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String attrCode ;
	/**
	*显示名称
	*/	
	@Column("ATTR_NAME")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String attrName ;
	/**
	*格式化
	*/	
	@Column("FORMAT")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String format ;
	/**
	*是否可排序
	*/	
	@Column("ORDER_FLAG")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String orderFlag ;
	/**
	*是否可编辑
	*/	
	@Column("EDIT_FLAG")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String editFlag ;
	/**
	*显示状态
	*/	
	@Column("SHOW_FLAG")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String showFlag ;
	/**
	*是否必填
	*/	
	@Column("REQ_FLAG")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String reqFlag ;
	/**
	*顺序号
	*/	
	@Column("LOCATION")
	@ColDefine(type = ColType.INT, notNull = true)
	private java.lang.Integer location ;
	
	/**
	* 关联表主键
	*/	
	public String getListItemId() {
		return listItemId;
	}
	/**
	* 关联表主键
	*/	
	public void setListItemId(String listItemId) {
		this.listItemId = listItemId;
	} 
	/**
	* 列表id
	*/	
	public String getListId() {
		return listId;
	}
	/**
	* 列表id
	*/	
	public void setListId(String listId) {
		this.listId = listId;
	} 
	/**
	* 机构代码
	*/	
	public String getComId() {
		return comId;
	}
	/**
	* 机构代码
	*/	
	public void setComId(String comId) {
		this.comId = comId;
	} 
	/**
	* 用户代码
	*/	
	public String getUserId() {
		return userId;
	}
	/**
	* 用户代码
	*/	
	public void setUserId(String userId) {
		this.userId = userId;
	} 
	/**
	* 属性代码
	*/	
	public String getAttrCode() {
		return attrCode;
	}
	/**
	* 属性代码
	*/	
	public void setAttrCode(String attrCode) {
		this.attrCode = attrCode;
	} 
	/**
	* 显示名称
	*/	
	public String getAttrName() {
		return attrName;
	}
	/**
	* 显示名称
	*/	
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	} 
	/**
	* 格式化
	*/	
	public String getFormat() {
		return format;
	}
	/**
	* 格式化
	*/	
	public void setFormat(String format) {
		this.format = format;
	} 
	/**
	* 是否可排序
	*/	
	public String getOrderFlag() {
		return orderFlag;
	}
	/**
	* 是否可排序
	*/	
	public void setOrderFlag(String orderFlag) {
		this.orderFlag = orderFlag;
	} 
	/**
	* 是否可编辑
	*/	
	public String getEditFlag() {
		return editFlag;
	}
	/**
	* 是否可编辑
	*/	
	public void setEditFlag(String editFlag) {
		this.editFlag = editFlag;
	} 
	/**
	* 显示状态
	*/	
	public String getShowFlag() {
		return showFlag;
	}
	/**
	* 显示状态
	*/	
	public void setShowFlag(String showFlag) {
		this.showFlag = showFlag;
	} 
	/**
	* 是否必填
	*/	
	public String getReqFlag() {
		return reqFlag;
	}
	/**
	* 是否必填
	*/	
	public void setReqFlag(String reqFlag) {
		this.reqFlag = reqFlag;
	} 
	/**
	* 顺序号
	*/	
	public java.lang.Integer getLocation() {
		return location;
	}
	/**
	* 顺序号
	*/	
	public void setLocation(java.lang.Integer location) {
		this.location = location;
	} 
}
