package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-05 16:06:50 
 * t_base_config-基础信息配置类型表 javaBean
 */
@Table("T_BASE_CONFIG")
@PK(name="PK_T_BASE_CONFIG",value={"configCode"})
public class BaseConfig extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*配置信息项
	*/	
	@Column("CONFIG_CODE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String configCode ;
	/**
	*配置名称
	*/	
	@Column("CONFIG_NAME")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String configName ;
	/**
	*配置说明
	*/	
	@Column("REMARK")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String remark ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2, notNull = true)
	private String valid ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	
	/**
	* 配置信息项
	*/	
	public String getConfigCode() {
		return configCode;
	}
	/**
	* 配置信息项
	*/	
	public void setConfigCode(String configCode) {
		this.configCode = configCode;
	} 
	/**
	* 配置名称
	*/	
	public String getConfigName() {
		return configName;
	}
	/**
	* 配置名称
	*/	
	public void setConfigName(String configName) {
		this.configName = configName;
	} 
	/**
	* 配置说明
	*/	
	public String getRemark() {
		return remark;
	}
	/**
	* 配置说明
	*/	
	public void setRemark(String remark) {
		this.remark = remark;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
}
