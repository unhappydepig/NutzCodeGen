package com.sinosoft.domain.base;

import java.util.List;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 15:45:17 
 * 机构信息表-t_base_company javaBean
 */
@Table("T_BASE_COMPANY")
@PK(name="PK_T_BASE_COMPANY",value={"comId"})
public class BaseCompany extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*机构ID
	*/	
	@Column("COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String comId ;
	/**
	*上级机构ID
	*/	
	@Column("UP_COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String upComId ;
	/**
	*机构名称
	*/	
	@Column("COM_NAME")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String comName ;
	/**
	*机构类型
	*/	
	@Column("COM_TYPE")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String comType ;
	/**
	*所属行业
	*/	
	@Column("COM_TRADE")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String comTrade ;
	/**
	*地址
	*/	
	@Column("ADDRESS")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String address ;
	/**
	*电话
	*/	
	@Column("TELEPHONE")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String telephone ;
	/**
	*邮箱
	*/	
	@Column("MAIL")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String mail ;
	/**
	*顺序号
	*/	
	@Column("LOCATION")
	@ColDefine(type = ColType.INT, notNull = true)
	private java.lang.Integer location ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2, notNull = true)
	private String valid ;
	/**
	 * 下级列表
	 */
	private List<BaseCompany> comList;
	/**
	* 机构ID
	*/	
	public String getComId() {
		return comId;
	}
	/**
	* 机构ID
	*/	
	public void setComId(String comId) {
		this.comId = comId;
	} 
	/**
	* 上级机构ID
	*/	
	public String getUpComId() {
		return upComId;
	}
	/**
	* 上级机构ID
	*/	
	public void setUpComId(String upComId) {
		this.upComId = upComId;
	} 
	/**
	* 机构名称
	*/	
	public String getComName() {
		return comName;
	}
	/**
	* 机构名称
	*/	
	public void setComName(String comName) {
		this.comName = comName;
	} 
	/**
	* 机构类型
	*/	
	public String getComType() {
		return comType;
	}
	/**
	* 机构类型
	*/	
	public void setComType(String comType) {
		this.comType = comType;
	} 
	/**
	* 所属行业
	*/	
	public String getComTrade() {
		return comTrade;
	}
	/**
	* 所属行业
	*/	
	public void setComTrade(String comTrade) {
		this.comTrade = comTrade;
	} 
	/**
	* 地址
	*/	
	public String getAddress() {
		return address;
	}
	/**
	* 地址
	*/	
	public void setAddress(String address) {
		this.address = address;
	} 
	/**
	* 电话
	*/	
	public String getTelephone() {
		return telephone;
	}
	/**
	* 电话
	*/	
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	} 
	/**
	* 邮箱
	*/	
	public String getMail() {
		return mail;
	}
	/**
	* 邮箱
	*/	
	public void setMail(String mail) {
		this.mail = mail;
	} 
	/**
	* 顺序号
	*/	
	public java.lang.Integer getLocation() {
		return location;
	}
	/**
	* 顺序号
	*/	
	public void setLocation(java.lang.Integer location) {
		this.location = location;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	}
	public List<BaseCompany> getComList() {
		return comList;
	} 
	
}
