package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:26 
 * 员工信息-t_base_user javaBean
 */
@Table("T_BASE_USER")
@PK(name="PK_T_BASE_USER",value={"userId"})
public class BaseUser extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*员工ID
	*/	
	@Column("USER_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String userId ;
	/**
	*员工代码
	*/	
	@Column("USER_CODE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String userCode ;
	/**
	*机构代码
	*/	
	@Column("COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String comId ;
	/**
	*部门id
	*/	
	@Column("DEPT_ID")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String deptId ;
	/**
	*密码
	*/	
	@Column("PASSWORD")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String password ;
	/**
	*盐值
	*/	
	@Column("SALE")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String sale ;
	/**
	*名称
	*/	
	@Column("NAME")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String name ;
	/**
	*员工职务
	*/	
	@Column("USER_JOB")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String userJob ;
	/**
	*员工照片
	*/	
	@Column("USER_PIC")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String userPic ;
	/**
	*性别
	*/	
	@Column("SEX")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String sex ;
	/**
	*生日
	*/	
	@Column("BIRTHDAY")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date birthday ;
	/**
	*手机
	*/	
	@Column("MOBILE")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String mobile ;
	/**
	*邮箱
	*/	
	@Column("MAIL")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String mail ;
	/**
	*在职状态
	*/	
	@Column("STATE")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String state ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2)
	private String valid ;
	
	/**
	* 员工ID
	*/	
	public String getUserId() {
		return userId;
	}
	/**
	* 员工ID
	*/	
	public void setUserId(String userId) {
		this.userId = userId;
	} 
	/**
	* 员工代码
	*/	
	public String getUserCode() {
		return userCode;
	}
	/**
	* 员工代码
	*/	
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	} 
	/**
	* 机构代码
	*/	
	public String getComId() {
		return comId;
	}
	/**
	* 机构代码
	*/	
	public void setComId(String comId) {
		this.comId = comId;
	} 
	/**
	* 部门id
	*/	
	public String getDeptId() {
		return deptId;
	}
	/**
	* 部门id
	*/	
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	} 
	/**
	* 密码
	*/	
	public String getPassword() {
		return password;
	}
	/**
	* 密码
	*/	
	public void setPassword(String password) {
		this.password = password;
	} 
	/**
	* 盐值
	*/	
	public String getSale() {
		return sale;
	}
	/**
	* 盐值
	*/	
	public void setSale(String sale) {
		this.sale = sale;
	} 
	/**
	* 名称
	*/	
	public String getName() {
		return name;
	}
	/**
	* 名称
	*/	
	public void setName(String name) {
		this.name = name;
	} 
	/**
	* 员工职务
	*/	
	public String getUserJob() {
		return userJob;
	}
	/**
	* 员工职务
	*/	
	public void setUserJob(String userJob) {
		this.userJob = userJob;
	} 
	/**
	* 员工照片
	*/	
	public String getUserPic() {
		return userPic;
	}
	/**
	* 员工照片
	*/	
	public void setUserPic(String userPic) {
		this.userPic = userPic;
	} 
	/**
	* 性别
	*/	
	public String getSex() {
		return sex;
	}
	/**
	* 性别
	*/	
	public void setSex(String sex) {
		this.sex = sex;
	} 
	/**
	* 生日
	*/	
	public java.util.Date getBirthday() {
		return birthday;
	}
	/**
	* 生日
	*/	
	public void setBirthday(java.util.Date birthday) {
		this.birthday = birthday;
	} 
	/**
	* 手机
	*/	
	public String getMobile() {
		return mobile;
	}
	/**
	* 手机
	*/	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	} 
	/**
	* 邮箱
	*/	
	public String getMail() {
		return mail;
	}
	/**
	* 邮箱
	*/	
	public void setMail(String mail) {
		this.mail = mail;
	} 
	/**
	* 在职状态
	*/	
	public String getState() {
		return state;
	}
	/**
	* 在职状态
	*/	
	public void setState(String state) {
		this.state = state;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
}
