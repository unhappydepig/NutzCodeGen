package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-03 11:24:15 
 * 员工权限信息-t_base_user_company javaBean
 */
@Table("T_BASE_USER_COMPANY")
@PK(name="PK_T_BASE_USER_COMPANY",value={"userId","comId"})
public class BaseUserCompany extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*员工ID
	*/	
	@Column("USER_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String userId ;
	/**
	*机构ID
	*/	
	@Column("COM_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String comId ;
	
	/**
	* 员工ID
	*/	
	public String getUserId() {
		return userId;
	}
	/**
	* 员工ID
	*/	
	public void setUserId(String userId) {
		this.userId = userId;
	} 
	/**
	* 机构ID
	*/	
	public String getComId() {
		return comId;
	}
	/**
	* 机构ID
	*/	
	public void setComId(String comId) {
		this.comId = comId;
	} 
}
