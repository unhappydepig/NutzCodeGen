package com.sinosoft.domain.base;

import java.util.ArrayList;
import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
import com.sinosoft.common.db.StaticDaoUtil;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 23:06:25 
 * 基础代码表-t_base_code javaBean
 */
@Table("T_BASE_CODE")
@PK(name="PK_T_BASE_CODE",value={"codeId"})
public class BaseCode extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*主键
	*/	
	@Column("CODE_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String codeId ;
	/**
	*上级基础代码
	*/	
	@Column("UP_CODE_ID")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String upCodeId ;
	/**
	*key
	*/	
	@Column("CODE_CODE")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String codeCode ;
	/**
	*值
	*/	
	@Column("CODE_VAL")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String codeVal ;
	/**
	*备注
	*/	
	@Column("REMARK")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String remark ;
	/**
	*顺序号
	*/	
	@Column("LOCATION")
	@ColDefine(type = ColType.INT, notNull = true)
	private java.lang.Integer location ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2, notNull = true)
	private String valid ;
	
	private int subSize =0;
	
	List<BaseCode> subCodeList = new ArrayList<BaseCode>();
	
	/**
	* 主键
	*/	
	public String getCodeId() {
		return codeId;
	}
	/**
	* 主键
	*/	
	public void setCodeId(String codeId) {
		this.codeId = codeId;
	} 
	/**
	* 上级基础代码
	*/	
	public String getUpCodeId() {
		return upCodeId;
	}
	/**
	* 上级基础代码
	*/	
	public void setUpCodeId(String upCodeId) {
		this.upCodeId = upCodeId;
	} 
	/**
	* key
	*/	
	public String getCodeCode() {
		return codeCode;
	}
	/**
	* key
	*/	
	public void setCodeCode(String codeCode) {
		this.codeCode = codeCode;
	} 
	/**
	* 值
	*/	
	public String getCodeVal() {
		return codeVal;
	}
	/**
	* 值
	*/	
	public void setCodeVal(String codeVal) {
		this.codeVal = codeVal;
	} 
	/**
	* 备注
	*/	
	public String getRemark() {
		return remark;
	}
	/**
	* 备注
	*/	
	public void setRemark(String remark) {
		this.remark = remark;
	} 
	/**
	* 顺序号
	*/	
	public java.lang.Integer getLocation() {
		return location;
	}
	/**
	* 顺序号
	*/	
	public void setLocation(java.lang.Integer location) {
		this.location = location;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	}
	public int getSubSize() {
		return StaticDaoUtil.getDao().count(BaseCode.class, Cnd.where("upCodeId", "=", this.getCodeId()));
	}
	public List<BaseCode> getSubCodeList() {
		return subCodeList;
	}
	public void setSubCodeList(List<BaseCode> subCodeList) {
		this.subCodeList = subCodeList;
	}
	
	
}
