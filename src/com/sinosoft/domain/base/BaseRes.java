package com.sinosoft.domain.base;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-06-05 12:34:46.376 
 * 资源信息javaBean
 */
@Table("T_BASE_RES")
@PK(name="PK_T_BASE_RES",value={"resId"})
public class BaseRes extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*资源ID
	*/	
	@Column("RES_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String resId ;
	/**
	*上级资源ID
	*/	
	@Column("UP_RES_ID")
	@ColDefine(type = ColType.VARCHAR, width =30, notNull = true)
	private String upResId ;
	/**
	*资源名称
	*/	
	@Column("RES_NAME")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String resName ;
	/**
	*资源类型
	*/	
	@Column("RES_TYPE")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String resType ;
	/**
	*资源指向
	*/	
	@Column("TARGET")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String target ;
	/**
	*指向URL
	*/	
	@Column("URL")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String url ;
	/**
	*图标
	*/	
	@Column("RES_ICON")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String resIcon ;
	/**
	*选择器
	*/	
	@Column("SELECTOR")
	@ColDefine(type = ColType.VARCHAR, width =200)
	private String selector ;
	/**
	*顺序号
	*/	
	@Column("LOCATION")
	@ColDefine(type = ColType.INT, notNull = true)
	private java.lang.Integer location ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =30)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*有效标示
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2, notNull = true)
	private String valid ;
	
	/**
	* 资源ID
	*/	
	public String getResId() {
		return resId;
	}
	/**
	* 资源ID
	*/	
	public void setResId(String resId) {
		this.resId = resId;
	} 
	/**
	* 上级资源ID
	*/	
	public String getUpResId() {
		return upResId;
	}
	/**
	* 上级资源ID
	*/	
	public void setUpResId(String upResId) {
		this.upResId = upResId;
	} 
	/**
	* 资源名称
	*/	
	public String getResName() {
		return resName;
	}
	/**
	* 资源名称
	*/	
	public void setResName(String resName) {
		this.resName = resName;
	} 
	/**
	* 资源类型
	*/	
	public String getResType() {
		return resType;
	}
	/**
	* 资源类型
	*/	
	public void setResType(String resType) {
		this.resType = resType;
	} 
	/**
	* 资源指向
	*/	
	public String getTarget() {
		return target;
	}
	/**
	* 资源指向
	*/	
	public void setTarget(String target) {
		this.target = target;
	} 
	/**
	* 指向URL
	*/	
	public String getUrl() {
		return url;
	}
	/**
	* 指向URL
	*/	
	public void setUrl(String url) {
		this.url = url;
	} 
	/**
	* 图标
	*/	
	public String getResIcon() {
		return resIcon;
	}
	/**
	* 图标
	*/	
	public void setResIcon(String resIcon) {
		this.resIcon = resIcon;
	} 
	/**
	* 选择器
	*/	
	public String getSelector() {
		return selector;
	}
	/**
	* 选择器
	*/	
	public void setSelector(String selector) {
		this.selector = selector;
	} 
	/**
	* 顺序号
	*/	
	public java.lang.Integer getLocation() {
		return location;
	}
	/**
	* 顺序号
	*/	
	public void setLocation(java.lang.Integer location) {
		this.location = location;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**
	* 有效标示
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标示
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
}
