package com.sinosoft.domain.code;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-22 10:11:15 
 * 数据类型映射关系 javaBean
 */
@Table("T_CODE_DB_MAPPING")
@PK(name="PK_T_CODE_DB_MAPPING",value={"dbMapId"})
public class CodeDbMapping extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*主键
	*/	
	@Column("DB_MAP_ID")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String dbMapId ;
	/**
	*映射类型
	*/	
	@Column("MAP_TYPE_ID")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String mapTypeId ;
	/**
	*变量名
	*/	
	@Column("COVER_NAME")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String coverName ;
	/**
	*转换方式
	*/	
	@Column("COVER_TYPE")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String coverType ;
	/**
	*转换入参
	*/	
	@Column("COVER_IN")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String coverIn ;
	/**
	*转换比对值
	*/	
	@Column("COVER_VAL")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String coverVal ;
	/**
	*转换结果
	*/	
	@Column("COVER_OUT")
	@ColDefine(type = ColType.VARCHAR, width =100, notNull = true)
	private String coverOut ;
	/**
	*优先级
	*/	
	@Column("COVER_LEVEL")
	@ColDefine(type = ColType.INT)
	private java.lang.Integer coverLevel ;
	/**
	*有效标识
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2)
	private String valid ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	*映射名称
	*/	
	private String mapName ;
	
	/**
	* 主键
	*/	
	public String getDbMapId() {
		return dbMapId;
	}
	/**
	* 主键
	*/	
	public void setDbMapId(String dbMapId) {
		this.dbMapId = dbMapId;
	} 
	/**
	* 映射类型
	*/	
	public String getMapTypeId() {
		return mapTypeId;
	}
	/**
	* 映射类型
	*/	
	public void setMapTypeId(String mapTypeId) {
		this.mapTypeId = mapTypeId;
	} 
	/**
	* 变量名
	*/	
	public String getCoverName() {
		return coverName;
	}
	/**
	* 变量名
	*/	
	public void setCoverName(String coverName) {
		this.coverName = coverName;
	} 
	/**
	* 转换方式
	*/	
	public String getCoverType() {
		return coverType;
	}
	/**
	* 转换方式
	*/	
	public void setCoverType(String coverType) {
		this.coverType = coverType;
	} 
	/**
	* 转换入参
	*/	
	public String getCoverIn() {
		return coverIn;
	}
	/**
	* 转换入参
	*/	
	public void setCoverIn(String coverIn) {
		this.coverIn = coverIn;
	} 
	/**
	* 转换比对值
	*/	
	public String getCoverVal() {
		return coverVal;
	}
	/**
	* 转换比对值
	*/	
	public void setCoverVal(String coverVal) {
		this.coverVal = coverVal;
	} 
	/**
	* 转换结果
	*/	
	public String getCoverOut() {
		return coverOut;
	}
	/**
	* 转换结果
	*/	
	public void setCoverOut(String coverOut) {
		this.coverOut = coverOut;
	} 
	/**
	* 优先级
	*/	
	public java.lang.Integer getCoverLevel() {
		return coverLevel;
	}
	/**
	* 优先级
	*/	
	public void setCoverLevel(java.lang.Integer coverLevel) {
		this.coverLevel = coverLevel;
	} 
	/**
	* 有效标识
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标识
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getMapName() {
		return mapName;
	}
	public void setMapName(String mapName) {
		this.mapName = mapName;
	}
	
}
