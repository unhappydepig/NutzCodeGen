package com.sinosoft.domain.code;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-20 14:17:07 
 * 数据库配置-t_code_db_config javaBean
 */
@Table("T_CODE_DB_CONFIG")
@PK(name="PK_T_CODE_DB_CONFIG",value={"dbConfigId"})
public class CodeDbConfig extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*主键
	*/	
	@Column("DB_CONFIG_ID")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String dbConfigId ;
	/**
	*数据库配置名称
	*/	
	@Column("CONFIG_NAME")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String configName ;
	/**
	*数据库类型
	*/	
	@Column("DB_TYPE")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String dbType ;
	/**
	*数据库驱动
	*/	
	@Column("DB_DRIVER")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String dbDriver ;
	/**
	*数据库地址
	*/	
	@Column("DB_ADDRESS")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String dbAddress ;
	/**
	*用户名
	*/	
	@Column("DB_USER")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String dbUser ;
	/**
	*密码
	*/	
	@Column("DB_PASS")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String dbPass ;
	/**
	*有效标识
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2)
	private String valid ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	
	/**
	* 主键
	*/	
	public String getDbConfigId() {
		return dbConfigId;
	}
	/**
	* 主键
	*/	
	public void setDbConfigId(String dbConfigId) {
		this.dbConfigId = dbConfigId;
	} 
	/**
	* 数据库配置名称
	*/	
	public String getConfigName() {
		return configName;
	}
	/**
	* 数据库配置名称
	*/	
	public void setConfigName(String configName) {
		this.configName = configName;
	} 
	/**
	* 数据库类型
	*/	
	public String getDbType() {
		return dbType;
	}
	/**
	* 数据库类型
	*/	
	public void setDbType(String dbType) {
		this.dbType = dbType;
	} 
	/**
	* 数据库驱动
	*/	
	public String getDbDriver() {
		return dbDriver;
	}
	/**
	* 数据库驱动
	*/	
	public void setDbDriver(String dbDriver) {
		this.dbDriver = dbDriver;
	} 
	/**
	* 数据库地址
	*/	
	public String getDbAddress() {
		return dbAddress;
	}
	/**
	* 数据库地址
	*/	
	public void setDbAddress(String dbAddress) {
		this.dbAddress = dbAddress;
	} 
	/**
	* 用户名
	*/	
	public String getDbUser() {
		return dbUser;
	}
	/**
	* 用户名
	*/	
	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	} 
	/**
	* 密码
	*/	
	public String getDbPass() {
		return dbPass;
	}
	/**
	* 密码
	*/	
	public void setDbPass(String dbPass) {
		this.dbPass = dbPass;
	} 
	/**
	* 有效标识
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标识
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
}
