package com.sinosoft.domain.code;

import java.util.List;

import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Table;

import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-23 15:41:15 
 * 数据类型映射类型 javaBean
 */
@Table("T_CODE_MAP_TYPE")
@PK(name="PK_T_CODE_MAP_TYPE",value={"mapTypeId"})
public class CodeMapType extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*主键
	*/	
	@Column("MAP_TYPE_ID")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true,precision=8)
	private String mapTypeId ;
	/**
	*数据库类型
	*/	
	@Column("DB_TYPE")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String dbType ;
	/**
	*方案ID
	*/	
	@Column("PLAN_ID")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String planId ;
	/**
	*映射名称
	*/	
	@Column("MAP_NAME")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String mapName ;
	/**
	*转换针对的对象
	*/	
	@Column("MAP_TARGET")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String mapTarget ;
	/**
	*有效标识
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2)
	private String valid ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	/**
	* 主键
	*/	
	public String getMapTypeId() {
		return mapTypeId;
	}
	/**
	* 主键
	*/	
	public void setMapTypeId(String mapTypeId) {
		this.mapTypeId = mapTypeId;
	} 
	/**
	* 数据库类型
	*/	
	public String getDbType() {
		return dbType;
	}
	/**
	* 数据库类型
	*/	
	public void setDbType(String dbType) {
		this.dbType = dbType;
	} 
	/**
	* 方案ID
	*/	
	public String getPlanId() {
		return planId;
	}
	/**
	* 方案ID
	*/	
	public void setPlanId(String planId) {
		this.planId = planId;
	} 
	/**
	* 映射名称
	*/	
	public String getMapName() {
		return mapName;
	}
	/**
	* 映射名称
	*/	
	public void setMapName(String mapName) {
		this.mapName = mapName;
	} 
	/**
	* 转换针对的对象
	*/	
	public String getMapTarget() {
		return mapTarget;
	}
	/**
	* 转换针对的对象
	*/	
	public void setMapTarget(String mapTarget) {
		this.mapTarget = mapTarget;
	} 
	/**
	* 有效标识
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标识
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
	

	/**
	 * 映射类型下包含的明细
	 */
	List<CodeDbMapping>  dbMappingList = null;
	public List<CodeDbMapping> getDbMappingList() {
		return dbMappingList;
	}
	public void setDbMappingList(List<CodeDbMapping> dbMappingList) {
		this.dbMappingList = dbMappingList;
	}
	
	
	
}
