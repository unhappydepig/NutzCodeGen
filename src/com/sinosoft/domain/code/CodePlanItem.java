package com.sinosoft.domain.code;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-22 08:51:00 
 * 方案明细 javaBean
 */
@Table("T_CODE_PLAN_ITEM")
@PK(name="PK_T_CODE_PLAN_ITEM",value={"planItemId"})
public class CodePlanItem extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*主键
	*/	
	@Column("PLAN_ITEM_ID")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String planItemId ;
	/**
	*方案ID
	*/	
	@Column("PLAN_ID")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String planId ;
	/**
	*明细名称
	*/	
	@Column("NAME")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String name ;
	/**
	*文件相对路径
	*/	
	@Column("FILE_PATH")
	@ColDefine(type = ColType.VARCHAR, width =100)
	private String filePath ;
	/**
	*文件名称
	*/	
	@Column("FILE_NAME")
	@ColDefine(type = ColType.VARCHAR, width =200)
	private String fileName ;
	/**
	*文件内容
	*/	
	@Column("FILE_CONTEXT")
	@ColDefine(type = ColType.TEXT)
	private String fileContext ;
	/**
	*后缀名
	*/	
	@Column("FILE_EXT")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String fileExt ;
	/**
	*转换针对的对象
	*/	
	@Column("MAP_TARGET")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String mapTarget ;
	/**
	*有效标识
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2)
	private String valid ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	
	/**
	* 主键
	*/	
	public String getPlanItemId() {
		return planItemId;
	}
	/**
	* 主键
	*/	
	public void setPlanItemId(String planItemId) {
		this.planItemId = planItemId;
	} 
	/**
	* 方案ID
	*/	
	public String getPlanId() {
		return planId;
	}
	/**
	* 方案ID
	*/	
	public void setPlanId(String planId) {
		this.planId = planId;
	} 
	/**
	* 明细名称
	*/	
	public String getName() {
		return name;
	}
	/**
	* 明细名称
	*/	
	public void setName(String name) {
		this.name = name;
	} 
	/**
	* 文件相对路径
	*/	
	public String getFilePath() {
		return filePath;
	}
	/**
	* 文件相对路径
	*/	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	} 
	/**
	* 文件名称
	*/	
	public String getFileName() {
		return fileName;
	}
	/**
	* 文件名称
	*/	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	} 
	/**
	* 文件内容
	*/	
	public String getFileContext() {
		return fileContext;
	}
	/**
	* 文件内容
	*/	
	public void setFileContext(String fileContext) {
		this.fileContext = fileContext;
	} 
	/**
	* 后缀名
	*/	
	public String getFileExt() {
		return fileExt;
	}
	/**
	* 后缀名
	*/	
	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	} 
	/**
	* 转换针对的对象
	*/	
	public String getMapTarget() {
		return mapTarget;
	}
	/**
	* 转换针对的对象
	*/	
	public void setMapTarget(String mapTarget) {
		this.mapTarget = mapTarget;
	} 
	/**
	* 有效标识
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标识
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
}
