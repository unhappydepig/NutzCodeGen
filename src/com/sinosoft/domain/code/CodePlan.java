package com.sinosoft.domain.code;

import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ColDefine;
import org.nutz.dao.entity.annotation.ColType;
import com.sinosoft.common.bean.BaseDto;
/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-24 19:52:26.854 
 * 代码生成方案javaBean
 */
@Table("T_CODE_PLAN")
@PK(name="PK_T_CODE_PLAN",value={"planId"})
public class CodePlan extends BaseDto {
	private static final long serialVersionUID = 1L;
	/**
	*主键
	*/	
	@Column("PLAN_ID")
	@ColDefine(type = ColType.VARCHAR, width =50, notNull = true)
	private String planId ;
	/**
	*方案名称
	*/	
	@Column("PLAN_NAME")
	@ColDefine(type = ColType.VARCHAR, width =500, notNull = true)
	private String planName ;
	/**
	*数据转换
	*/	
	@Column("DB_MAP")
	@ColDefine(type = ColType.VARCHAR, width =500)
	private String dbMap ;
	/**
	*代码包名
	*/	
	@Column("PACKET_NAME")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String packetName ;
	/**
	*代码生成方式
	*/	
	@Column("GEN_TYPE")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String genType ;
	/**
	*是否共享
	*/	
	@Column("SHARE_FLAG")
	@ColDefine(type = ColType.VARCHAR, width =2)
	private String shareFlag ;
	/**
	*有效标识
	*/	
	@Column("VALID")
	@ColDefine(type = ColType.VARCHAR, width =2)
	private String valid ;
	/**
	*创建人
	*/	
	@Column("CREATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String createBy ;
	/**
	*创建时间
	*/	
	@Column("CREATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date createTime ;
	/**
	*更新人
	*/	
	@Column("UPDATE_BY")
	@ColDefine(type = ColType.VARCHAR, width =50)
	private String updateBy ;
	/**
	*更新时间
	*/	
	@Column("UPDATE_TIME")
	@ColDefine(type = ColType.DATETIME)
	private java.util.Date updateTime ;
	
	/**
	* 主键
	*/	
	public String getPlanId() {
		return planId;
	}
	/**
	* 主键
	*/	
	public void setPlanId(String planId) {
		this.planId = planId;
	} 
	/**
	* 方案名称
	*/	
	public String getPlanName() {
		return planName;
	}
	/**
	* 方案名称
	*/	
	public void setPlanName(String planName) {
		this.planName = planName;
	} 
	/**
	* 数据转换
	*/	
	public String getDbMap() {
		return dbMap;
	}
	/**
	* 数据转换
	*/	
	public void setDbMap(String dbMap) {
		this.dbMap = dbMap;
	} 
	/**
	* 代码包名
	*/	
	public String getPacketName() {
		return packetName;
	}
	/**
	* 代码包名
	*/	
	public void setPacketName(String packetName) {
		this.packetName = packetName;
	} 
	/**
	* 代码生成方式
	*/	
	public String getGenType() {
		return genType;
	}
	/**
	* 代码生成方式
	*/	
	public void setGenType(String genType) {
		this.genType = genType;
	} 
	/**
	* 是否共享
	*/	
	public String getShareFlag() {
		return shareFlag;
	}
	/**
	* 是否共享
	*/	
	public void setShareFlag(String shareFlag) {
		this.shareFlag = shareFlag;
	} 
	/**
	* 有效标识
	*/	
	public String getValid() {
		return valid;
	}
	/**
	* 有效标识
	*/	
	public void setValid(String valid) {
		this.valid = valid;
	} 
	/**
	* 创建人
	*/	
	public String getCreateBy() {
		return createBy;
	}
	/**
	* 创建人
	*/	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	/**
	* 创建时间
	*/	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 创建时间
	*/	
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	} 
	/**
	* 更新人
	*/	
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	* 更新人
	*/	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	} 
	/**
	* 更新时间
	*/	
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	/**
	* 更新时间
	*/	
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	} 
}
