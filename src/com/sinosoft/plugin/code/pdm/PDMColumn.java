package com.sinosoft.plugin.code.pdm;

public class PDMColumn {
	private String id;
	private String name;
	private String code;
	private String dataType; // 数据类型
	private String dataTypeExt;//不含经度的
	private String length; // 数据长度
	private String precision; // 数据精度
	private String mandatory ="0"; // 字段是否为空，默认可空
	private String defaultValue; // 字段默认值
	private String lowValue;
	private String highValue;
	private String comment;
	private String pkflag;
	private PDMTable table;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getPrecision() {
		return precision;
	}

	public void setPrecision(String precision) {
		this.precision = precision;
	}

	public String getMandatory() {
		return mandatory;
	}

	public void setMandatory(String mandatory) {
		this.mandatory = mandatory;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getLowValue() {
		return lowValue;
	}

	public void setLowValue(String lowValue) {
		this.lowValue = lowValue;
	}

	public String getHighValue() {
		return highValue;
	}

	public void setHighValue(String highValue) {
		this.highValue = highValue;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public PDMTable getTable() {
		return table;
	}

	public void setTable(PDMTable table) {
		this.table = table;
	}

	public String getDataTypeExt() {
		if(dataType.indexOf("(")>-1){
			dataTypeExt = dataType.substring(0, dataType.indexOf("("));	
		}else{
			dataTypeExt = dataType;
		}			
		return dataTypeExt;
	}

	public String getPkflag() {
		return pkflag;
	}

	public void setPkflag(String pkflag) {
		this.pkflag = pkflag;
	}
	
}
