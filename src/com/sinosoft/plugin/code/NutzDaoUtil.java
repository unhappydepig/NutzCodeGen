package com.sinosoft.plugin.code;

import java.util.HashMap;
import java.util.Map;

import org.nutz.dao.Dao;
import org.nutz.dao.impl.NutDao;
import org.nutz.dao.impl.SimpleDataSource;

import com.sinosoft.domain.code.CodeDbConfig;
/**
 * 代码生成时使用的
 * @author unhappydepig 
 * @mail yqw8912@163.com
 * @time 2015-5-20 下午3:00:53
 */
public class NutzDaoUtil {
	private static Map<String, Dao> daoMap;
	/**
	 * 获取链接
	 * @param dbConfig
	 * @return
	 * @throws Exception
	 */
	public Dao getNutzDao(CodeDbConfig dbConfig) throws Exception {
		if (null == daoMap || !daoMap.containsKey(dbConfig.getDbConfigId())) {
			SimpleDataSource ds = new SimpleDataSource();
			if (null != dbConfig.getDbDriver()&&!"".equals(dbConfig.getDbDriver())) {
				ds.setDriverClassName(dbConfig.getDbDriver());
			}
			ds.setJdbcUrl(dbConfig.getDbAddress());
			ds.setUsername(dbConfig.getDbUser());
			ds.setPassword(dbConfig.getDbPass());
			Dao dao = new NutDao(ds);
			if(null==daoMap){
				daoMap = new HashMap<String, Dao>();
			}
			daoMap.put(dbConfig.getDbConfigId(), dao);
		}
		return daoMap.get(dbConfig.getDbConfigId());
	}
	public void remove(String configCode){
		if(null!=daoMap){
			daoMap.remove(configCode);
		}	
	}
}
