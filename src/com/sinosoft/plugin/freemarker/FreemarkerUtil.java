package com.sinosoft.plugin.freemarker;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreemarkerUtil {
	
    public static String generateByFtlTemplate(String templateContext, Map labelDataMap) {
        //使用FreeMaker模板
        try {
        	if(null==templateContext||"".equals(templateContext)||null==labelDataMap){
        		return templateContext;
        	}
        	if(templateContext.indexOf("$")<0){
        		return templateContext;
        	}
            Configuration configuration = new Configuration();
            configuration.setTemplateLoader(new StringTemplateLoader(templateContext));
            configuration.setDefaultEncoding("UTF-8");
            configuration.setClassicCompatible(true);
            Template template = configuration.getTemplate(""); //设置属性 空值处理
            StringWriter writer = new StringWriter();
            template.process(labelDataMap, writer);
            return writer.toString();
        } catch (TemplateException e) {
        	e.printStackTrace();
        } catch (IOException e) {
        	e.printStackTrace();
        }
        return null;
    }
}
