package com.sinosoft.common.util;

import java.util.HashMap;
import java.util.Map;

import com.sinosoft.common.datatype.DateTime;

/**
 * 缓存工具类
 * @author unhappydepig
 *
 */
public class CacheUtil {
	/**
	 * 缓存默认存放5分钟
	 */
	private static final int cacheTime = 60;
	private static final String dueTimeFlag = "dueTime";
	private static final String objectFlag = "object";
	/**
	 * 就是把对象放到内存里
	 * @param key
	 * @param value
	 */
	public static void put(String key,Object value){
		put(key, value, cacheTime);
	} 
	/**
	 * 就是把对象放到内存里
	 * @param key
	 * @param value
	 */
	public static void put(String key,Object value,int cacheTime){
		Map<String,Object> cacheItem = new HashMap<String,Object>();
		cacheItem.put(dueTimeFlag, DateTime.current().addMinute(cacheTime).toString(DateTime.YEAR_TO_SECOND));
		cacheItem.put(objectFlag, value);
		CacheConstans.cacheMap.put(key, cacheItem);
	} 	
	public static Object get(String key){
		if(null==CacheConstans.cacheMap.get(key)){
			return null;
		}
		Map<String,Object> cacheItem = CacheConstans.cacheMap.get(key);
		DateTime dueTime =  new DateTime(cacheItem.get(dueTimeFlag).toString(),DateTime.YEAR_TO_SECOND);
		if(DateTime.current().after(dueTime)){
			CacheConstans.cacheMap.remove(key);
			return null;
		}
		return cacheItem.get(objectFlag);
	}
}
