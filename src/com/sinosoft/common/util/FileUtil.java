package com.sinosoft.common.util;



import java.io.File;
import java.net.URL;


public final class FileUtil
{
  public static String tempFilePath = null;
  public static String separator = SeparatorUtil.getFileSeparator();

  private static String getClassNameWithoutPackage(Class cl) {
    String className = cl.getName();
    int pos = className.lastIndexOf('.') + 1;
    if (pos == -1) {
      pos = 0;
    }
    return className.substring(pos);
  }
  public static String getRealPathName(Class cl)
  {
    URL url = cl.getResource(getClassNameWithoutPackage(cl) + ".class");
    if (url != null) {
      return url.getPath();
    }
    return null;
  }
   /**
    * 创建新文件
    * @param newFile
    * @throws Exception
    */
	public static void createNewFile(File newFile) throws Exception {
		File parent = newFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		}
		newFile.createNewFile();
	 } 
	/**
	 * 根据.分割的字符串创建目录
	 * @param packetURL
	 * @return
	 */
	public static String getFilePath(String packetURL){
		String[] packet = StringUtil.split(packetURL, ".");
		String url = "";
		for(int i =0;i<packet.length;i++){	
			if(null!=packet[i]&&!"".equals(packet[i])){
				url = url+packet[i]+FileUtil.separator;
			}
		}				
		return tempFilePath+url;
	}
}
