package com.sinosoft.common.util;

import java.util.HashMap;
import java.util.Map;

import com.sinosoft.common.bean.SessionObj;

/**
 * 放置缓存的
 * @author unhappydepig
 *
 */
public class CacheConstans {
	
	/**
	 * 缓存信息的Map
	 */
    public static Map<String,Map<String,Object>> cacheMap =  new HashMap<String, Map<String, Object>>();
    /**
     * session中放置信息的Map
     */
    public static Map<String,SessionObj> sessionMap =  new HashMap<String,SessionObj>();
}
