package com.sinosoft.common.util;

import java.util.ArrayList;
import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;

import com.sinosoft.common.bean.CodeNameDto;
import com.sinosoft.common.db.DaoUtil;
import com.sinosoft.common.db.StaticDaoUtil;
import com.sinosoft.domain.base.BaseCode;

/**
 * 基础代码处理类
 * @author unhappydepig 
 * @mail yqw8912@163.com
 * @time 2015-4-27 下午5:50:44
 */
public class BaseCommonUtil {
	private static final String cacheFlag = "BaseComm";
	/*
	 * 查询基础代码列表
	 * @param codeType
	 * @return
	 */
	public static List<CodeNameDto> findCodeList(String codeType,String sessionId) {
		DaoUtil daoUtil = new DaoUtil(StaticDaoUtil.getDao());
		if(null==codeType){
			return null;
		}		
		String key = cacheFlag+Constans.PK_SEP+codeType;
		Object obj =CacheUtil.get(key);
		if(null!=obj){
			return (List<CodeNameDto>) obj;
		}
		Dao dao = StaticDaoUtil.getDao();
		List resultCodeList= new ArrayList<CodeNameDto>();
		BaseCode baseCode = dao.fetch(BaseCode.class, Cnd.where("CODE_CODE", "=",codeType));
  		if(null!=baseCode){
  			BaseCode baseCodeContion = new BaseCode();
  			baseCodeContion.setUpCodeId(baseCode.getCodeId());  
      		List<BaseCode>  baseCodeList= daoUtil.queryList(baseCodeContion);
  		    	if(null!=baseCodeList&&baseCodeList.size()>0){
  	          		for(int i =0 ;i<baseCodeList.size();i++){
  	          		CodeNameDto codeNameDto = new CodeNameDto();
  	          		codeNameDto.setCodeCode(baseCodeList.get(i).getCodeCode());
  	          		codeNameDto.setCodeName(baseCodeList.get(i).getCodeVal());
  	          		resultCodeList.add(codeNameDto);
  	          			}
  		    		}
  		}	
  		CacheUtil.put(key, resultCodeList);
		return resultCodeList;
	}	
}
