package com.sinosoft.common.util;

import java.util.List;

import com.sinosoft.castor.CastorUtil;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.db.DaoUtil;
import com.sinosoft.common.db.StaticDaoUtil;
import com.sinosoft.domain.base.BaseCode;
import com.sinosoft.domain.base.BaseCompany;
import com.sinosoft.domain.base.BaseConfig;
import com.sinosoft.domain.base.BaseRes;
import com.sinosoft.domain.base.BaseUser;
import com.sinosoft.domain.code.CodeMapType;
import com.sinosoft.domain.code.CodePlan;
/**
 * 翻译工具类
 * @author unhappydepig 
 * @mail yqw8912@163.com
 * @time 2015-5-8 下午4:06:50
 */
public class TransUtil {
	private static final String cacheFlag = "trans";
	public static String code2Name(String codeType,String codeCode){ 	
		if(null==codeCode||"".equals(codeCode)){
			return "";
		}		
		String key = cacheFlag+Constans.PK_SEP+codeType+Constans.PK_SEP+codeCode;
		Object obj =CacheUtil.get(key);
		if(null!=obj){
			return obj.toString();
		}	
		String codeVal = codeCode;
		String[] codeArray = codeCode.split(",");
		if(codeArray.length>1){
			String[] valArray = new String[codeArray.length];
			for(int i=0;i<codeArray.length;i++){
				String val = code2Name(codeType, codeArray[i]);
				valArray[i] = val;
			}
			codeVal = CastorUtil.castTo(valArray, String.class);			
		}else{
			codeVal = code2NameDb(codeType, codeCode);
		}
		CacheUtil.put(key, codeVal);
		return codeVal;
	}
	
	/**
	 * 翻译代码
	 */
	private static String code2NameDb(String codeType,String codeCode){
		DaoUtil daoUtil = new DaoUtil(StaticDaoUtil.getDao());
		if("company".equals(codeType)){
  			return company(daoUtil,codeCode);
  		}
		if("user".equals(codeType)){
  			return user(daoUtil,codeCode);
  		}		
		if("baseConfig".equals(codeType)){
  			return baseConfig(daoUtil,codeCode);
  		}		
		if("baseRes".equals(codeType)){
  			return baseRes(daoUtil,codeCode);
  		}			
		if("codePlan".equals(codeType)){
  			return codePlan(daoUtil,codeCode);
  		}			
		if("codeMapType".equals(codeType)){
  			return codeMapType(daoUtil,codeCode);
  		}
		BaseCode baseCode  = new BaseCode();
		baseCode.setCodeCode(codeType);
		List<BaseCode> codeTypeList =  daoUtil .queryList(baseCode, new BaseCondition(1,1));
		String codeTypeId = null;
		if(null!=codeTypeList&&codeTypeList.size()>0){
			codeTypeId = codeTypeList.get(0).getCodeId();
		}else{
			return codeCode;
		}
		baseCode  = new BaseCode();
		baseCode.setUpCodeId(codeTypeId);
		baseCode.setCodeCode(codeCode);
		List<BaseCode> codeList =  daoUtil .queryList(baseCode, new BaseCondition(1,1));
		if(null!=codeList&&codeList.size()>0){
			return codeList.get(0).getCodeVal();
		}
 		return codeCode;
	} 
	/**
	 * 机构代码
	 */
	private static String company(DaoUtil daoUtil,String codeCode){ 
		BaseCompany baseCompany = daoUtil.detailByPK(BaseCompany.class,codeCode);
		if(null!=baseCompany){
			return baseCompany.getComName();
		}
 		return codeCode;
	} 
	/**
	 * 代码模版
	 */
	private static String codePlan(DaoUtil daoUtil,String codeCode){ 
		CodePlan codePlan = daoUtil.detailByPK(CodePlan.class,codeCode);
		if(null!=codePlan){
			return codePlan.getPlanName();
		}
 		return codeCode;
	}  
	/**
	 * 转换类型
	 */
	private static String codeMapType(DaoUtil daoUtil,String codeCode){ 
		CodeMapType codeMapType = daoUtil.detailByPK(CodeMapType.class,codeCode);
		if(null!=codeMapType){
			return codeMapType.getMapName();
		}
 		return codeCode;
	} 
	/**
	 * 员工代码
	 */
	private static String user(DaoUtil daoUtil,String codeCode){ 
		BaseUser baseUser = daoUtil.detailByPK(BaseUser.class,codeCode);
		if(null!=baseUser){
			return baseUser.getName();
		}
 		return codeCode;
	} 	
	/**
	 * 机构代码
	 */
	private static String baseConfig(DaoUtil daoUtil,String codeCode){ 
		BaseConfig baseConfig = daoUtil.detailByPK(BaseConfig.class,codeCode);
		if(null!=baseConfig){
			return baseConfig.getConfigName();
		}
 		return codeCode;
	} 		
	/**
	 * 资源
	 */
	private static String baseRes(DaoUtil daoUtil,String codeCode){ 
		BaseRes baseRes = daoUtil.detailByPK(BaseRes.class,codeCode);
		if(null!=baseRes){
			return baseRes.getResName();
		}
 		return codeCode;
	} 	
}
