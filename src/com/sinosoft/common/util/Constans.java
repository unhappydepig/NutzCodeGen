package com.sinosoft.common.util;
/**
 * 系统常量类
 * @author unhappydepig 
 * @mail yqw8912@163.com
 * @time 2015-5-5 下午2:33:36
 */
public class Constans {
	//主键分割符
	public static final String PK_SEP ="___";//
	
	//数据权限级别Power
	/**
	 * 超级用户
	 */
	public static final int P_USER =100;//当前用户
	/**
	 * 超级用户
	 */
	public static final int P_SUPER =0;//超级用户
	/**
	 * 当前级别
	 */
	public static final int P_SELF =1;//当前级别
	/**
	 * 当前及所有子集
	 */
	public static final int P_SELF_SUB =2;//当前及所有子集
	/**
	 * 当前及下级子集
	 */
	public static final int P_SELF_NEXY_SUB =3;//当前及下级子集
	/**
	 * 所有子集
	 */
	public static final int P_SUB =4;//所有子集
	/**
	 * 下级子集
	 */
	public static final int P_NEXY_SUB =5;//下级子集
	/**
	 * 当前及所有上级
	 */
	public static final int P_SELF_UP =6;//当前及所有上级
	
	//系统内置岗位信息	
	public static final String G_SUPER ="0";//超级管理员
	
	
	
	
	//查询级别Query
	public static final int Q_EQU =0;//全文匹配查询
	public static final int Q_LIKE =1;//全文模糊查询
	
	
}
