package com.sinosoft.common.util;

import java.util.Properties;


public class SeparatorUtil {

	/* system properties to get separators */
	static final Properties PROPERTIES = new Properties(System.getProperties());

	/**
	 * get line separator on current platform
	 * 
	 * @return line separator
	 */
	public static String getLineSeparator() {
		return System.getProperties().getProperty("line.separator");
	}

	/**
	 * get path separator on current platform
	 * 
	 * @return path separator
	 */
	public static String getPathSeparator() {
		return System.getProperties().getProperty("path.separator");
	}
	public static String getFileSeparator() {
		return System.getProperties().getProperty("file.separator");
	}
		
	public static void main(String[] args) {
		System.out.println("Line separator is: "
				+ new SeparatorUtil().getLineSeparator());
		System.out.println("Path separator is: "
				+ SeparatorUtil.getPathSeparator());
		System.out.println("File separator is: "
				+ SeparatorUtil.getFileSeparator());
	}
}
