package com.sinosoft.common.action;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.dao.Dao;
import org.nutz.json.Json;
import org.nutz.mvc.ActionContext;

import com.sinosoft.common.bean.SessionObj;
import com.sinosoft.common.db.DaoUtil;
import com.sinosoft.common.util.CacheConstans;
/**
 * 所有Action继承的父类
 * @author unhappydepig 
 * @mail yqw8912@163.com
 * @time 2015-4-7 下午5:37:16
 */
public class BaseAction {
	protected final static String successMessage = "操作成功!";
	protected final static String erroeMessage = "操作失败!";
	protected  Dao dao;
	protected  DaoUtil daoUtil;
	/**
	 * 把上下文对象赋值到DaoCtl中
	 * @param context
	 */
	public  void setContext(ActionContext context){
		daoUtil = new DaoUtil();
		daoUtil.setContext(context);
	}
	/**
	 * 把上下文对象赋值到DaoCtl中
	 * @param context
	 */
	public  void setDao(Dao daoTemp){
		dao = daoTemp;
		daoUtil.setDao(dao);
	}
	protected void ajaxDone(HttpServletResponse response,int statusCode, String message,boolean closeFlag) {
		Map<String,Object> ajaxMap = new HashMap<String,Object>();
		ajaxMap.put("statusCode", statusCode);
		ajaxMap.put("message", message);
		ajaxMap.put("closeCurrent", closeFlag);
		this.writerResponse(response,Json.toJson(ajaxMap));
	}
	protected void ajaxDoneSuccess(HttpServletResponse response,String message,boolean closeFlag) {
		ajaxDone(response,200, message,closeFlag);
	}	
	protected void ajaxDoneSuccess(HttpServletResponse response,String message) {
		ajaxDone(response,200, message,false);
	}

	protected void ajaxDoneError(HttpServletResponse response, String message) {
		ajaxDone(response,300, message,false);
	}
	protected void ajaxDoneError(HttpServletResponse response, Exception exception) {
		
		StackTraceElement[] errArrayH = exception.getStackTrace();
		StringBuffer error = new StringBuffer();
		error.append(exception.toString()+"<br/>");
		for (int i = 1; i < 5; i++) {
			if (null != errArrayH[i]) {
				error.append(errArrayH[i].toString()+"<br/>");
			}
		}		
		ajaxDone(response,300, error.toString(),false);
	}	
	private void showError(Exception e) {
		String[] errArray = new String[10];
		StackTraceElement[] errArrayH = e.getStackTrace();
		errArray[0] = e.toString();
		for (int i = 1; i < errArray.length; i++) {
			if (null != errArrayH[i]) {
				errArray[i] = errArrayH[i - 1].toString();
			}
		}
	}
	/**
	 * 获取Session中的用户信息
	 * @param req
	 * @return
	 */
	protected SessionObj getSessionObj(HttpServletRequest req) {
		return CacheConstans.sessionMap.get(req.getSession().getId());
	}
	/**
	 * 返回结果信息
	 * @param response
	 * @param responseString
	 */
	protected void writerResponse(HttpServletResponse response,String responseString){
		try {
			response.setContentType("text/html;charset=UTF-8");
			PrintWriter writer = response.getWriter();
			writer.print(responseString);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
