package com.sinosoft.common.web.filter;

import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.ActionContext;
import org.nutz.mvc.ActionFilter;
import org.nutz.mvc.View;
import org.nutz.mvc.view.ServerRedirectView;

import com.sinosoft.domain.base.BaseUser;


/**
 * 
 * @author unhappydepig
 *
 */
public class UserLoginFilter implements ActionFilter {
    private final Log log= Logs.get();    
	public View match(ActionContext context) {
		BaseUser user = (BaseUser) context.getRequest().getSession()
				.getAttribute("userSession");
		if (user == null) {
				ServerRedirectView view = new ServerRedirectView(
						"/login");
				return view;
		}		
		context.getRequest().setAttribute("user", user);
		return null;
	}

}
