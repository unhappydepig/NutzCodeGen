package com.sinosoft.common.web.filter;

import org.nutz.dao.Dao;
import org.nutz.lang.Mirror;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.ActionContext;
import org.nutz.mvc.ActionFilter;
import org.nutz.mvc.View;
/**
 * 基础监听用来把数据库操作对象放到Action中 同时可以用来记录各种系统日志
 * @author unhappydepig 
 * @mail yqw8912@163.com
 * @time 2015-4-27 下午4:18:21
 */
public class BaseFilter implements ActionFilter {
    private final Log log= Logs.get();    
	public View match(ActionContext context) {
		Object baseAction =  context.getModule();
		Mirror mirror = Mirror.me(baseAction.getClass());
		mirror.invoke(baseAction, "setContext",context);
		mirror.invoke(baseAction, "setDao",context.getIoc().get(Dao.class));
//		System.out.println(""+new Date()+context.getRequest().getContextPath()+context.getIoc().get(Dao.class));
		
		return null;
	}
}

