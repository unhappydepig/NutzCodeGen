package com.sinosoft.common.web.view.taglib;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * 文件上传的Action
 * 
 * @author yangqunwei yqw8912@163.com
 * @time 2015-4-9 下午6:32:47
 */
public class FileTag extends TagSupport {
	/**
	 * 
	 * @author unhappydepig 
	 * @mail yqw8912@163.com
	 * @time 2015-5-26 下午2:25:17
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 上传域Id
	 */
	protected String id = "fileid";
	/**
	 * 文件大小限制
	 */
	protected int fileLimieSize = 1024000000;
	/**
	 * 上传后缀限制
	 */
	protected String fileExt = "";
	/**
	 * 自动上传
	 */
	protected boolean autoFlag = true;
	/**
	 * 成功上传后的回调
	 */
	protected String successBack;
	/**
	 * 是否
	 */
	protected boolean multiFlag = false;

	/**
	 * 文件上传图标
	 */
	protected String fileIcon = "cloud-upload";

	public int doStartTag() throws JspException {
		HttpServletRequest request = (HttpServletRequest) this.pageContext
				.getRequest();
		StringBuffer results = new StringBuffer();
		try {
			results.append("<div id=\""+id+"\" data-toggle=\"upload\" data_name =\""+id+"\"");
			results.append(" data-uploader=\""+request.getContextPath()+"/common/file/upload\" ");
			results.append(" data-file-size-limit =\""+fileLimieSize+"\"");
			results.append(" data-file-type-exts=\""+fileExt+"\"");
			results.append(" data-auto=\""+autoFlag+"\"");
			results.append(" data-multi=\""+multiFlag+"\"");
			results.append(" data-on-upload-success=\""+successBack+"\"");
			results.append(" data-icon=\""+fileIcon+"\"");
		} catch (Exception e) {
			throw new JspException(e.getMessage(), e);
		}
		results.append("/>");
		String output = results.toString();
		TagUtils.getInstance().write(pageContext, output);
		return 1;
	}

	public int doEndTag() throws JspException {
		return 6;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getFileLimieSize() {
		return fileLimieSize;
	}

	public void setFileLimieSize(int fileLimieSize) {
		this.fileLimieSize = fileLimieSize;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public boolean isAutoFlag() {
		return autoFlag;
	}

	public void setAutoFlag(boolean autoFlag) {
		this.autoFlag = autoFlag;
	}

	public String getSuccessBack() {
		return successBack;
	}

	public void setSuccessBack(String successBack) {
		this.successBack = successBack;
	}

	public boolean isMultiFlag() {
		return multiFlag;
	}

	public void setMultiFlag(boolean multiFlag) {
		this.multiFlag = multiFlag;
	}

	public String getFileIcon() {
		return fileIcon;
	}

	public void setFileIcon(String fileIcon) {
		this.fileIcon = fileIcon;
	}

}