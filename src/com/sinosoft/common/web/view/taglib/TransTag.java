package com.sinosoft.common.web.view.taglib;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sinosoft.common.util.TransUtil;
/**
 * 翻译标签
 * @author yangqunwei yqw8912@163.com
 * @time 2015-4-9  下午6:58:56
 */
public class TransTag extends TagSupport {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    /**
     * Name of the bean that contains the data we will be rendering.
     */
    protected String name = null;

    /**
     * Name of the property to be accessed on the specified bean.
     */
    protected String property = null;
    
    protected String codetype = null;

    /**
     * The scope to be searched to retrieve the specified bean.
     */
    protected String scope = null;
    // --------------------------------------------------------- Public Methods

    /**
     * Process the start tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {
        // Look up the requested property value
        Object value = TagUtils.getInstance().lookup(pageContext, name, property, scope);

        if (value == null) {
            return (SKIP_BODY); // Nothing to output
        }
        // Convert value to the String with some formatting
        String output = value.toString();
        if(null!=output){
        	if(null==codetype){
        		codetype = property;
        	}
        	output = TransUtil.code2Name(codetype, output);
        }        
        TagUtils.getInstance().write(pageContext, output);
        return (SKIP_BODY);

    }
    /**
     * Release all allocated resources.
     */
    public void release() {
        super.release();
        name = null;
        property = null;
        scope = null;
        codetype = null;
    }
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getCodetype() {
		return codetype;
	}
	public void setCodetype(String codetype) {
		this.codetype = codetype;
	}
    

}
