package com.sinosoft.common.web.view.taglib;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;

import com.sinosoft.common.util.BeanUtil;

/**
 * 标签工具类
 * @author yangqunwei yqw8912@163.com
 * @time 2015-4-9  下午6:35:06
 */
public class TagUtils {
    private static final TagUtils instance = new TagUtils();	
    private static final Map<String,Integer> scopes = new HashMap<String,Integer>();
    static {
        scopes.put("page", new Integer(PageContext.PAGE_SCOPE));
        scopes.put("request", new Integer(PageContext.REQUEST_SCOPE));
        scopes.put("session", new Integer(PageContext.SESSION_SCOPE));
        scopes.put("application", new Integer(PageContext.APPLICATION_SCOPE));        
    }    
    protected TagUtils() {
        super();
    }
    public static TagUtils getInstance() {
        return instance;
    }
    public Object lookup(PageContext pageContext, String name, String scopeName)
            throws JspException {

        if (scopeName == null) {
            return pageContext.findAttribute(name);
        }

        try {
            return pageContext.getAttribute(name, instance.getScope(scopeName));

        } catch (JspException e) {
            throw e;
        }

    }   
    public int getScope(String scopeName) throws JspException {
        Integer scope = (Integer) scopes.get(scopeName.toLowerCase());
        if (scope == null) {
            throw new JspException("没有对象的对象!");
        }

        return scope.intValue();
    }  
    public Object lookup(
            PageContext pageContext,
            String name,
            String property,
            String scope)
            throws JspException {

        // Look up the requested bean, and return if requested
        Object bean = lookup(pageContext, name, scope);
        if (bean == null) {
            JspException e = null;
            if (scope == null) {
                e = new JspException("Cannot find bean "+name+" in any scope!");
            } else {
                e = new JspException("Cannot find bean "+name+" in scope "+scope);
            }
            throw e;
        }

        if (property == null) {
            return bean;
        }
        try {
            return BeanUtil.getProperty(bean, property);
        } catch (Exception e) {
            throw new JspException("No getter method for property "+property+" of bean "+name);
        }

    }  
    public void write(PageContext pageContext, String text)
            throws JspException {

        JspWriter writer = pageContext.getOut();

        try {
            writer.print(text);

        } catch (IOException e) {
            throw new JspException("Input/output error:"+ e.toString());
        }

    }
	public void writePrevious(PageContext pageContext, String text) throws JspException {
        JspWriter writer = pageContext.getOut();
        if (writer instanceof BodyContent) {
            writer = ((BodyContent) writer).getEnclosingWriter();
        }

        try {
            writer.print(text);

        } catch (IOException e) {
            throw new JspException("Input/output error:"+ e.toString());
        }

    }
}
