package com.sinosoft.common.web.view.taglib;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import com.sinosoft.common.bean.CodeNameDto;
import com.sinosoft.common.util.BaseCommonUtil;

/**
 * 
 * @author yangqunwei yqw8912@163.com
 * @time 2015-4-9  下午6:32:47
 */
public class SelectTag extends TagSupport
{
  private static final long serialVersionUID = 1L;
  /**
   * 输入与名称
   */
  protected String inputName = null;
  /**
   * 输入域ID
   */
  protected String id = null;
  /**
   * 输入域类型
   */
  protected String codeType = null;
  /**
   * 是否显示全部
   */
  protected boolean withAll = false;
  /**
   * 是否显示请选择
   */
  protected boolean withNull = false;
  protected String name = null;  
  protected String property = null;  
  protected String scope = null;
  protected boolean hiddenCode = true;
  protected String toggle = "selectpicker";  
  protected String width = "150";   
  protected boolean multiple = false; 
  protected String hiddenArg = null;  

  
  public int doStartTag()
    throws JspException
  {
	String sessionId = this.pageContext.getSession().getId();
    HttpServletRequest request = (HttpServletRequest)this.pageContext.getRequest();
    StringBuffer results =  null;            
    try {
    	results = new StringBuffer("<select");
      results.append(" name=\"");
      results.append(inputName);
      results.append("\"");    	
      if(null!=toggle){
        results.append(" data-toggle=\"");
        results.append(toggle);
        results.append("\" ");      	
     }
      if(null!=width){
          results.append(" data-width=\"");
          results.append(width);
          results.append("\" ");      	
       } 
      if(multiple){
          results.append(" multiple ");    	
       }      
      results.append(">");
      Object value = TagUtils.getInstance().lookup(pageContext, name, property, scope);
      if(withAll) {
            results.append("<option value=\"\">" + "所有" + "</option>");
        }
      if(withNull) {
          results.append("<option value=\"\">" + "请选择" + "</option>");
      }
      List<CodeNameDto> resultCodeList = null;// 查询出的结果List
      	if(request.getAttribute(codeType + "List") != null) {
          resultCodeList = (List<CodeNameDto>)request.getAttribute(codeType + "List");
      	} else{
      		resultCodeList = BaseCommonUtil.findCodeList(codeType,sessionId);
      	}   
        if(resultCodeList != null && resultCodeList.size() > 0) {
            StringBuilder tempStr = new StringBuilder();
            for(int i = 0; i < resultCodeList.size(); i++) {
                CodeNameDto valueDto = (CodeNameDto)resultCodeList.get(i);
                String code = valueDto.getCodeCode();
                if(hiddenCodeArg(code)) {
                    continue;
                }
                tempStr.setLength(0);
                tempStr.append("<option value=\"" + code + "\"");
                if(null!=value&&code.equals(value)) {
                    tempStr.append(" selected");
                	}
                tempStr.append(">");
                if(code.trim().length() == 0) {
                    code = "&nbsp;";
                }
                // 如果不是隐藏代码时，需要将代码显示出来
                if(hiddenCode == false) {
                    tempStr.append(code);
                    tempStr.append("-");
                }
                tempStr.append(valueDto.getCodeName());
                tempStr.append("</option>");
                results.append(tempStr.toString());
            }
        }      	
    } catch (Exception e) {
      throw new JspException(e.getMessage(), e);
    }
    results.append("</select>");
    String output = results.toString();
    TagUtils.getInstance().write(pageContext, output);
    return 1;
  }
  public int doEndTag() throws JspException {
    return 6;
  }
  /**
   * 屏蔽代码
   * @param code
   * @return
   */
  private boolean hiddenCodeArg(String code) {
      String[] codeArg = null;
      if(hiddenArg != null) {
          if(hiddenArg.indexOf(",") > -1) {
              codeArg = hiddenArg.split(",");
              for(int i = 0; i < codeArg.length; i++) {
                  if(code.equals(codeArg[i])) {
                      return true;
                  }
                  else {
                      continue;
                  }
              }

          }
          else {
              if(code.equals(hiddenArg)) {
                  return true;
              }
          }
      }
      return false;
  }  
public String getInputName() {
	return inputName;
}
public void setInputName(String inputName) {
	this.inputName = inputName;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getCodeType() {
	return codeType;
}
public void setCodeType(String codeType) {
	this.codeType = codeType;
}
public boolean isWithAll() {
	return withAll;
}
public void setWithAll(boolean withAll) {
	this.withAll = withAll;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getProperty() {
	return property;
}
public void setProperty(String property) {
	this.property = property;
}
public String getScope() {
	return scope;
}
public void setScope(String scope) {
	this.scope = scope;
}
public String getToggle() {
	return toggle;
}
public void setToggle(String toggle) {
	this.toggle = toggle;
}
public String getWidth() {
	return width;
}
public void setWidth(String width) {
	this.width = width;
}

public boolean isMultiple() {
	return multiple;
}
public void setMultiple(boolean multiple) {
	this.multiple = multiple;
}
public boolean isHiddenCode() {
	return hiddenCode;
}
public String getHiddenArg() {
	return hiddenArg;
}
public void setHiddenArg(String hiddenArg) {
	this.hiddenArg = hiddenArg;
}
public void setHiddenCode(boolean hiddenCode) {
	this.hiddenCode = hiddenCode;
}
public boolean isWithNull() {
	return withNull;
}
public void setWithNull(boolean withNull) {
	this.withNull = withNull;
}
  
}