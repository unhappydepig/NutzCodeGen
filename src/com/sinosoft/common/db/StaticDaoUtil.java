package com.sinosoft.common.db;

import org.nutz.dao.Dao;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.Mvcs;

public class StaticDaoUtil {
	private static Log log = Logs.get(); 
	/**
	 * 获取数据操作对象
	 * @return
	 */
	public static Dao getDao(){
		Dao dao = getRequestDao();
		if(null==dao){
			dao =  getDefaultDao();
		}
		if(null==dao){
			dao = getFileDao();
		}
		return dao;
	}
	/**
	 * Request作用域内
	 * @return
	 */
	private static Dao getRequestDao(){
		Dao dao = null;
		try{
			dao = Mvcs.getIoc().get(Dao.class);
		}catch(Exception e){
			log.info("从Request中取值失败!");
		}
		return dao;
	}
	/**
	 * Request作用域外
	 * @return
	 */
	private static Dao getDefaultDao(){
		Dao dao = null;
		try{
			dao = Mvcs.ctx().getDefaultIoc().get(Dao.class);
		}catch(Exception e){
			log.info("从应用服务器中取值失败!");
		}
		return dao;		
	}	
	/**
	 * 从配置文件中获取数据对象
	 * @return
	 */
	private static Dao getFileDao(){
	    return FileDaoUtil.getDao();
	}
}
