package com.sinosoft.common.db;
import javax.sql.DataSource;

import org.nutz.dao.Dao;
import org.nutz.dao.impl.NutDao;
import org.nutz.ioc.Ioc;
import org.nutz.ioc.impl.NutIoc;
import org.nutz.ioc.loader.json.JsonLoader;


public  class FileDaoUtil {
	private static Ioc ioc;

	public static Dao getDao() { // 暂不考虑线程同步的问题
	    if (ioc == null)
	        ioc = new NutIoc(new JsonLoader("config/datasource.json")); 
	    DataSource ds = ioc.get(DataSource.class);
	    Dao dao = new NutDao(ds);
	    return dao;
	}
}
