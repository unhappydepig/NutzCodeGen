package com.sinosoft.common.bean;
/**
 * 通用基础代码转换对象
 * @author unhappydepig 
 * @mail yqw8912@163.com
 * @time 2015-4-27 下午5:48:58
 */
public class CodeNameDto {
	private String codeCode = null;
	private String codeName = null;
	private String flag = "";
	public String getCodeCode() {
		return codeCode;
	}
	public void setCodeCode(String codeCode) {
		this.codeCode = codeCode;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
}
