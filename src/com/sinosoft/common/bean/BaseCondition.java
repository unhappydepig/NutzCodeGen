package com.sinosoft.common.bean;

import org.nutz.dao.pager.Pager;

/**
 * 从页面获取基础查询对象-分页参数
 * @author yangqunwei yqw8912@163.com
 * @time 2015-4-8  下午4:05:32
 */
public class BaseCondition extends  Pager{
	private static final long serialVersionUID = 1L;
	private int pageCurrent = 1;
	public BaseCondition(int pz,int pn){
		super.setPageNumber(pn);
		super.setPageSize(pz);
	}
	public BaseCondition(){
	}	
	private String orderField = null;
	private String orderDirection = null;
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderDirection() {
		return orderDirection;
	}
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	public int getPageCurrent() {
		return pageCurrent;
	}
	public void setPageCurrent(int pageCurrent) {
		super.setPageNumber(pageCurrent);
		this.pageCurrent = pageCurrent;
	}
	
	
}
