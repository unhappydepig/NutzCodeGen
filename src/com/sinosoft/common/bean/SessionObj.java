package com.sinosoft.common.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sinosoft.domain.base.BaseRes;
import com.sinosoft.domain.base.BaseUser;

/**
 * Session中关联的数据
 * @author unhappydepig
 *
 */
public class SessionObj {
	/**
	 * 当前登录用户
	 */
	private BaseUser baseUser= new BaseUser();
	/**
	 * 当前用户的权限列表
	 */
	private String[] taskArray = new String[]{};
	/**
	 * 当前用户的岗位列表
	 */
	private String[] gradeArray = new String[]{};
	/**
	 * 资源列表
	 */
	private List<BaseRes> resList = new ArrayList<BaseRes>();
	/**
	 * 当前用户所能操作的机构列表Key 为权限级别 
	 */
	private Map<Integer,String[]>  comArrayMap = new HashMap<Integer,String[]>();
	
	public BaseUser getBaseUser() {
		return baseUser;
	}
	public void setBaseUser(BaseUser baseUser) {
		this.baseUser = baseUser;
	}
	public String[] getTaskArray() {
		return taskArray;
	}
	public void setTaskArray(String[] taskArray) {
		this.taskArray = taskArray;
	}
	
	public Map<Integer, String[]> getComArrayMap() {
		return comArrayMap;
	}
	public void setComArrayMap(Map<Integer, String[]> comArrayMap) {
		this.comArrayMap = comArrayMap;
	}
	public String[] getGradeArray() {
		return gradeArray;
	}
	public void setGradeArray(String[] gradeArray) {
		this.gradeArray = gradeArray;
	}
	public List<BaseRes> getResList() {
		return resList;
	}
	public void setResList(List<BaseRes> resList) {
		this.resList = resList;
	}
}
