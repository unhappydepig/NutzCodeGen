package com.sinosoft.common.bean;

import java.io.Serializable;

import org.nutz.dao.entity.annotation.PK;
import org.nutz.lang.Mirror;

import com.sinosoft.common.util.BeanUtil;
import com.sinosoft.common.util.Constans;

public class BaseDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 获取主键的值用"_"分割
	 */
	private  String PK;
	/**
	 * 获取对象主键信息
	 * @return
	 */
    public  String getPK() {
 
        Mirror<?> mirror = Mirror.me(this.getClass());
        PK annPK = mirror.getAnnotation(PK.class);
        String[] colNameArray = annPK.value();
        if(null!=colNameArray&&colNameArray.length>0){
        	if(1==colNameArray.length){        		
        		return mirror.getValue(this, colNameArray[0]).toString();
        	}
        	String pkString = "";
        	for(int i=0;i<(colNameArray.length-1);i++){
        		pkString = pkString+mirror.getValue(this, colNameArray[i]).toString()+Constans.PK_SEP;
        	}
        	pkString = pkString+mirror.getValue(this, colNameArray[(colNameArray.length-1)]).toString();
        	return pkString;
        }        
    	return null;
	}
    /**
     * 根据规则化的主键信息给实体主键赋值
     * @param id
     */
	public void setPK(String id) {
        Mirror<?> mirror = Mirror.me(this.getClass());
        PK annPK = mirror.getAnnotation(PK.class);
        String[] colNameArray = annPK.value();
        String[] colValArray = id.split(Constans.PK_SEP);
        if(null!=colNameArray&&colNameArray.length>=0){
        	if(colValArray.length==colValArray.length){
        		BeanUtil.setValue(this, colNameArray[0], colValArray[0]);        		
        		for(int i=1;i<colValArray.length;i++){
            		BeanUtil.setValue(this, colNameArray[0], colValArray[0]);
        		}
        	}
        }	
	}
    
}
