package com.sinosoft.modules.code;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.bean.CodeNameDto;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.code.CodeDbMapping;
import com.sinosoft.domain.code.CodeMapType;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-21 14:36:58 
 * 数据类型映射关系 Action
 */
@IocBean
@At("/code/CodeDbMapping")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class CodeDbMappingAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.code.listCodeDbMapping")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")CodeDbMapping codeDbMapping,HttpServletRequest req) {
		dao.create(CodeDbMapping.class, false);
		setMapTypeList(req);
		List<CodeDbMapping> codeDbMappingList = daoUtil.queryList(codeDbMapping,baseCondition);
		req.setAttribute("codeDbMappingList", codeDbMappingList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("codeDbMapping", codeDbMapping);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	private void setMapTypeList(HttpServletRequest req) {
		List<CodeMapType> mapTypeIdList = daoUtil.queryList(new CodeMapType());
		List<CodeNameDto> codeList = new ArrayList<CodeNameDto>();
		for(int i=0;i<mapTypeIdList.size();i++){
			CodeMapType codeMapType= mapTypeIdList.get(i);
			CodeNameDto  codeNameDto= new CodeNameDto();
			codeNameDto.setCodeCode(codeMapType.getMapTypeId());
			codeNameDto.setCodeName(codeMapType.getMapName());
			codeList.add(codeNameDto);
		}
		req.setAttribute("mapTypeIdList", codeList);//查询结果返回页面 		
	}
	//准备新增
	@At
	@Ok("jsp:jsp.code.editCodeDbMapping")
	public void toadd(HttpServletRequest req,@Param("..")CodeDbMapping codeDbMapping) {
		setMapTypeList(req);
		req.setAttribute("codeDbMapping",codeDbMapping);
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")CodeDbMapping codeDbMapping) {
		try {
			daoUtil.add(codeDbMapping);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.code.editCodeDbMapping")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		setMapTypeList(req);
		req.setAttribute("codeDbMapping",daoUtil.detailByPK(CodeDbMapping.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")CodeDbMapping codeDbMapping) {
		try {
			daoUtil.update(codeDbMapping);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(CodeDbMapping.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(CodeDbMapping.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.code.editCodeDbMapping")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("codeDbMapping",daoUtil.detailByPK(CodeDbMapping.class,id));
		req.setAttribute("actionType", "view");
	}
}