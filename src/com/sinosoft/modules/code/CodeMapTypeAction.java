package com.sinosoft.modules.code;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.code.CodeDbMapping;
import com.sinosoft.domain.code.CodeMapType;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-21 14:36:58 
 * 数据类型映射类型 Action
 */
@IocBean
@At("/code/CodeMapType")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class CodeMapTypeAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.code.listCodeMapType")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")CodeMapType codeMapType,HttpServletRequest req) {
		dao.create(CodeMapType.class, false);
		codeMapType.setCreateBy(daoUtil.getUserID());
		baseCondition.setOrderField("mapName");
		List<CodeMapType> codeMapTypeList = daoUtil.queryList(codeMapType,baseCondition);
		req.setAttribute("codeMapTypeList", codeMapTypeList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("codeMapType", codeMapType);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//包含的子信息列表
	@At
	@Ok("jsp:jsp.code.listCodeMapTypeItem")
	public void subList(@Param("mapTypeId")String mapTypeId,HttpServletRequest req) {
		List<CodeDbMapping> dbMappingList= daoUtil.getDao().query(CodeDbMapping.class,Cnd.where("mapTypeId","=",mapTypeId).asc("coverLevel"));
		req.setAttribute("dbMappingList", dbMappingList);
	}	
	//准备新增
	@At
	@Ok("jsp:jsp.code.editCodeMapType")
	public void toadd(HttpServletRequest req,@Param("..")CodeMapType codeMapType) {
		req.setAttribute("codeMapType", codeMapType);
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")CodeMapType codeMapType) {
		try {
			daoUtil.add(codeMapType);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.code.editCodeMapType")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("codeMapType",daoUtil.detailByPK(CodeMapType.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")CodeMapType codeMapType) {
		try {
			daoUtil.update(codeMapType);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(CodeMapType.class,idArray[i]);
				daoUtil.delete(CodeDbMapping.class, Cnd.where("mapTypeId", "=", idArray[i]));
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(CodeMapType.class,id);
				daoUtil.delete(CodeDbMapping.class, Cnd.where("mapTypeId", "=", id));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.code.editCodeMapType")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("codeMapType",daoUtil.detailByPK(CodeMapType.class,id));
		req.setAttribute("actionType", "view");
	}
	//批量复制
	@At
	@Ok("jsp:jsp.code.listCodeMapTypeCopy")
	public void toCopy(@Param("..")BaseCondition baseCondition,@Param("..")CodeMapType codeMapType,HttpServletRequest req) {
		dao.create(CodeMapType.class, false);
		List<CodeMapType> codeMapTypeList = daoUtil.queryList(codeMapType,baseCondition);
		for(int i=0;i<codeMapTypeList.size();i++){
			CodeMapType mapType = codeMapTypeList.get(i);
			mapType.setDbMappingList(daoUtil.getDao().query(CodeDbMapping.class,Cnd.where("mapTypeId","=",mapType.getMapTypeId()).asc("coverLevel")));
		}
		req.setAttribute("codeMapTypeList", codeMapTypeList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("codeMapType", codeMapType);//返回查询条件鞭面页面跳转后查询条件丢失
	}
}