package com.sinosoft.modules.code;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.bean.CodeNameDto;
import com.sinosoft.common.util.Constans;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.code.CodeDbConfig;
import com.sinosoft.domain.code.CodeDbMapping;
import com.sinosoft.domain.code.CodeMapType;
import com.sinosoft.domain.code.CodePlan;
import com.sinosoft.domain.code.CodePlanItem;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-20 16:57:31 
 * 代码生成方案-t_code_plan Action
 */
@IocBean
@At("/code/CodePlan")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class CodePlanAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.code.listCodePlan")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")CodePlan codePlan,HttpServletRequest req) {
		System.out.println(daoUtil);
		dao.create(CodePlan.class, false);
		daoUtil.setPowerLevel(Constans.P_USER);
		List<CodePlan> codePlanList = daoUtil.queryList(codePlan,baseCondition);
		req.setAttribute("codePlanList", codePlanList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("codePlan", codePlan);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//复制列表
	@At
	@Ok("jsp:jsp.code.listCodePlanCopy")
	public void toCopy(@Param("..")BaseCondition baseCondition,HttpServletRequest req) {
		dao.create(CodePlan.class, false);
		Cnd cnd = Cnd.where("createBy","=",daoUtil.getUserID()).or("shareFlag", "=", "Y");
		List<CodePlan> codePlanList = daoUtil.getDao().query(CodePlan.class,cnd, baseCondition);
		baseCondition.setRecordCount(daoUtil.getDao().count(CodePlan.class, cnd));
		req.setAttribute("codePlanList", codePlanList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void copy(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
			//复制主表数据
			CodePlan codePlan = daoUtil.detailByPK(CodePlan.class,id);
			codePlan.setPlanId(null);
			daoUtil.add(codePlan);
			String planId = codePlan.getPlanId();
			//复制文件模板
			CodePlanItem codePlanItem= new CodePlanItem();
			codePlanItem.setPlanId(id);
			List<CodePlanItem> planItemList =  daoUtil.queryList(codePlanItem);
			for(int i=0;i<planItemList.size();i++){
				CodePlanItem planItme = planItemList.get(i);
				planItme.setPlanId(planId);
				planItme.setPlanItemId(null);
				daoUtil.add(planItme);
			}
			//复制数据模板
			CodeMapType codeMapType= new CodeMapType();
			codeMapType.setPlanId(id);
			List<CodeMapType> mapTypeList =  daoUtil.queryList(codeMapType);
			for(int i=0;i<mapTypeList.size();i++){
				CodeMapType mapType = mapTypeList.get(i);
				String mapTypeIdOld =  mapType.getMapTypeId();
				mapType.setPlanId(planId);
				mapType.setMapTypeId(null); 
				daoUtil.add(mapType);
				String mapTypeIdNew = mapType.getMapTypeId();
				//复制数据模板明细
				CodeDbMapping codeDbMapping = new CodeDbMapping();
				codeDbMapping.setMapTypeId(mapTypeIdOld);
				List<CodeDbMapping> dbMappingList =  daoUtil.queryList(codeDbMapping);
				for(int k=0;k<dbMappingList.size();k++){
					CodeDbMapping dbMap = dbMappingList.get(k);
					dbMap.setMapTypeId(mapTypeIdNew);
					dbMap.setDbMapId(null);
					daoUtil.add(dbMap);
				}
				
			}			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备新增
	@At
	@Ok("jsp:jsp.code.editCodePlan")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("dbMapList", getCoverType(null,"NoPlanId"));
		req.setAttribute("codePlan", new CodePlan());
		req.setAttribute("actionType", "add");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")CodePlan codePlan) {
		try {
			daoUtil.add(codePlan);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.code.editCodePlan")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		CodePlan codePlan = daoUtil.detailByPK(CodePlan.class,id);
		req.setAttribute("codePlan",codePlan);
		req.setAttribute("dbMapList", getCoverType(codePlan.getDbMap(),codePlan.getPlanId()));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")CodePlan codePlan) {
		try {
			daoUtil.update(codePlan);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				String id = idArray[i];
				daoUtil.deleteByPK(CodePlan.class,id);
				//删除文件模板
				daoUtil.delete(CodePlanItem.class,Cnd.where("planId", "=", id));
				//删除数据模板
				CodeMapType codeMapType= new CodeMapType();
				codeMapType.setPlanId(id);
				List<CodeMapType> mapTypeList =  daoUtil.queryList(codeMapType);
				for(int k=0;k<mapTypeList.size();k++){
					CodeMapType mapType = mapTypeList.get(k);		
					daoUtil.delete(CodeDbMapping.class, Cnd.where("mapTypeId", "=", mapType.getMapTypeId()));
					daoUtil.deleteByPK(CodeDbMapping.class,mapType.getMapTypeId());
				}					
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(CodePlan.class,id);
				//删除文件模板
				daoUtil.delete(CodePlanItem.class,Cnd.where("planId", "=", id));
				//删除数据模板
				CodeMapType codeMapType= new CodeMapType();
				codeMapType.setPlanId(id);
				List<CodeMapType> mapTypeList =  daoUtil.queryList(codeMapType);
				for(int i=0;i<mapTypeList.size();i++){
					CodeMapType mapType = mapTypeList.get(i);		
					daoUtil.delete(CodeDbMapping.class, Cnd.where("mapTypeId", "=", mapType.getMapTypeId()));
					daoUtil.deleteByPK(CodeDbMapping.class,mapType.getMapTypeId());
				}	
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.code.editCodePlan")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("codePlan",daoUtil.detailByPK(CodePlan.class,id));
		req.setAttribute("actionType", "view");
	}
	/**
	 * 挑换到代码生成页面
	 * @param req
	 * @param id
	 */
	@At
	@Ok("jsp:jsp.code.genCode")
	public void toGenerate(HttpServletRequest req,@Param("id") String id) {
		CodePlan codePlan = daoUtil.detailByPK(CodePlan.class,id);
		req.setAttribute("codePlan",codePlan);
		req.setAttribute("dbMapList", getCoverType(codePlan.getDbMap(),codePlan.getPlanId()));
		req.setAttribute("codePlan",codePlan);
		CodePlanItem codePlanItem = new CodePlanItem();
		codePlanItem.setValid("Y");
		codePlanItem.setPlanId(id);
		List<CodePlanItem> planItemList = daoUtil.queryList(codePlanItem); 
		req.setAttribute("planItemList",planItemList );		
		//获取可用的数据连接
		CodeDbConfig  codeDbConfig = new CodeDbConfig();
		codeDbConfig.setCreateBy(daoUtil.getUserID());
		req.setAttribute("dbConfigList",daoUtil.queryList(codeDbConfig));
	}
	
	/**
	 * 获取转换类型
	 * @param selected
	 * @return
	 */
	public  List<CodeNameDto> getCoverType(String selected,String planId){
		CodeMapType mapType = new CodeMapType();
		mapType.setPlanId(planId);
		List<CodeMapType> list = daoUtil.queryList(mapType);
		List<CodeNameDto> codeNameList = new ArrayList<CodeNameDto>();
		if(null!=list&&list.size()>0){
			for(int i=0;i<list.size();i++){
				CodeMapType coverType= list.get(i);
				CodeNameDto codeNameDto = new CodeNameDto();
				codeNameDto.setCodeCode(coverType.getMapTypeId());
				codeNameDto.setCodeName(coverType.getMapName());
				if(null!=selected&&selected.indexOf(coverType.getMapTypeId())>-1){
					codeNameDto.setFlag("selected");
				}				
				codeNameList.add(codeNameDto);
			}
		}	
		return codeNameList;
	}
}