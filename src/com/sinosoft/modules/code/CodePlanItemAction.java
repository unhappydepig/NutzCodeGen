package com.sinosoft.modules.code;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.code.CodePlanItem;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-20 14:17:07 
 * 方案明细-t_code_plan_item Action
 */
@IocBean
@At("/code/CodePlanItem")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class CodePlanItemAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.code.listCodePlanItem")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")CodePlanItem codePlanItem,HttpServletRequest req) {
		dao.create(CodePlanItem.class, false);
		List<CodePlanItem> codePlanItemList = daoUtil.queryList(codePlanItem,baseCondition);
		req.setAttribute("codePlanItemList", codePlanItemList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("codePlanItem", codePlanItem);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.code.editCodePlanItem")
	public void toadd(HttpServletRequest req,@Param("..")CodePlanItem codePlanItem,@Param("planId") String planId) {
		codePlanItem.setPlanId(planId);
		req.setAttribute("codePlanItem",codePlanItem);
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")CodePlanItem codePlanItem) {
		try {
			daoUtil.add(codePlanItem);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.code.editCodePlanItem")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("codePlanItem",daoUtil.detailByPK(CodePlanItem.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")CodePlanItem codePlanItem) {
		try {
			daoUtil.update(codePlanItem);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(CodePlanItem.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(CodePlanItem.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.code.editCodePlanItem")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("codePlanItem",daoUtil.detailByPK(CodePlanItem.class,id));
		req.setAttribute("actionType", "view");
	}
}