package com.sinosoft.modules.mem;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.mem.MemExt;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-04-29 23:07:49 
 * 会员扩展信息-t_mem_ext Action
 */
@IocBean
@At("/mem/MemExt")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class MemExtAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.mem.listMemExt")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")MemExt memExt,HttpServletRequest req) {
		dao.create(MemExt.class, false);
		List<MemExt> memExtList = daoUtil.queryList(memExt,baseCondition);
		req.setAttribute("memExtList", memExtList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("memExt", memExt);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.mem.editMemExt")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("memExt", new MemExt());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")MemExt memExt) {
		try {
			daoUtil.add(memExt);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.mem.editMemExt")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("memExt",daoUtil.detailByPK(MemExt.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")MemExt memExt) {
		try {
			daoUtil.update(memExt);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(MemExt.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(MemExt.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.mem.editMemExt")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("memExt",daoUtil.detailByPK(MemExt.class,id));
		req.setAttribute("actionType", "view");
	}
}