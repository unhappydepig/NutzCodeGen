package com.sinosoft.modules.mem;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.mem.MemLevel;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-04-29 23:07:49 
 * 会员等级信息表-t_mem_level Action
 */
@IocBean
@At("/mem/MemLevel")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class MemLevelAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.mem.listMemLevel")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")MemLevel memLevel,HttpServletRequest req) {
		dao.create(MemLevel.class, false);
		List<MemLevel> memLevelList = daoUtil.queryList(memLevel,baseCondition);
		req.setAttribute("memLevelList", memLevelList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("memLevel", memLevel);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.mem.editMemLevel")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("memLevel", new MemLevel());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")MemLevel memLevel) {
		try {
			daoUtil.add(memLevel);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.mem.editMemLevel")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("memLevel",daoUtil.detailByPK(MemLevel.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")MemLevel memLevel) {
		try {
			daoUtil.update(memLevel);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(MemLevel.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(MemLevel.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.mem.editMemLevel")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("memLevel",daoUtil.detailByPK(MemLevel.class,id));
		req.setAttribute("actionType", "view");
	}
}