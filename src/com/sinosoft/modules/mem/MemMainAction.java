package com.sinosoft.modules.mem;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.mem.MemMain;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-04-29 23:07:49 
 * 会员信息表-t_mem_main Action
 */
@IocBean
@At("/mem/MemMain")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class MemMainAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.mem.listMemMain")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")MemMain memMain,HttpServletRequest req) {
		dao.create(MemMain.class, false);
		List<MemMain> memMainList = daoUtil.queryList(memMain,baseCondition);
		req.setAttribute("memMainList", memMainList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("memMain", memMain);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.mem.editMemMain")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("memMain", new MemMain());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")MemMain memMain) {
		try {
			daoUtil.add(memMain);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.mem.editMemMain")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("memMain",daoUtil.detailByPK(MemMain.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")MemMain memMain) {
		try {
			daoUtil.update(memMain);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(MemMain.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(MemMain.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.mem.editMemMain")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("memMain",daoUtil.detailByPK(MemMain.class,id));
		req.setAttribute("actionType", "view");
	}
}