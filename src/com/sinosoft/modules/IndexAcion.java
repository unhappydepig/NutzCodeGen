package com.sinosoft.modules;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;

import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.SessionObj;
import com.sinosoft.common.util.CacheConstans;
import com.sinosoft.common.util.MD5Util;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseUser;

/**
 * 总页面所使用的Action
 * 
 * @author unhappydepig
 * @mail yqw8912@163.com
 * @time 2015-4-7 下午5:38:30
 */
@At("/")
@Filters({ @By(type = UserLoginFilter.class), @By(type = BaseFilter.class) })
public class IndexAcion extends BaseAction {
	@At("admin")
	@Ok("jsp:jsp.index")
	public void home(HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws Exception {
		this.index(req, res, session);
	}

	@At("/")
	@Ok("jsp:jsp.index")
	public void index(HttpServletRequest req, HttpServletResponse res,
			HttpSession session) {
		SessionObj sessionObj = CacheConstans.sessionMap.get(req.getSession().getId());
		req.setAttribute("resList", sessionObj.getResList());
//		System.out.println(dao);
	}
	@At
	@Ok("jsp:jsp.welcome")
	public void welcome(HttpServletRequest req, HttpServletResponse res,
			HttpSession session) {
	}
	//准备修改密码
	@At
	@Ok("jsp:jsp.common.changepwd")
	public void toChangepwd(HttpServletRequest req, HttpServletResponse res,
			HttpSession session) {
		
	}	
	/**
	 * 修改密码
	 * @param req
	 */
	@At
	public void changepwd(HttpServletRequest req,HttpServletResponse response,String oldPass,String newPass) {
		String errorMessage = null;
		SessionObj obj = this.getSessionObj(req);
		BaseUser user = obj.getBaseUser();
		if(MD5Util.MD5(oldPass).equals(user.getPassword())){
			user.setPassword(MD5Util.MD5(newPass));
			daoUtil.update(user);
		}else{
			errorMessage = "旧密码输入错误!";
		}
		if(null!=errorMessage&&!"".equals(errorMessage)){
			ajaxDoneError(response,errorMessage);
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
}
