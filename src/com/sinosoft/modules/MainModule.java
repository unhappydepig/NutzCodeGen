package com.sinosoft.modules;

import org.nutz.mvc.adaptor.PairAdaptor;
import org.nutz.mvc.annotation.IocBy;
import org.nutz.mvc.annotation.Modules;
import org.nutz.mvc.annotation.AdaptBy;
import org.nutz.mvc.ioc.provider.ComboIocProvider;

/**
 * Nutz主入口
 * @author unhappydepig
 *
 */
@Modules(scanPackage=true)
@IocBy(type=ComboIocProvider.class,args={
	"*org.nutz.ioc.loader.json.JsonLoader",
	"config",
	"*org.nutz.ioc.loader.annotation.AnnotationIocLoader",
	"com.sinosoft"})
@AdaptBy(type=PairAdaptor.class)
public class MainModule {
	
}
