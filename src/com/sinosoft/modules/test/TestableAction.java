package com.sinosoft.modules.test;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.test.Testable;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-06-26 16:14:34.115 
 * 测试表Action
 */
@IocBean
@At("/test/Testable")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class TestableAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.test.listTestable")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")Testable testable,HttpServletRequest req) {
		dao.create(Testable.class, false);
		List<Testable> testableList = daoUtil.queryList(testable,baseCondition);
		req.setAttribute("testableList", testableList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("testable", testable);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.test.editTestable")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("testable", new Testable());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")Testable testable) {
		try {
			daoUtil.add(testable);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.test.editTestable")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("testable",daoUtil.detailByPK(Testable.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")Testable testable) {
		try {
			daoUtil.update(testable);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(Testable.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(Testable.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.test.editTestable")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("testable",daoUtil.detailByPK(Testable.class,id));
		req.setAttribute("actionType", "view");
	}
}