package com.sinosoft.modules;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.nutz.lang.Strings;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.SessionObj;
import com.sinosoft.common.util.CacheConstans;
import com.sinosoft.common.util.MD5Util;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.domain.base.BaseUser;
import com.sinosoft.domain.base.BaseUserGrade;
import com.sinosoft.service.base.BaseResService;
/**
 * 登录使用的Action
 * @author unhappydepig 
 * @mail yqw8912@163.com
 * @time 2015-4-7 下午5:38:30
 */
@At("/login")
@Filters({ @By(type = BaseFilter.class) })
public class LoginAcion  extends BaseAction{
    @At("")
    @Ok("jsp:jsp.login")  
    public void login(HttpServletRequest req, HttpServletResponse res,
	    HttpSession session) {	
    } 
    @At("/doLogin")  
    @Ok("raw:html")    
    public String dologin(@Param("j_username") String j_username,
            @Param("j_password") String j_password,
            @Param("j_captcha") String j_captcha, HttpSession session,
            HttpServletRequest req) {
        if (Strings.isBlank(j_username) || Strings.isBlank(j_password)){
            return "用户名及密码不能为空！";
        }
        String vcode = Strings.sNull(session.getAttribute("ValidateCode"));
		if (!vcode.equals(j_captcha)){
			return "验证码不正确！"; 
		}
		BaseUser baseUser = new BaseUser();
		baseUser.setUserCode(j_username);
		BaseUser user = daoUtil.detailByContion(baseUser);
        if (user == null){
            return "用户名不存在！";
        }
        if(!user.getPassword().equals(MD5Util.MD5(j_password))){
            return "密码错误！";        	
        }
        String userId = user.getUserId();
        session.setAttribute("userSession", user);
        SessionObj sessionObj= new SessionObj();
        sessionObj.setBaseUser(user);
        //岗位信息Begin
        BaseUserGrade userGrade = new BaseUserGrade();
        userGrade.setUserId(userId);
        List<BaseUserGrade> userGradeList = daoUtil.queryList(userGrade);
        String[] gradeArray = null;
        if(null!=userGradeList&&userGradeList.size()>0){
        	gradeArray = new String[userGradeList.size()];
        	for(int i=0;i<userGradeList.size();i++){
        		gradeArray[i] = userGradeList.get(i).getGradeId();        		
        	}
        }
        sessionObj.setGradeArray(gradeArray);
        //岗位信息End        
        //资源信息Begin
        BaseResService resServer = new BaseResService(daoUtil);
        sessionObj.setResList(resServer.getResList(gradeArray));
        //资源信息End
        CacheConstans.sessionMap.put(req.getSession().getId(), sessionObj);	
        //授权数据机构End        
    	return "true";
    }   
    @At
    @Ok("jsp:jsp.login")  
    public void loginOut(HttpServletRequest req, HttpServletResponse res,
	    HttpSession session) {	    	
        session.removeAttribute("userSession");
        CacheConstans.sessionMap.remove(req.getSession().getId());	
    }     
}
