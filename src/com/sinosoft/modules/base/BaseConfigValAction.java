package com.sinosoft.modules.base;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseConfigVal;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-05 16:06:53 
 * t_base_config_val-配置值表 Action
 */
@IocBean
@At("/base/BaseConfigVal")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseConfigValAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseConfigVal")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")BaseConfigVal baseConfigVal,HttpServletRequest req) {
		dao.create(BaseConfigVal.class, false);
		List<BaseConfigVal> baseConfigValList = daoUtil.queryList(baseConfigVal,baseCondition);
		req.setAttribute("baseConfigValList", baseConfigValList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseConfigVal", baseConfigVal);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseConfigVal")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("baseConfigVal", new BaseConfigVal());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseConfigVal baseConfigVal) {
		try {
			daoUtil.add(baseConfigVal);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseConfigVal")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseConfigVal",daoUtil.detailByPK(BaseConfigVal.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseConfigVal baseConfigVal) {
		try {
			daoUtil.update(baseConfigVal);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseConfigVal.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseConfigVal.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseConfigVal")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseConfigVal",daoUtil.detailByPK(BaseConfigVal.class,id));
		req.setAttribute("actionType", "view");
	}
}