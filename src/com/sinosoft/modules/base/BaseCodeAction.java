package com.sinosoft.modules.base;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseCode;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 23:06:25 
 * 基础代码表-t_base_code Action
 */
@IocBean
@At("/base/BaseCode")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseCodeAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseCode")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")BaseCode baseCode,HttpServletRequest req) {
		dao.create(BaseCode.class, false);
		baseCode.setUpCodeId("0000");
		List<BaseCode> baseCodeList = daoUtil.queryList(baseCode,baseCondition);
		req.setAttribute("baseCodeList", baseCodeList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseCode", baseCode);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//查询子信息列表
	@At
	@Ok("jsp:jsp.base.listBaseCodeSub")
	public void subList(@Param("UpCodeId")String UpCodeId,HttpServletRequest req) {
		dao.create(BaseCode.class, false);
		BaseCode baseCode = new BaseCode();
		baseCode.setUpCodeId(UpCodeId);
		List<BaseCode> baseCodeList = daoUtil.queryList(baseCode);
		req.setAttribute("baseCodeList", baseCodeList);//查询结果返回页面 
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseCode")
	public void toadd(HttpServletRequest req,@Param("..")BaseCode baseCode) {
		req.setAttribute("baseCode", baseCode);
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseCode baseCode) {
		try {
			daoUtil.add(baseCode);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseCode")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseCode",daoUtil.detailByPK(BaseCode.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseCode baseCode) {
		try {
			daoUtil.update(baseCode);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseCode.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseCode.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseCode")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseCode",daoUtil.detailByPK(BaseCode.class,id));
		req.setAttribute("actionType", "view");
	}
}