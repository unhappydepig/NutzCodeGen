package com.sinosoft.modules.base;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseTableExt;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:25 
 * 数据增强表t_base_table_ext Action
 */
@IocBean
@At("/base/BaseTableExt")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseTableExtAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseTableExt")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")BaseTableExt baseTableExt,HttpServletRequest req) {
		dao.create(BaseTableExt.class, false);
		List<BaseTableExt> baseTableExtList = daoUtil.queryList(baseTableExt,baseCondition);
		req.setAttribute("baseTableExtList", baseTableExtList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseTableExt", baseTableExt);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseTableExt")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("baseTableExt", new BaseTableExt());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseTableExt baseTableExt) {
		try {
			daoUtil.add(baseTableExt);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseTableExt")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseTableExt",daoUtil.detailByPK(BaseTableExt.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseTableExt baseTableExt) {
		try {
			daoUtil.update(baseTableExt);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseTableExt.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseTableExt.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseTableExt")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseTableExt",daoUtil.detailByPK(BaseTableExt.class,id));
		req.setAttribute("actionType", "view");
	}
}