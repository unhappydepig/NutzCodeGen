package com.sinosoft.modules.base;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseLisitem;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:25 
 * 自定义查询结果-t_base_list_item Action
 */
@IocBean
@At("/base/BaseLisitem")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseLisitemAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseLisitem")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")BaseLisitem baseLisitem,HttpServletRequest req) {
		dao.create(BaseLisitem.class, false);
		List<BaseLisitem> baseLisitemList = daoUtil.queryList(baseLisitem,baseCondition);
		req.setAttribute("baseLisitemList", baseLisitemList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseLisitem", baseLisitem);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseLisitem")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("baseLisitem", new BaseLisitem());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseLisitem baseLisitem) {
		try {
			daoUtil.add(baseLisitem);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseLisitem")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseLisitem",daoUtil.detailByPK(BaseLisitem.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseLisitem baseLisitem) {
		try {
			daoUtil.update(baseLisitem);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseLisitem.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseLisitem.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseLisitem")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseLisitem",daoUtil.detailByPK(BaseLisitem.class,id));
		req.setAttribute("actionType", "view");
	}
}