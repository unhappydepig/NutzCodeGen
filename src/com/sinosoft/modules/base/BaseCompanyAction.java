package com.sinosoft.modules.base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseCompany;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 15:45:17 
 * 机构信息表-t_base_company Action
 */
@IocBean
@At("/base/BaseCompany")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseCompanyAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseCompany")
	public void list(HttpServletRequest req) {
		daoUtil.setPowerFlag(true);
		req.setAttribute("baseCompanyList",daoUtil.queryList(new BaseCompany()));//查询结果返回页面 
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseCompany")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("baseCompany", new BaseCompany());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseCompany baseCompany) {
		try {
			baseCompany.setUpComId(req.getParameter("up.comId"));
			daoUtil.add(baseCompany);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseCompany")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseCompany",daoUtil.detailByPK(BaseCompany.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseCompany baseCompany) {
		try {
			baseCompany.setUpComId(req.getParameter("up.comId"));
			daoUtil.update(baseCompany);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseCompany.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseCompany.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseCompany")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseCompany",daoUtil.detailByPK(BaseCompany.class,id));
		req.setAttribute("actionType", "view");
	}
}