package com.sinosoft.modules.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.dao.Cnd;
import org.nutz.dao.sql.Criteria;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.util.MD5Util;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseCompany;
import com.sinosoft.domain.base.BaseGrade;
import com.sinosoft.domain.base.BaseUser;
import com.sinosoft.domain.base.BaseUserCompany;
import com.sinosoft.domain.base.BaseUserGrade;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:26 
 * 员工信息-t_base_user Action
 */
@IocBean
@At("/base/BaseUser")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseUserAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseUser")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")BaseUser baseUser,HttpServletRequest req) {
		dao.create(BaseUser.class, false);
		daoUtil.setPowerFlag(true);
		List<BaseUser> baseUserList = daoUtil.queryList(baseUser,baseCondition);
		req.setAttribute("baseUserList", baseUserList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseUser", baseUser);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseUser")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("baseUser", new BaseUser());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseUser baseUser) {
		try {
			baseUser.setComId(req.getParameter("com.comId"));
			baseUser.setDeptId(req.getParameter("dept.comId"));
			baseUser.setPassword(MD5Util.MD5(baseUser.getPassword()));
			daoUtil.add(baseUser);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseUser")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseUser",daoUtil.detailByPK(BaseUser.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseUser baseUser) {
		try {
			baseUser.setComId(req.getParameter("com.comId"));
			baseUser.setDeptId(req.getParameter("dept.comId"));
			String oldPassword = req.getParameter("oldPassword");
			if(!oldPassword.equals(baseUser.getPassword())){
				baseUser.setPassword(MD5Util.MD5(baseUser.getPassword()));
			}
			
			
			daoUtil.update(baseUser);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseUser.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseUser.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseUser")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseUser",daoUtil.detailByPK(BaseUser.class,id));
		req.setAttribute("actionType", "view");
	}
	//准备更新
	@At
	@Ok("jsp:jsp.base.gradeBaseUser")
	public void tograde(HttpServletRequest req,@Param("id") String id) {
		BaseUserGrade userGrade = new BaseUserGrade();
		userGrade.setUserId(id);
		Map<String,String> userGradeMap = new HashMap<String,String>();
		List<BaseUserGrade> userGradeList = daoUtil.queryList(userGrade);
		if(null!=userGradeList&&userGradeList.size()>=0){
			for(int i=0;i<userGradeList.size();i++){
				userGradeMap.put(userGradeList.get(i).getGradeId(), "");
			}
		}		
		BaseGrade gradeContion = new BaseGrade();
		gradeContion.setValid("Y");
		List<BaseGrade> gradeList = daoUtil.queryList(gradeContion);
		for(int i=0;i<gradeList.size();i++){
			BaseGrade grade = gradeList.get(i);
			if(userGradeMap.containsKey(grade.getGradeId())){
				grade.setValid("Y");
			}else{
				grade.setValid("N");
			}
		}
		req.setAttribute("userId", id);
		req.setAttribute("gradeList", gradeList);
	}
	/**
	 * 保存岗位信息
	 * @param req
	 */
	@At	
	public void savegrade(HttpServletRequest req,HttpServletResponse response) {
		try {
			String[] gradeIds = req.getParameterValues("gradeId");
			String userId = req.getParameter("userId");
			Criteria cri = Cnd.cri() ;
			cri.where().and("userId", "=", userId);
			daoUtil.getDao().clear(BaseUserGrade.class, cri);
			if(null!=gradeIds&&gradeIds.length>0){
				for(int i=0;i<gradeIds.length;i++){
					BaseUserGrade userGrade = new BaseUserGrade();
					userGrade.setUserId(userId);
					userGrade.setGradeId(gradeIds[i]);
					daoUtil.add(userGrade);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//准备更新
	@At
	@Ok("jsp:jsp.base.companyBaseUser")
	public void tocompany(HttpServletRequest req,@Param("id") String id) {
		BaseUserCompany userCompany = new BaseUserCompany();
		userCompany.setUserId(id);
		Map<String,String> userCompanyMap = new HashMap<String,String>();
		List<BaseUserCompany> userCompanyList = daoUtil.queryList(userCompany);
		if(null!=userCompanyList&&userCompanyList.size()>=0){
			for(int i=0;i<userCompanyList.size();i++){
				userCompanyMap.put(userCompanyList.get(i).getComId(), "");
			}
		}		
		BaseCompany companyContion = new BaseCompany();
		companyContion.setValid("Y");
		List<BaseCompany> companyList = daoUtil.queryList(companyContion);
		for(int i=0;i<companyList.size();i++){
			BaseCompany company = companyList.get(i);
			if(userCompanyMap.containsKey(company.getComId())){
				company.setValid("Y");
			}else{
				company.setValid("N");
			}
		}
		req.setAttribute("userId", id);
		req.setAttribute("companyList", companyList);
	}
	/**
	 * 保存岗位信息
	 * @param req
	 */
	@At	
	public void savecompany(HttpServletRequest req,HttpServletResponse response) {
		try {
			String[] comIds = req.getParameterValues("comId");
			String userId = req.getParameter("userId");
			Criteria cri = Cnd.cri() ;
			cri.where().and("userId", "=", userId);
			daoUtil.getDao().clear(BaseUserCompany.class, cri);
			if(null!=comIds&&comIds.length>0){
				for(int i=0;i<comIds.length;i++){
					BaseUserCompany userCompany = new BaseUserCompany();
					userCompany.setUserId(userId);
					userCompany.setComId(comIds[i]);
					daoUtil.add(userCompany);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
}