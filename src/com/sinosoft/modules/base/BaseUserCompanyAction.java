package com.sinosoft.modules.base;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseUserCompany;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-03 11:24:15 
 * 员工权限信息-t_base_user_company Action
 */
@IocBean
@At("/base/BaseUserCompany")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseUserCompanyAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseUserCompany")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")BaseUserCompany baseUserCompany,HttpServletRequest req) {
		dao.create(BaseUserCompany.class, false);
		List<BaseUserCompany> baseUserCompanyList = daoUtil.queryList(baseUserCompany,baseCondition);
		req.setAttribute("baseUserCompanyList", baseUserCompanyList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseUserCompany", baseUserCompany);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseUserCompany")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("baseUserCompany", new BaseUserCompany());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseUserCompany baseUserCompany) {
		try {
			daoUtil.add(baseUserCompany);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseUserCompany")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseUserCompany",daoUtil.detailByPK(BaseUserCompany.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseUserCompany baseUserCompany) {
		try {
			daoUtil.update(baseUserCompany);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseUserCompany.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseUserCompany.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseUserCompany")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseUserCompany",daoUtil.detailByPK(BaseUserCompany.class,id));
		req.setAttribute("actionType", "view");
	}
}