package com.sinosoft.modules.base;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseRes;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-06-05 09:56:59.578 
 * 资源信息Action
 */
@IocBean
@At("/base/BaseRes")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseResAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseRes")
	public void list(HttpServletRequest req) {
//		dao.create(BaseRes.class, false);
		setResList(req);
//		List<BaseRes> baseResList = daoUtil.queryList(new BaseRes());
//		req.setAttribute("baseResList", baseResList);//查询结果返回页面 
//		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
//		req.setAttribute("baseRes", baseRes);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	private void setResList(HttpServletRequest req){
		List<BaseRes> baseResList = daoUtil.queryList(new BaseRes());
		req.setAttribute("baseResList", baseResList);//查询结果返回页面 
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseRes")
	public void toadd(HttpServletRequest req) {
		setResList(req);
		req.setAttribute("baseRes", new BaseRes());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseRes baseRes) {
		try {
			daoUtil.add(baseRes);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseRes")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		setResList(req);
		req.setAttribute("baseRes",daoUtil.detailByPK(BaseRes.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseRes baseRes) {
		try {
			daoUtil.update(baseRes);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseRes.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseRes.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseRes")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseRes",daoUtil.detailByPK(BaseRes.class,id));
		req.setAttribute("actionType", "view");
	}
}