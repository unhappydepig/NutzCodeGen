package com.sinosoft.modules.base;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseForm;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:26 
 * 自定义表单类型_t_base_form Action
 */
@IocBean
@At("/base/BaseForm")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseFormAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseForm")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")BaseForm baseForm,HttpServletRequest req) {
		dao.create(BaseForm.class, false);
		List<BaseForm> baseFormList = daoUtil.queryList(baseForm,baseCondition);
		req.setAttribute("baseFormList", baseFormList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseForm", baseForm);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseForm")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("baseForm", new BaseForm());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseForm baseForm) {
		try {
			daoUtil.add(baseForm);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseForm")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseForm",daoUtil.detailByPK(BaseForm.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseForm baseForm) {
		try {
			daoUtil.update(baseForm);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseForm.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseForm.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseForm")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseForm",daoUtil.detailByPK(BaseForm.class,id));
		req.setAttribute("actionType", "view");
	}
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseCustomerForm")
	public void customForm(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseForm",daoUtil.detailByPK(BaseForm.class,id));
		req.setAttribute("actionType", "edit");
	}
}