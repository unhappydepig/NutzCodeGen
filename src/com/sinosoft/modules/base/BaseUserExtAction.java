package com.sinosoft.modules.base;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseUserExt;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:26 
 * 员工扩展信息-t_base_user_ext Action
 */
@IocBean
@At("/base/BaseUserExt")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseUserExtAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseUserExt")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")BaseUserExt baseUserExt,HttpServletRequest req) {
		dao.create(BaseUserExt.class, false);
		List<BaseUserExt> baseUserExtList = daoUtil.queryList(baseUserExt,baseCondition);
		req.setAttribute("baseUserExtList", baseUserExtList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseUserExt", baseUserExt);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseUserExt")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("baseUserExt", new BaseUserExt());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseUserExt baseUserExt) {
		try {
			daoUtil.add(baseUserExt);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseUserExt")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseUserExt",daoUtil.detailByPK(BaseUserExt.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseUserExt baseUserExt) {
		try {
			daoUtil.update(baseUserExt);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseUserExt.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseUserExt.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseUserExt")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseUserExt",daoUtil.detailByPK(BaseUserExt.class,id));
		req.setAttribute("actionType", "view");
	}
}