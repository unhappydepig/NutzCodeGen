package com.sinosoft.modules.base;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import com.sinosoft.castor.CastorUtil;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.util.StringUtil;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseGrade;
import com.sinosoft.domain.base.BaseGradeRes;
import com.sinosoft.domain.base.BaseRes;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:25 
 * 岗位信息表-t_base_grade Action
 */
@IocBean
@At("/base/BaseGrade")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseGradeAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseGrade")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")BaseGrade baseGrade,HttpServletRequest req) {
		dao.create(BaseGrade.class, false);
		dao.create(BaseGradeRes.class, false);
		List<BaseGrade> baseGradeList = daoUtil.queryList(baseGrade,baseCondition);
		req.setAttribute("baseGradeList", baseGradeList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseGrade", baseGrade);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseGrade")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("baseGrade", new BaseGrade());
		List<BaseRes> resList = this.getSessionObj(req).getResList();
		req.setAttribute("baseResList", resList);
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseGrade baseGrade) {
		try {
			daoUtil.add(baseGrade);			
			daoUtil.delete(BaseGradeRes.class, Cnd.where("gradeId","=", baseGrade.getGradeId()));		
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseGrade")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		BaseGrade baseGrade = daoUtil.detailByPK(BaseGrade.class,id);
		List<BaseGradeRes> gradeResList = dao.query(BaseGradeRes.class,Cnd.where("gradeId", "=", id));
		String[] resArray = new String[gradeResList.size()];
		for(int i=0;i<gradeResList.size();i++){
			resArray[i] = gradeResList.get(i).getResId();
		}
		String resCode = CastorUtil.castTo(resArray,String.class);
		baseGrade.setResCode(resCode);
		req.setAttribute("baseGrade",baseGrade);
		List<BaseRes> resList = this.getSessionObj(req).getResList();
		req.setAttribute("baseResList", resList);
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseGrade baseGrade) {
		try {
			daoUtil.update(baseGrade);
			String gradeId = baseGrade.getGradeId();
			daoUtil.delete(BaseGradeRes.class, Cnd.where("gradeId","=",gradeId));
			String[] resArray = StringUtil.split(baseGrade.getResCode(),",");
			for(int i=0;i<resArray.length;i++){
				BaseGradeRes gradeRes= new BaseGradeRes();
				gradeRes.setGradeId(gradeId);
				gradeRes.setResId(resArray[i]);
				daoUtil.add(gradeRes);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseGrade.class,idArray[i]);
				daoUtil.delete(BaseGradeRes.class, Cnd.where("gradeId","=",idArray[i]));
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseGrade.class,id);
				daoUtil.delete(BaseGradeRes.class, Cnd.where("gradeId","=",id));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseGrade")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseGrade",daoUtil.detailByPK(BaseGrade.class,id));
		req.setAttribute("actionType", "view");
	}
}