package com.sinosoft.modules.base;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseFormItem;

/**
 * @author yangqunwei
 * @mail yqw8912@163.com
 * @time  2015-05-02 14:51:25 
 * 自定义表单-t_base_form_item Action
 */
@IocBean
@At("/base/BaseFormItem")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class BaseFormItemAction extends BaseAction {
	//查询列表
	@At("")
	@Ok("jsp:jsp.base.listBaseFormItem")
	public void list(@Param("..")BaseCondition baseCondition,@Param("..")BaseFormItem baseFormItem,HttpServletRequest req) {
		dao.create(BaseFormItem.class, false);
		List<BaseFormItem> baseFormItemList = daoUtil.queryList(baseFormItem,baseCondition);
		req.setAttribute("baseFormItemList", baseFormItemList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseFormItem", baseFormItem);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//准备新增
	@At
	@Ok("jsp:jsp.base.editBaseFormItem")
	public void toadd(HttpServletRequest req) {
		req.setAttribute("baseFormItem", new BaseFormItem());
		req.setAttribute("actionType", "add");
	}	
	/**
	 * 新增数据
	 * @param req
	 */
	@At
	public void add(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseFormItem baseFormItem) {
		try {
			daoUtil.add(baseFormItem);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}	
	//准备更新
	@At
	@Ok("jsp:jsp.base.editBaseFormItem")
	public void toedit(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseFormItem",daoUtil.detailByPK(BaseFormItem.class,id));
		req.setAttribute("actionType", "edit");
	}
	/**
	 * 新增数据
	 * @param req
	 */
	@At	
	public void edit(HttpServletRequest req,HttpServletResponse response,@Param("..")BaseFormItem baseFormItem) {
		try {
			daoUtil.update(baseFormItem);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}	
		ajaxDoneSuccess(response,successMessage,true);
	}
	//批量删除
	@At	
	public  void deletes(HttpServletRequest req,HttpServletResponse response,@Param("ids") String[] idArray) {
		try {
			for(int i=0;i<idArray.length;i++){
				daoUtil.deleteByPK(BaseFormItem.class,idArray[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}
	//批量删除
	@At	
	public  void delete(HttpServletRequest req,HttpServletResponse response,@Param("id") String id) {
		try {
				daoUtil.deleteByPK(BaseFormItem.class,id);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxDoneError(response,e.getMessage());		
		}		
		ajaxDoneSuccess(response,successMessage);
	}	
	@At	
	@Ok("jsp:jsp.base.editBaseFormItem")
	public void view(HttpServletRequest req,@Param("id") String id) {
		req.setAttribute("baseFormItem",daoUtil.detailByPK(BaseFormItem.class,id));
		req.setAttribute("actionType", "view");
	}
}