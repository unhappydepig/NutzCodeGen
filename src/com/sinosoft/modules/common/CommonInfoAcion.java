package com.sinosoft.modules.common;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import com.sinosoft.common.action.BaseAction;
import com.sinosoft.common.bean.BaseCondition;
import com.sinosoft.common.web.filter.BaseFilter;
import com.sinosoft.common.web.filter.UserLoginFilter;
import com.sinosoft.domain.base.BaseCompany;
import com.sinosoft.domain.base.BaseConfig;
import com.sinosoft.domain.base.BaseUser;
/**
 * 基础数据的Action
 * @author unhappydepig
 *
 */
@IocBean
@At("/common/CommonInfo")
@Filters({@By(type = UserLoginFilter.class), @By(type = BaseFilter.class)})
public class CommonInfoAcion  extends BaseAction{
	//查询结构列表
	@At
	@Ok("jsp:jsp.common.listCompany")
	public void CompanyList(@Param("..")BaseCondition baseCondition,@Param("..")BaseCompany baseCompany,HttpServletRequest req) {	
		daoUtil.setPowerFlag(true);
		List<BaseCompany> baseCompanyList = daoUtil.queryList(baseCompany,baseCondition);
		req.setAttribute("baseCompanyList", baseCompanyList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseCompany", baseCompany);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//查询用户列表
	@At
	@Ok("jsp:jsp.common.listUser")
	public void UserList(@Param("..")BaseCondition baseCondition,@Param("..")BaseUser baseUser,HttpServletRequest req) {
		dao.create(BaseUser.class, false);
		daoUtil.setPowerFlag(true);
		List<BaseUser> baseUserList = daoUtil.queryList(baseUser,baseCondition);
		req.setAttribute("baseUserList", baseUserList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseUser", baseUser);//返回查询条件鞭面页面跳转后查询条件丢失
	}
	//查询基础配置项列表
	@At
	@Ok("jsp:jsp.common.listConfig")
	public void ConfigList(@Param("..")BaseCondition baseCondition,@Param("..")BaseConfig baseConfig,HttpServletRequest req) {
		dao.create(BaseConfig.class, false);
		List<BaseConfig> baseConfigList = daoUtil.queryList(baseConfig,baseCondition);
		req.setAttribute("baseConfigList", baseConfigList);//查询结果返回页面 
		req.setAttribute("baseCondition", baseCondition);//总记录数返回页面
		req.setAttribute("baseConfig", baseConfig);//返回查询条件鞭面页面跳转后查询条件丢失
	}
}
