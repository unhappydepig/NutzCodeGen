package com.sinosoft.castor;

import org.nutz.castor.Castors;
/**
 * 转换工具类
 * @author unhappydepig 
 * @mail yqw8912@163.com
 * @time 2015-5-7 下午5:36:10
 */
public class CastorUtil {
	private static Castors castors;
	public static <T> T castTo(Object src, Class<T> toType) {
		if(null==castors){
			initCastors();
		}
		return castors.castTo(src, toType);
	}
	private static void initCastors() {
		castors = Castors.me();
	}
	
}

