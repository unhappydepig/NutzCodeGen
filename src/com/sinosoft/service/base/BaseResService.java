package com.sinosoft.service.base;

import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;

import com.sinosoft.common.db.DaoUtil;
import com.sinosoft.common.util.Constans;
import com.sinosoft.domain.base.BaseGradeRes;
import com.sinosoft.domain.base.BaseRes;

/**
 * 与资源相关的服务实现
 * @author unhappydepig
 *
 */
public class BaseResService {
	private Dao dao;
	private DaoUtil daoUtil;
	public BaseResService(Dao dao){
		this.dao = dao;
		daoUtil = new DaoUtil(dao);
	}
	public BaseResService(DaoUtil daoUtil){
		this.daoUtil = daoUtil;
		this.dao = daoUtil.getDao();
	}
	/**
	 * 获取某人的岗位信息
	 * @param userId
	 * @param comId
	 * @return
	 */
	public List<BaseRes> getResList(String userId,String comId){
		String[] gradeArray = new BaseGradeService(daoUtil).gradeArray(userId, comId);
		return getResList(gradeArray);
	} 	
	/**
	 * 根据岗位代码获取资源列表
	 * @param gradeArray
	 * @return
	 */
	public List<BaseRes> getResList(String[] gradeArray){
		boolean superFlag = false;
		for(int i=0;i<gradeArray.length;i++){
			if(gradeArray[i].equals(Constans.G_SUPER)){
				superFlag = true;
				break;
			}
		}
		if(superFlag){
			return dao.query(BaseRes.class, Cnd.where("1", "=", "1").getOrderBy().asc("location"));
		}
		List<BaseGradeRes> gradeResList = dao.query(BaseGradeRes.class, Cnd.where("gradeId", "in", gradeArray));
		if(null!=gradeResList&&gradeResList.size()>0){
			String[] resArray = new String[gradeResList.size()];
			for(int i=0;i<gradeResList.size();i++){
				resArray[i] = gradeResList.get(i).getResId();
			}
			Cnd contion =  Cnd.where("resId", "in", resArray);
			contion.getOrderBy().asc("location");
			return dao.query(BaseRes.class,contion);
		}
		return null;
	}
}
