package com.sinosoft.service.base;

import java.util.List;

import org.nutz.dao.Dao;

import com.sinosoft.common.db.DaoUtil;
import com.sinosoft.domain.base.BaseGrade;

/**
 * 与岗位相关的服务实现
 * @author unhappydepig
 *
 */
public class BaseGradeService {
	private Dao dao;
	private DaoUtil daoUtil;
	public BaseGradeService(Dao dao){
		this.dao = dao;
		daoUtil = new DaoUtil(dao);
	}
	public BaseGradeService(DaoUtil daoUtil){
		this.daoUtil = daoUtil;
		this.dao = daoUtil.getDao();
	}
	/**
	 * 获取某人的岗位信息
	 * @param userId
	 * @param comId
	 * @return
	 */
	public String[] gradeArray(String userId,String comId){
		List<BaseGrade> gradeList =  daoUtil.queryList(new BaseGrade());
		String[] gradeArray = new String[gradeList.size()];
		for(int i=0;i<gradeList.size();i++){
			gradeArray[i] = gradeList.get(i).getGradeId();
		}
		return gradeArray;
	} 
}
