import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.util.FileUtils;
import org.nutz.lang.Files;

import com.sinosoft.common.db.FileDaoUtil;
import com.sinosoft.common.db.StaticDaoUtil;
import com.sinosoft.common.util.PowerUtil;


public class TestMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		System.out.println(FileDaoUtil.getDao());
		List<String> fileList = new ArrayList<String>();
		fileList.add("D:\\MAVEN\\REPO\\javax\\servlet\\servlet-api\\2.5\\servlet-api-2.5.jar");
		fileList.add("D:\\MAVEN\\REPO\\javax\\mail\\mail\\1.4.7\\mail-1.4.7.jar");
		fileList.add("D:\\MAVEN\\REPO\\javax\\activation\\activation\\1.1\\activation-1.1.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-beans\\3.1.0.RELEASE\\spring-beans-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-core\\3.1.0.RELEASE\\spring-core-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-asm\\3.1.0.RELEASE\\spring-asm-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\commons-logging\\commons-logging\\1.1.1\\commons-logging-1.1.1.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\ebaysf\\web\\cors-filter\\1.0.1\\cors-filter-1.0.1.jar");
		fileList.add("D:\\MAVEN\\REPO\\com\\thetransactioncompany\\java-property-utils\\1.9\\java-property-utils-1.9.jar");
		fileList.add("D:\\MAVEN\\REPO\\com\\thetransactioncompany\\cors-filter\\1.3.2\\cors-filter-1.3.2.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-context\\3.1.0.RELEASE\\spring-context-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-aop\\3.1.0.RELEASE\\spring-aop-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\aopalliance\\aopalliance\\1.0\\aopalliance-1.0.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-expression\\3.1.0.RELEASE\\spring-expression-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-context-support\\3.1.0.RELEASE\\spring-context-support-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-webmvc\\3.1.0.RELEASE\\spring-webmvc-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-web\\3.1.0.RELEASE\\spring-web-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-aspects\\3.1.0.RELEASE\\spring-aspects-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-test\\3.1.0.RELEASE\\spring-test-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-jdbc\\3.1.0.RELEASE\\spring-jdbc-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-tx\\3.1.0.RELEASE\\spring-tx-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\springframework\\spring-orm\\3.1.0.RELEASE\\spring-orm-3.1.0.RELEASE.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\slf4j\\slf4j-api\\1.6.6\\slf4j-api-1.6.6.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\slf4j\\slf4j-jdk14\\1.6.6\\slf4j-jdk14-1.6.6.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\apache\\poi\\poi\\3.8\\poi-3.8.jar");
		fileList.add("D:\\MAVEN\\REPO\\commons-codec\\commons-codec\\1.5\\commons-codec-1.5.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\apache\\poi\\poi-ooxml\\3.8\\poi-ooxml-3.8.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\apache\\poi\\poi-ooxml-schemas\\3.8\\poi-ooxml-schemas-3.8.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\apache\\xmlbeans\\xmlbeans\\2.3.0\\xmlbeans-2.3.0.jar");
		fileList.add("D:\\MAVEN\\REPO\\stax\\stax-api\\1.0.1\\stax-api-1.0.1.jar");
		fileList.add("D:\\MAVEN\\REPO\\dom4j\\dom4j\\1.6.1\\dom4j-1.6.1.jar");
		fileList.add("D:\\MAVEN\\REPO\\xml-apis\\xml-apis\\1.3.04\\xml-apis-1.3.04.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\apache\\camel\\camel-core\\2.4.0\\camel-core-2.4.0.jar");
		fileList.add("D:\\MAVEN\\REPO\\commons-logging\\commons-logging-api\\1.1\\commons-logging-api-1.1.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\fusesource\\commonman\\commons-management\\1.0\\commons-management-1.0.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\apache\\camel\\camel-spring\\2.4.0\\camel-spring-2.4.0.jar");
		fileList.add("D:\\MAVEN\\REPO\\log4j\\log4j\\1.2.14\\log4j-1.2.14.jar");
		fileList.add("D:\\MAVEN\\REPO\\com\\alibaba\\druid\\1.0.7\\druid-1.0.7.jar");
		fileList.add("D:\\MAVEN\\REPO\\com\\alibaba\\fastjson\\1.1.41\\fastjson-1.1.41.jar");
		fileList.add("D:\\MAVEN\\REPO\\commons-fileupload\\commons-fileupload\\1.2.2\\commons-fileupload-1.2.2.jar");
		fileList.add("D:\\MAVEN\\REPO\\commons-io\\commons-io\\2.0\\commons-io-2.0.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\jfree\\jfreechart\\1.0.15\\jfreechart-1.0.15.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\jfree\\jcommon\\1.0.17\\jcommon-1.0.17.jar");
		fileList.add("D:\\MAVEN\\REPO\\com\\lowagie\\itext\\2.1.5\\itext-2.1.5.jar");
		fileList.add("D:\\MAVEN\\REPO\\bouncycastle\\bcmail-jdk14\\138\\bcmail-jdk14-138.jar");
		fileList.add("D:\\MAVEN\\REPO\\bouncycastle\\bcprov-jdk14\\138\\bcprov-jdk14-138.jar");
		fileList.add("D:\\MAVEN\\REPO\\backport-util-concurrent\\backport-util-concurrent\\3.1\\backport-util-concurrent-3.1.jar");
		fileList.add("D:\\MAVEN\\REPO\\org\\beanshell\\bsh\\2.0b4\\bsh-2.0b4.jar");
		fileList.add("D:\\MAVEN\\REPO\\net\\sf\\ezmorph\\ezmorph\\1.0.6\\ezmorph-1.0.6.jar");
		fileList.add("D:\\MAVEN\\REPO\\commons-lang\\commons-lang\\2.3\\commons-lang-2.3.jar");
		fileList.add("D:\\MAVEN\\REPO\\net\\sf\\ehcache\\ehcache\\2.8.3\\ehcache-2.8.3.jar");
		fileList.add("D:\\MAVEN\\REPO\\commons-httpclient\\commons-httpclient\\3.1-rc1\\commons-httpclient-3.1-rc1.jar");
		fileList.add("D:\\MAVEN\\REPO\\axis\\axis\\1.3\\axis-1.3.jar");
		fileList.add("D:\\MAVEN\\REPO\\axis\\axis-jaxrpc\\1.3\\axis-jaxrpc-1.3.jar");
		fileList.add("D:\\MAVEN\\REPO\\wsdl4j\\wsdl4j\\1.5.1\\wsdl4j-1.5.1.jar");
		fileList.add("D:\\MAVEN\\REPO\\commons-discovery\\commons-discovery\\0.2\\commons-discovery-0.2.jar");
		fileList.add("D:\\MAVEN\\REPO\\net\\sf\\json-lib\\json-lib\\2.2.3\\json-lib-2.2.3-jdk15.jar");
		fileList.add("D:\\MAVEN\\REPO\\commons-beanutils\\commons-beanutils\\1.7.0\\commons-beanutils-1.7.0.jar");
		fileList.add("D:\\MAVEN\\REPO\\commons-collections\\commons-collections\\3.2\\commons-collections-3.2.jar");
		fileList.add("D:\\MAVEN\\REPO\\mysql\\mysql-connector-java\\5.1.31\\mysql-connector-java-5.1.31.jar");
		for(int i=0;i<fileList.size();i++){
			String oldFile = fileList.get(i);
			System.out.println(oldFile);
			System.out.println(Files.getName(oldFile));
			
			File itemFile = new File("C:\\Users\\unhappydepig\\Workspaces\\Cynthia\\Cynthia\\WebRoot\\WEB-INF\\lib\\"+Files.getName(oldFile));
			Files.createFileIfNoExists(itemFile);
			Files.copy(new File(oldFile), itemFile);
			
		}
//		System.out.println(StaticDaoUtil.getDao());
		
//		System.out.println(PowerUtil.getComArrayByUserId("1430627530532"));
	}

}
