import org.nutz.dao.Dao;
import org.nutz.ioc.Ioc;
import org.nutz.ioc.impl.NutIoc;
import org.nutz.ioc.loader.json.JsonLoader;


public  class DaoUtil {
	private static Ioc ioc;

	public static Dao getDao() { // 暂不考虑线程同步的问题
	    if (ioc == null)
	        ioc = new NutIoc(new JsonLoader("config/datasource.json")); 
	    return ioc.get(Dao.class);
	}
	public static void main(String[] args) {
		System.out.println(getDao());
	}
}
