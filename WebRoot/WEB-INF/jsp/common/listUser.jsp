<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseUser" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="userId" class="control-label x85">员工ID:</label>                       
                        <input type="text" name="userId" id="userId"  value="${baseUser.userId}"  size="15">                        
                        <label for="userCode" class="control-label x85">员工代码:</label>                       
                        <input type="text" name="userCode" id="userCode"  value="${baseUser.userCode}"  size="15">                        
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId"  value="${baseUser.comId}"  size="15">                        
                        <label for="deptId" class="control-label x85">部门id:</label>                       
                        <input type="text" name="deptId" id="deptId"  value="${baseUser.deptId}"  size="15">                        
                        <br/>
                        <label for="password" class="control-label x85">密码:</label>                       
                        <input type="text" name="password" id="password"  value="${baseUser.password}"  size="15">                        
                        <label for="sale" class="control-label x85">盐值:</label>                       
                        <input type="text" name="sale" id="sale"  value="${baseUser.sale}"  size="15">                        
                        <label for="name" class="control-label x85">名称:</label>                       
                        <input type="text" name="name" id="name"  value="${baseUser.name}"  size="15">                        
                        <label for="userJob" class="control-label x85">员工职务:</label>                       
                        <input type="text" name="userJob" id="userJob"  value="${baseUser.userJob}"  size="15">                        
                        <br/>
                        <label for="userPic" class="control-label x85">员工照片:</label>                       
                        <input type="text" name="userPic" id="userPic"  value="${baseUser.userPic}"  size="15">                        
                        <label for="sex" class="control-label x85">性别:</label>                       
                        <input type="text" name="sex" id="sex"  value="${baseUser.sex}"  size="15">                        
                        <label for="birthday" class="control-label x85">生日:</label>                       
                        <input type="text" name="birthday" id="birthday"  data-toggle="datepicker"  value='<fmt:formatDate value="${baseUser.birthday}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="mobile" class="control-label x85">手机:</label>                       
                        <input type="text" name="mobile" id="mobile"  value="${baseUser.mobile}"  size="15">                        
                        <br/>
                        <label for="mail" class="control-label x85">邮箱:</label>                       
                        <input type="text" name="mail" id="mail"  value="${baseUser.mail}"  size="15">                        
                        <label for="state" class="control-label x85">在职状态:</label>                       
                        <input type="text" name="state" id="state"  value="${baseUser.state}"  size="15">                        
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy"  value="${baseUser.createBy}"  size="15">                        
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${baseUser.createTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <br/>
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy"  value="${baseUser.updateBy}"  size="15">                        
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${baseUser.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="valid" class="control-label x85">有效标示:</label>                       
                        <input type="text" name="valid" id="valid"  value="${baseUser.valid}"  size="15">                        
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseUser/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseUser" data-title="新增员工信息-t_base_user">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseUser/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_USER">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_USER" data-toggle="icheck"></th>
				     <th >员工ID</th>         
				     <th >员工代码</th>         
				     <th >机构代码</th>         
				     <th >部门id</th>         
				     <th >密码</th>         
				     <th >盐值</th>         
				     <th >名称</th>         
				     <th >员工职务</th>         
				     <th >员工照片</th>         
				     <th >性别</th>         
				     <th >生日</th>         
				     <th >手机</th>         
				     <th >邮箱</th>         
				     <th >在职状态</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
				     <th >有效标示</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseUserList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_USER" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.userId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.userCode}                        
                   </td>                      
                   <td>                   		                   
                        ${item.comId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.deptId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.password}                        
                   </td>                      
                   <td>                   		                   
                        ${item.sale}                        
                   </td>                      
                   <td>                   		                   
                        ${item.name}                        
                   </td>                      
                   <td>                   		                   
                        ${item.userJob}                        
                   </td>                      
                   <td>                   		                   
                        ${item.userPic}                        
                   </td>                      
                   <td>                   		                   
                        ${item.sex}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.birthday}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.mobile}                        
                   </td>                      
                   <td>                   		                   
                        ${item.mail}                        
                   </td>                      
                   <td>                   		                   
                        ${item.state}                        
                   </td>                      
                   <td>                   		                   
                        ${item.createBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.updateBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.valid}                        
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseUser/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseUser/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseUser" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑员工信息-t_base_user"></a>
	                    <a href="${base}/base/BaseUser/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseUser" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看员工信息-t_base_user"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
