<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/common/CommonInfo/Configlist" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="configCode" class="control-label x85">配置项代码:</label>                       
                        <input type="text" name="configCode" id="configCode"  value="${baseConfig.configCode}"  size="15">  
                        <label for="configCode" class="control-label x85">配置项名称:</label>                       
                        <input type="text" name="configName" id="configName"  value="${baseConfig.configName}"  size="15">                        
                        <label for="remark" class="control-label x85">配置说明:</label>                       
                        <input type="text" name="remark" id="remark"  value="${baseConfig.remark}"  size="15">                        
                        <label for="valid" class="control-label x85">有效标示:</label>                       
						<s:select name="baseConfig" withNull="true" codeType="valid" inputName="valid" property="valid"></s:select>
						<button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;  
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_CONFIG" data-toggle="icheck"></th>
				     <th >配置信息项</th>         
				     <th >配置名称</th>         
				     <th >配置说明</th>         
				     <th >有效标示</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseConfigList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_CONFIG" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.configCode}                        
                   </td>                      
                   <td>                   		                   
                        ${item.configName}                        
                   </td>                      
                   <td>                   		                   
                        ${item.remark}                        
                   </td>                      
                   <td>      
                   		<s:trans name="item" property="valid" codetype="valid"/>    
                   </td>                      
                   <td> 
                   		<s:trans name="item" property="createBy" codetype="user"/> 
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td> 
                   		<s:trans name="item" property="updateBy" codetype="user"/> 
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
	                <td>
                    <a href="javascript:;" data-toggle="lookupback" data-args="{configCode:'${item.configCode}', configName:'${item.configName}'}" class="btn btn-blue" title="选择本项" data-icon="check">选择</a>
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
