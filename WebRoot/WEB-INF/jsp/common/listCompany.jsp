<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/common/CommonInfo/Companylist" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">                  
                        <label for="comName" class="control-label x85">机构名称:</label>                       
                        <input type="text" name="comName" id="comName"  value="${baseCompany.comName}"  size="15">                        
                        <label for="comType" class="control-label x85">机构类型:</label>              
						<s:select name="baseCompany" width="80" withAll="true" property="valid" codeType="comType" inputName="comType"></s:select>
						<label for="valid" class="control-label x85">有效标示:</label>    
						<s:select name="baseCompany" width="50" withAll="true" property="valid" codeType="valid" inputName="valid"></s:select>  
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
				     <th >机构名称</th>         
				     <th >机构类型</th>         
				     <th >所属行业</th>       
				     <th >有效标示</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseCompanyList}" varStatus="status">
	            <tr data-id="${status.current}">
                   <td>                   		                   
                        ${item.comName}                        
                   </td>                      
                   <td>               
                   		<s:trans name="item" property="comType" codetype="comType"/>  
                   </td>                      
                   <td>    
                   		<s:trans name="item" property="comTrade" codetype="comTrade"/> 
                   </td> 
                   <td>     
                   		<s:trans name="item" property="valid" codetype="valid"/>                    
                   </td>                      
	                <td>
                    <a href="javascript:;" data-toggle="lookupback" data-args="{comId:'${item.comId}', comName:'${item.comName}'}" class="btn btn-blue" title="选择本项" data-icon="check">选择</a>
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
