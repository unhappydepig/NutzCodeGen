<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/test/Testable" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="id" class="control-label x85">主键:</label>                       
                        <input type="text" name="id" id="id" value="${testable.id}"  size="15">                        
                        <label for="code" class="control-label x85">代码:</label>                       
                        <input type="text" name="code" id="code" value="${testable.code}"  size="15">                        
                        <label for="name" class="control-label x85">名称:</label>                       
                        <input type="text" name="name" id="name" value="${testable.name}"  size="15">                        
                        <label for="inputDate" class="control-label x85">日期:</label>                       
                        <input type="text" name="inputDate" id="inputDate" data-toggle="datepicker"  value='<fmt:formatDate value="${testable.inputDate}"  pattern="yyyy-MM-dd"></fmt:formatDate>' size="15">
                        <br/>
                        <label for="inputDouble" class="control-label x85">金额:</label>                       
                        <input type="text" name="inputDouble" id="inputDouble" value="${testable.inputDouble}"  size="15">                        
                        <label for="inputInt" class="control-label x85">整数:</label>                       
                        <input type="text" name="inputInt" id="inputInt" value="${testable.inputInt}"  size="15">                        
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy" value="${testable.createBy}"  size="15">                        
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime" data-toggle="datepicker"  value='<fmt:formatDate value="${testable.createTime}"  pattern="yyyy-MM-dd"></fmt:formatDate>' size="15">
                        <br/>
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy" value="${testable.updateBy}"  size="15">                        
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime" data-toggle="datepicker"  value='<fmt:formatDate value="${testable.updateTime}"  pattern="yyyy-MM-dd"></fmt:formatDate>' size="15">
            <button type="submit" class="btn-default" data-icon="search">查询</button>         
	        <a href="${base}/test/Testable/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_Testable" data-title="新增测试表">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/test/Testable/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_TEST_TABLE">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_TEST_TABLE" data-toggle="icheck"></th>
				     <th >主键</th>         
				     <th >代码</th>         
				     <th >名称</th>         
				     <th >日期</th>         
				     <th >金额</th>         
				     <th >整数</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${testableList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_TEST_TABLE" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.id}                        
                   </td>                      
                   <td>                   		                   
                        ${item.code}                        
                   </td>                      
                   <td>                   		                   
                        ${item.name}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.inputDate}"  pattern="yyyy-MM-dd"></fmt:formatDate>
                   </td>                      
                   <td>                   		                   
                        ${item.inputDouble}                        
                   </td>                      
                   <td>                   		                   
                        ${item.inputInt}                        
                   </td>                      
                   <td>                   		                   
                        ${item.createBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"></fmt:formatDate>
                   </td>                      
                   <td>                   		                   
                        ${item.updateBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"></fmt:formatDate>
                   </td>                      
	                <td>
	                  <a href="${base}/test/Testable/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/test/Testable/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_Testable" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑测试表"></a>
	                    <a href="${base}/test/Testable/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_Testable" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看测试表"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
