<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/test/Testable/${actionType}" id="${actionType}_Testable_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="id" class="control-label x85">主键:</label>                       
                        <input type="text" name="id" id="id" value="${testable.id}" data-rule = "required" maxlength= "30" size="15">                        
                   </td>
                   <td>                   		
                        <label for="code" class="control-label x85">代码:</label>                       
                        <input type="text" name="code" id="code" value="${testable.code}"  maxlength= "30" size="15">                        
                   </td>
                   <td>                   		
                        <label for="name" class="control-label x85">名称:</label>                       
                        <input type="text" name="name" id="name" value="${testable.name}"  maxlength= "500" size="15">                        
                   </td>
                   <td>                   		
                        <label for="inputDate" class="control-label x85">日期:</label>                       
                        <input type="text" name="inputDate" id="inputDate" value='<fmt:formatDate value="${testable.inputDate}"  pattern="yyyy-MM-dd"></fmt:formatDate>' data-toggle="datepicker"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="inputDouble" class="control-label x85">金额:</label>                       
                        <input type="text" name="inputDouble" id="inputDouble" value="${testable.inputDouble}" data-rule = "doubles'" maxlength= "12" size="15">                        
                   </td>
                   <td>                   		
                        <label for="inputInt" class="control-label x85">整数:</label>                       
                        <input type="text" name="inputInt" id="inputInt" value="${testable.inputInt}" data-rule = "digits'"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy" value="${testable.createBy}"  maxlength= "30" size="15">                        
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime" value='<fmt:formatDate value="${testable.createTime}"  pattern="yyyy-MM-dd"></fmt:formatDate>' data-toggle="datepicker"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy" value="${testable.updateBy}"  maxlength= "30" size="15">                        
                   </td>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime" value='<fmt:formatDate value="${testable.updateTime}"  pattern="yyyy-MM-dd"></fmt:formatDate>' data-toggle="datepicker"  size="15">
                   </td>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_Testable_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>