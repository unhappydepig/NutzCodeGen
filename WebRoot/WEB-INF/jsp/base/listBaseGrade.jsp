<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseGrade" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">                    
                        <label for="gradeName" class="control-label x85">岗位名称:</label>                       
                        <input type="text" name="gradeName" id="gradeName"  value="${baseGrade.gradeName}"  size="15">                        
                        <label for="comId" class="control-label x85">机构代码:</label>                     
                       <input type="hidden" name="comId" id="comId"  value="${baseGrade.comId}"  size="15"> 
                        <input type="text" name="comName" id="comName" data-width="820" value='<s:trans name="baseGrade" property="comId" codetype="company"/>'  data-toggle="lookup" data-url="${base}/common/CommonInfo/CompanyList" data-title="查询机构"   maxlength="30"  size="15">                            
                        <label for="valid" class="control-label x85">有效标示:</label>  
						<s:select name="baseGrade" withNull="true" codeType="valid" inputName="valid" property="valid"></s:select>   
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseGrade/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseGrade" data-title="新增岗位信息表">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseGrade/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_GRADE">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_GRADE" data-toggle="icheck"></th>
				     <th >岗位名称</th>         
				     <th >机构</th>         
				     <th >顺序号</th>            
				     <th >有效标示</th>      
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseGradeList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_GRADE" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.gradeName}                        
                   </td>                      
                   <td>             
                        <s:trans name="item" property="comId" codetype="company"/>                        
                   </td>                      
                   <td>                   		                   
                        ${item.location}                        
                   </td>                      
                   <td>      
                   		<s:trans name="item" property="valid" codetype="valid"/>    
                   </td>                      
                   <td> 
                   		<s:trans name="item" property="createBy" codetype="user"/> 
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td> 
                   		<s:trans name="item" property="updateBy" codetype="user"/> 
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                       
	                <td>
	                  <a href="${base}/base/BaseGrade/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseGrade/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseGrade" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑岗位信息表"></a>
	                    <a href="${base}/base/BaseGrade/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseGrade" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看岗位信息表"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
