<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
     <table class="table table-bordered" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
				     <th width="15px"></th>         
				     <th >key</th>         
				     <th >值</th>         
				     <th >备注</th>         
				     <th >顺序号</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
				     <th >有效标示</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseCodeList}" varStatus="status">
	            <tr data-id="${status.current}">
                   <td>   
                   		 <c:if test="${item.subSize==0}">
                     	 <i class="fa fa-square-o" value="${item.PK}" subSize ="${item.subSize}">  
                   		 </c:if>   
                   		 <c:if test="${item.subSize!=0}">
                     	 <i class="fa fa-plus-square-o" value="${item.PK}" subSize ="${item.subSize}">  
                   		 </c:if>                       		              		                                       
                   </td> 	                          
                   <td>              		                   
                        ${item.codeCode}                        
                   </td>                      
                   <td>                   		                   
                        ${item.codeVal}                        
                   </td>                      
                   <td>                   		                   
                        ${item.remark}                        
                   </td>                      
                   <td>                   		                   
                        ${item.location}                        
                   </td>                      
                   <td>                   
                        <s:trans name="item" property="createBy" codetype="user"/>                     
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>  
                        <s:trans name="item" property="updateBy" codetype="user"/>                       
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                  
                        <s:trans name="item" property="valid" codetype="valid"/>                     
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseCode/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseCode/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseCode" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑基础代码表"></a>
	                  <a href="${base}/base/BaseCode/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseCode" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看基础代码表"></a>	                  	  
	              	  <a href="${base}/base/BaseCode/toadd?upCodeId=${item.codeId}" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseCode" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="新增基础代码表"></a>            	                
	                </td>
	            </tr> 
	            <tr style="display: none;" id="${item.PK}tr">
	            	<td colspan="2"/>
	            	<td colspan="8" id="${item.PK}">	            		
	            	</td>
	            </tr>     		
        	</c:forEach>
        </tbody>
    </table>
<script type="text/javascript">
$(document).ready(function(){ 
	$("i").click(function(){
		var hb = "fa fa-plus-square-o";//合并
		var zk = "fa fa-minus-square-o";//展开
		var codeId  = $(this).attr("value");
		var iClass = $(this).attr("class");
		var subSize = $(this).attr("subSize");
		if(0==subSize){
			$(this).alertmsg("warn", "没有子节点!", {type:'warn'});
			return;
		}
		if(hb==iClass){
			$(this).attr("class",zk);
			$("#"+codeId+"tr").show();
	    	$(this).bjuiajax('refreshLayout', {url:'${base}/base/BaseCode/subList?UpCodeId='+codeId,target:'#'+codeId});
			return;
		}
		if(zk==iClass){
			$(this).attr("class",hb);
			$("#"+codeId+"tr").hide();
			return;
		}		
	    
	});
}); 
</script> 	