<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseUser" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="userId" class="control-label x85">员工:</label>                       
                        <input type="text" name="userId" id="userId"  value="${baseUser.userId}"  size="15">                        
                        <label for="userCode" class="control-label x85">员工号:</label>                       
                        <input type="text" name="userCode" id="userCode"  value="${baseUser.userCode}"  size="15">                        
                        <label for="comId" class="control-label x85">所属公司:</label>                       
                        <input type="text" name="comId" id="comId"  value="${baseUser.comId}"  size="15">                        
                        <label for="deptId" class="control-label x85">所属部门:</label>                       
                        <input type="text" name="deptId" id="deptId"  value="${baseUser.deptId}"  size="15">                           
                        <br/>                                                              
                        <label for="name" class="control-label x85">名称:</label>
                        <input type="text" name="name" id="name"  value="${baseUser.name}"  size="15">  
                        <label for="userJob" class="control-label x85">员工职务:</label>                       
                        <input type="text" name="userJob" id="userJob"  value="${baseUser.userJob}"  size="15">   
                        <label for="sex" class="control-label x85">性别:</label>     
                        <s:select name="baseUser" codeType="sex" withNull="true" inputName="sex" property="sex"></s:select> 
                        <label for="mobile" class="control-label x85">手机:</label>                       
                        <input type="text" name="mobile" id="mobile"  value="${baseUser.mobile}"  size="15">                            
                        <br/>                                                             
                        <label for="state" class="control-label x85">在职状态:</label>      
                        <s:select name="baseUser" codeType="userstate" withNull="true" inputName="state" property="state"></s:select> 
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseUser/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseUser" data-title="新增员工">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseUser/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_USER">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_USER" data-toggle="icheck"></th>
				     <th >员工号</th>         
				     <th >名称</th>        
				     <th >公司</th>         
				     <th >部门</th>         
				     <th >员工职务</th>         
				     <th >性别</th>         
				     <th >生日</th>         
				     <th >手机</th>         
				     <th >邮箱</th>         
				     <th >在职状态</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >有效标示</th>         
                <th width="120">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseUserList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_USER" data-toggle="icheck" value="${item.PK}"></td>                     
                   <td>                   		                   
                        ${item.userCode}                        
                   </td>                                     
                   <td>                 		                   
                        ${item.name}                        
                   </td>         
                   <td>   
                   		<s:trans name="item" property="comId" codetype="company"/>
                   </td>                      
                   <td>        
                   		<s:trans name="item" property="deptId" codetype="company"/> 
                   </td>                     
                   <td>                   		                   
                        ${item.userJob}                        
                   </td>                      
                   <td>   
                   		<s:trans name="item" property="sex" codetype="sex"/>  
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.birthday}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.mobile}                        
                   </td>                      
                   <td>                   		                   
                        ${item.mail}                        
                   </td>                      
                   <td>    
                   		<s:trans name="item" property="state" codetype="userstate"/>  
                   </td>                      
                   <td>                             
                   		<s:trans name="item" property="createBy" codetype="user"/>         
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                    
                   <td>        
                   		<s:trans name="item" property="valid" codetype="valid"/>
                   </td>                      
	                <td>
	                  <%--<a href="${base}/base/BaseUser/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a> --%>
	                  <a href="${base}/base/BaseUser/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseUser"  data-title="编辑员工" titla="编辑">编</a>
	                  <a href="${base}/base/BaseUser/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseUser"  data-title="查看员工">查</a>	                  	  
	               	  <br/>
	               	  <a href="${base}/base/BaseUser/tograde?id=${item.PK}" class="btn btn-green" data-icon="fa-code-fork" data-toggle="dialog" data-id="grade_BaseUser"  data-title="岗位授权">岗</a>	                  	  
	               	  <a href="${base}/base/BaseUser/tocompany?id=${item.PK}" class="btn btn-green" data-icon="fa-group" data-toggle="dialog" data-id="com_BaseUser"  data-title="数据授权">数</a>	                  	  
	               
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
