<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseGradeRes/${actionType}" id="${actionType}_BaseGradeRes_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="gradeId" class="control-label x85">岗位ID:</label>                       
                        <input type="text" name="gradeId" id="gradeId" value="${baseGradeRes.gradeId}" data-rule = "required" maxlength= "30" size="15">                        
                   </td>
                   <td>                   		
                        <label for="resId" class="control-label x85">资源ID:</label>                       
                        <input type="text" name="resId" id="resId" value="${baseGradeRes.resId}" data-rule = "required" maxlength= "30" size="15">                        
                   </td>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy" value="${baseGradeRes.createBy}"  maxlength= "30" size="15">                        
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime" value='<fmt:formatDate value="${baseGradeRes.createTime}"  pattern="yyyy-MM-dd"></fmt:formatDate>' data-toggle="datepicker"  size="15">
                   </td>
                </tr>
                <tr>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_BaseGradeRes_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>