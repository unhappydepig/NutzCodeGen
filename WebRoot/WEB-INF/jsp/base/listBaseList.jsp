<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseList" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="listId" class="control-label x85">列表模板ID:</label>                       
                        <input type="text" name="listId" id="listId"  value="${baseList.listId}"  size="15">                        
                        <label for="formName" class="control-label x85">列表名称:</label>                       
                        <input type="text" name="formName" id="formName"  value="${baseList.formName}"  size="15">                        
                        <label for="formId" class="control-label x85">关联表单ID:</label>                       
                        <input type="text" name="formId" id="formId"  value="${baseList.formId}"  size="15">                        
                        <label for="description" class="control-label x85">描述描述:</label>                       
                        <input type="text" name="description" id="description"  value="${baseList.description}"  size="15">                        
                        <br/>
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy"  value="${baseList.createBy}"  size="15">                        
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${baseList.createTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy"  value="${baseList.updateBy}"  size="15">                        
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${baseList.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <br/>
                        <label for="valid" class="control-label x85">有效标示:</label>                       
                        <input type="text" name="valid" id="valid"  value="${baseList.valid}"  size="15">                        
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseList/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseList" data-title="新增自定义查询结果类型-t_base_list">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseList/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_LIST">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_LIST" data-toggle="icheck"></th>
				     <th >列表模板ID</th>         
				     <th >列表名称</th>         
				     <th >关联表单ID</th>         
				     <th >描述描述</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
				     <th >有效标示</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseListList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_LIST" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.listId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.formName}                        
                   </td>                      
                   <td>                   		                   
                        ${item.formId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.description}                        
                   </td>                      
                   <td>                   		                   
                        ${item.createBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.updateBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.valid}                        
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseList/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseList/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseList" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑自定义查询结果类型-t_base_list"></a>
	                    <a href="${base}/base/BaseList/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseList" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看自定义查询结果类型-t_base_list"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
