<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseLisitem" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="listItemId" class="control-label x85">关联表主键:</label>                       
                        <input type="text" name="listItemId" id="listItemId"  value="${baseLisitem.listItemId}"  size="15">                        
                        <label for="listId" class="control-label x85">列表id:</label>                       
                        <input type="text" name="listId" id="listId"  value="${baseLisitem.listId}"  size="15">                        
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId"  value="${baseLisitem.comId}"  size="15">                        
                        <label for="userId" class="control-label x85">用户代码:</label>                       
                        <input type="text" name="userId" id="userId"  value="${baseLisitem.userId}"  size="15">                        
                        <br/>
                        <label for="attrCode" class="control-label x85">属性代码:</label>                       
                        <input type="text" name="attrCode" id="attrCode"  value="${baseLisitem.attrCode}"  size="15">                        
                        <label for="attrName" class="control-label x85">显示名称:</label>                       
                        <input type="text" name="attrName" id="attrName"  value="${baseLisitem.attrName}"  size="15">                        
                        <label for="format" class="control-label x85">格式化:</label>                       
                        <input type="text" name="format" id="format"  value="${baseLisitem.format}"  size="15">                        
                        <label for="orderFlag" class="control-label x85">是否可排序:</label>                       
                        <input type="text" name="orderFlag" id="orderFlag"  value="${baseLisitem.orderFlag}"  size="15">                        
                        <br/>
                        <label for="editFlag" class="control-label x85">是否可编辑:</label>                       
                        <input type="text" name="editFlag" id="editFlag"  value="${baseLisitem.editFlag}"  size="15">                        
                        <label for="showFlag" class="control-label x85">显示状态:</label>                       
                        <input type="text" name="showFlag" id="showFlag"  value="${baseLisitem.showFlag}"  size="15">                        
                        <label for="reqFlag" class="control-label x85">是否必填:</label>                       
                        <input type="text" name="reqFlag" id="reqFlag"  value="${baseLisitem.reqFlag}"  size="15">                        
                        <label for="location" class="control-label x85">顺序号:</label>                       
                        <input type="text" name="location" id="location"  value="${baseLisitem.location}"  size="15">                        
                        <br/>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseLisitem/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseLisitem" data-title="新增自定义查询结果-t_base_list_item">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseLisitem/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_LIST_ITEM">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_LIST_ITEM" data-toggle="icheck"></th>
				     <th >关联表主键</th>         
				     <th >列表id</th>         
				     <th >机构代码</th>         
				     <th >用户代码</th>         
				     <th >属性代码</th>         
				     <th >显示名称</th>         
				     <th >格式化</th>         
				     <th >是否可排序</th>         
				     <th >是否可编辑</th>         
				     <th >显示状态</th>         
				     <th >是否必填</th>         
				     <th >顺序号</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseLisitemList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_LIST_ITEM" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.listItemId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.listId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.comId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.userId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.attrCode}                        
                   </td>                      
                   <td>                   		                   
                        ${item.attrName}                        
                   </td>                      
                   <td>                   		                   
                        ${item.format}                        
                   </td>                      
                   <td>                   		                   
                        ${item.orderFlag}                        
                   </td>                      
                   <td>                   		                   
                        ${item.editFlag}                        
                   </td>                      
                   <td>                   		                   
                        ${item.showFlag}                        
                   </td>                      
                   <td>                   		                   
                        ${item.reqFlag}                        
                   </td>                      
                   <td>                   		                   
                        ${item.location}                        
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseLisitem/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseLisitem/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseLisitem" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑自定义查询结果-t_base_list_item"></a>
	                    <a href="${base}/base/BaseLisitem/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseLisitem" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看自定义查询结果-t_base_list_item"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
