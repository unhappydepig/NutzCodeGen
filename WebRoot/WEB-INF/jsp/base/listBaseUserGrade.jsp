<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseUserGrade" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="userId" class="control-label x85">员工ID:</label>                       
                        <input type="text" name="userId" id="userId"  value="${baseUserGrade.userId}"  size="15">                        
                        <label for="gradeId" class="control-label x85">岗位ID:</label>                       
                        <input type="text" name="gradeId" id="gradeId"  value="${baseUserGrade.gradeId}"  size="15">                        
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseUserGrade/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseUserGrade" data-title="新增员工岗位-t_base_user_grade">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseUserGrade/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_USER_GRADE">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_USER_GRADE" data-toggle="icheck"></th>
				     <th >员工ID</th>         
				     <th >岗位ID</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseUserGradeList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_USER_GRADE" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.userId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.gradeId}                        
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseUserGrade/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseUserGrade/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseUserGrade" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑员工岗位-t_base_user_grade"></a>
	                    <a href="${base}/base/BaseUserGrade/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseUserGrade" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看员工岗位-t_base_user_grade"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
