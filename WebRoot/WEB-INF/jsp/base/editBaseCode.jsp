<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseCode/${actionType}" id="edit_BaseCode_form" data-toggle="validate" data-alertmsg="false" >  
       	              <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${baseCode.createTime}"  pattern="yyyy-MM-dd"/>'  >
                     <input type="hidden" name="createBy" id="createBy" value="${baseCode.createBy}"     maxlength="30"  size="15">   
                     <input type="hidden" name="updateBy" id="updateBy" value="${baseCode.updateBy}"     maxlength="30"  size="15"> 
                     <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${baseCode.updateTime}"  pattern="yyyy-MM-dd"/>'>
					<input type="hidden" name="codeId" id="codeId" value="${baseCode.codeId}">          	 	
					<input type="hidden" name="upCodeId" id="upCodeId" value="${baseCode.upCodeId}">          	 	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="codeCode" class="control-label x85">键:</label>                       
                        <input type="text" name="codeCode" id="codeCode" value="${baseCode.codeCode}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="codeVal" class="control-label x85">值:</label>                       
                        <input type="text" name="codeVal" id="codeVal" value="${baseCode.codeVal}"     maxlength="100"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="remark" class="control-label x85">备注:</label>                       
                        <input type="text" name="remark" id="remark" value="${baseCode.remark}"     maxlength="500"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="location" class="control-label x85">顺序号:</label>                       
                        <input type="text" name="location" id="location" value="${baseCode.location}"     data-rule ="required;digits"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label> 
                        <s:select name="baseCode" codeType="valid" property="valid" inputName="valid"></s:select>                                         
                   </td>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>