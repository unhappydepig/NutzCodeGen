<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseUserGrade/${actionType}" id="edit_BaseUserGrade_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="userId" class="control-label x85">员工ID:</label>                       
                        <input type="text" name="userId" id="userId" value="${baseUserGrade.userId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="gradeId" class="control-label x85">岗位ID:</label>                       
                        <input type="text" name="gradeId" id="gradeId" value="${baseUserGrade.gradeId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>