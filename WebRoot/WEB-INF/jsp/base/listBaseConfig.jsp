<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseConfig" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="configCode" class="control-label x85">配置信息项:</label>                       
                        <input type="hidden" name="configCode" id="configCode"  value="${baseConfig.configCode}"  size="15"> 
                        <input type="text" name="configName" id="configName" data-width="820" value='${baseConfig.configName}'  data-toggle="lookup" data-url="${base}/common/CommonInfo/ConfigList" data-title="查询配置项"  maxlength="30"  size="15">                        
                        
                                                
                        <label for="remark" class="control-label x85">配置说明:</label>                       
                        <input type="text" name="remark" id="remark"  value="${baseConfig.remark}"  size="15">                        
                        <label for="valid" class="control-label x85">有效标示:</label>                       
						<s:select name="baseConfig" withNull="true" codeType="valid" inputName="valid" property="valid"></s:select> 
                        <br/>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseConfig/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseConfig" data-title="新增基础信息配置类型">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseConfig/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_CONFIG">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_CONFIG" data-toggle="icheck"></th>
				     <th >配置信息项</th>         
				     <th >配置名称</th>         
				     <th >配置说明</th>         
				     <th >有效标示</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseConfigList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_CONFIG" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.configCode}                        
                   </td>                      
                   <td>                   		                   
                        ${item.configName}                        
                   </td>                      
                   <td>                   		                   
                        ${item.remark}                        
                   </td>                      
                   <td>      
                   		<s:trans name="item" property="valid" codetype="valid"/>    
                   </td>                      
                   <td> 
                   		<s:trans name="item" property="createBy" codetype="user"/> 
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td> 
                   		<s:trans name="item" property="updateBy" codetype="user"/> 
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseConfig/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseConfig/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseConfig" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑基础信息配置类型"></a>
	                    <a href="${base}/base/BaseConfig/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseConfig" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看基础信息配置类型"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
