<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseTableExt/${actionType}" id="edit_BaseTableExt_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="extId" class="control-label x85">扩展属性id:</label>                       
                        <input type="text" name="extId" id="extId" value="${baseTableExt.extId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId" value="${baseTableExt.comId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="tableCode" class="control-label x85">主表名:</label>                       
                        <input type="text" name="tableCode" id="tableCode" value="${baseTableExt.tableCode}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="extTableCode" class="control-label x85">扩展表名:</label>                       
                        <input type="text" name="extTableCode" id="extTableCode" value="${baseTableExt.extTableCode}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="attrName" class="control-label x85">属性名称:</label>                       
                        <input type="text" name="attrName" id="attrName" value="${baseTableExt.attrName}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="dataType" class="control-label x85">数据类型:</label>                       
                        <input type="text" name="dataType" id="dataType" value="${baseTableExt.dataType}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="reqFlag" class="control-label x85">是否必填:</label>                       
                        <input type="text" name="reqFlag" id="reqFlag" value="${baseTableExt.reqFlag}"     maxlength="2"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy" value="${baseTableExt.createBy}"     maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime" value='<fmt:formatDate value="${baseTableExt.createTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy" value="${baseTableExt.updateBy}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime" value='<fmt:formatDate value="${baseTableExt.updateTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>                       
                        <input type="text" name="valid" id="valid" value="${baseTableExt.valid}"     data-rule ="required"  maxlength="2"  size="15">                        
                   </td>
                </tr>
                <tr>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>