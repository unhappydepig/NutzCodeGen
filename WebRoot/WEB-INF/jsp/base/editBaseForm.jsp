<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseForm/${actionType}" id="edit_BaseForm_form" data-toggle="validate" data-alertmsg="false" >   
        <input type="hidden" name="createBy" id="createBy" value="${baseForm.createBy}"     maxlength="30"  size="15">    
        <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${baseForm.createTime}"  pattern="yyyy-MM-dd"/>'    data-rule ="date"  size="15">
        <input type="hidden" name="updateBy" id="updateBy" value="${baseForm.updateBy}"     maxlength="30"  size="15">  
        <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${baseForm.updateTime}"  pattern="yyyy-MM-dd"/>'    data-rule ="date"  size="15">
        <input type="hidden" name="formId" id="formId" value="${baseForm.formId}"     maxlength="30"  size="15">             	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="formName" class="control-label x85">表单名称:</label>                       
                        <input type="text" name="formName" id="formName" value="${baseForm.formName}"     data-rule ="required"  maxlength="100"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="description" class="control-label x85">表单描述:</label>                       
                        <input type="text" name="description" id="description" value="${baseForm.description}"     maxlength="500"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label> 
                        <s:select name="baseForm" codeType="valid" inputName="valid" property="valid"></s:select>                                    
                   </td>
                </tr>
                <tr>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>