<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseRes/${actionType}" id="${actionType}_BaseRes_form" data-toggle="validate" data-alertmsg="false" >  
        <input type="hidden" name="resId" id="resId" value="${baseRes.resId}" maxlength= "30" size="15">                            
        <%-- <label for="createBy" class="control-label x85">创建人:</label>    --%>                       
        <input type="hidden" name="createBy" id="createBy" value="${baseCompany.createBy}"     maxlength="30"  size="15">    
        <%-- <label for="createTime" class="control-label x85">创建时间:</label> --%>                          
        <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${baseCompany.createTime}"  pattern="yyyy-MM-dd"/>'    data-rule ="date"  size="15">
        <%-- <label for="updateBy" class="control-label x85">更新人:</label>   --%>                        
        <input type="hidden" name="updateBy" id="updateBy" value="${baseCompany.updateBy}"     maxlength="30"  size="15">    
        <%-- <label for="updateTime" class="control-label x85">更新时间:</label>    --%>                       
        <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${baseCompany.updateTime}"  pattern="yyyy-MM-dd"/>'    data-rule ="date"  size="15">   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="upResId" class="control-label x85">上级资源ID:</label>                                               
                        <input type="hidden" name="upResId" id="upResId" value="${baseRes.upResId}">
                        <input type="text" name="upResIdVal" id="upResIdVal"  value='<s:trans name="baseRes" property="upResId" codetype="baseRes"/>' data-rule = "required" data-toggle="selectztree" size="18" data-tree="#j_select_tree2" readonly maxlength= "30" size="15">
                            <ul id="j_select_tree2" class="ztree hide" data-toggle="ztree" data-expand-all="true" data-check-enable="true" data-chk-style="radio" data-radio-type="all" data-on-check="S_NodeCheck" data-on-click="S_NodeClick">
								<c:forEach var="item" items="${baseResList}">
										<li type="itemNode"  data-id="${item.resId}" data-pid="${item.upResId}" >${item.resName}</li>
								</c:forEach> 
                            </ul>                                                
                   </td>
                   <td>                   		
                        <label for="resName" class="control-label x85">资源名称:</label>                       
                        <input type="text" name="resName" id="resName" value="${baseRes.resName}"  maxlength= "100" size="15">                        
                   </td>
                   <td>                   		
                        <label for="resType" class="control-label x85">资源类型:</label>         
                        <s:select name="baseRes" codeType="sysResType"  inputName="resType" property="resType"></s:select>                         
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>  
                        <s:select name="baseRes" codeType="valid" inputName="valid" property="valid"></s:select>                                        
                   </td>
                </tr>
                <tr>
                   <td type="ResType" val ="M">                   		
                        <label for="target" class="control-label x85">资源指向:</label>                 
                        <s:select name="baseRes" codeType="resTarget" inputName="target" property="target"></s:select>                            
                   </td>
                   <td type="ResType" val="M">                   		
                        <label for="url" class="control-label x85">指向URL:</label>                       
                        <input type="text" name="url" id="url" value="${baseRes.url}"  maxlength= "500" size="15">                        
                   </td>
                   <td type="ResType" val="M">                   		
                        <label for="resIcon" class="control-label x85">图标:</label>                       
                        <input type="text" name="resIcon" id="resIcon" value="${baseRes.resIcon}"  maxlength= "100" size="15">                        
                   </td>
                   <td type="ResType" val="M">                   		
                        <label for="location" class="control-label x85">顺序号:</label>                       
                        <input type="text" name="location" id="location" value="${baseRes.location}" data-rule = "required"  size="15">                        
                   </td>
                   <td type="ResType" val="B">                   		
                        <label for="selector" class="control-label x85">选择器:</label>                       
                        <input type="text" name="selector" id="selector" value="${baseRes.selector}"  maxlength= "200" size="15">                        
                   </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<script type="text/javascript">
//选择事件
function S_NodeCheck(e, treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj(treeId),
        nodes = zTree.getCheckedNodes(true);
    var ids = '', names = ''
    
    for (var i = 0; i < nodes.length; i++) {
        ids   += ','+ nodes[i].id;
        names += ','+ nodes[i].name;
    }
    if (ids.length > 0) {
        ids = ids.substr(1), names = names.substr(1)
    }
    debugger;
    $("#upResId").val(ids);
    $("#upResIdVal").val(names);
}
//单击事件
function S_NodeClick(event, treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj(treeId);    
    zTree.checkNode(treeNode, !treeNode.checked, true, true);    
    event.preventDefault();
}
$(document).ready(function(){
	var resType = $("select[name=resType]").val();
	changeResType(resType);
	$("select[name=resType]").change(function (){
		var resType = $(this).val();
		changeResType(resType);
	});
});
//根据资源类型显示不同的信息项
function changeResType(resType){
	$("td[type=ResType]").each(
		function(){
			if($(this).attr("val")==resType){
				$(this).show();
			}else{
				$(this).hide();
			}
		}
	);
	
}
</script> 	
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
		<script type="text/javascript">
		$(function(){   
			readOnlyAll("${actionType}_BaseRes_form");
		}); 
		</script>
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>