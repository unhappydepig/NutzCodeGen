<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<script type="text/javascript">

</script> 	
<div class="bjui-pageContent">
    <form action="${base}/base/BaseConfigVal/${actionType}" id="edit_BaseConfigVal_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="configCode" class="control-label x85">配置信息项:</label>                      
                      	<input type="hidden" name="configCode" id="configCode"  value="${baseConfigVal.configCode}"  size="15"> 
                      	<input type="text" name="configName" id="configName" data-width="820" value='<s:trans name="baseConfigVal" property="configCode" codetype="baseConfig"/>'  data-toggle="lookup" data-url="${base}/common/CommonInfo/ConfigList" data-title="查询配置项" data-rule ="required"  maxlength="30"  size="15">                                                                     
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="hidden" name="comId" id="comId"  value="${baseConfigVal.comId}"  size="15"> 
                        <input type="text" name="comName" id="comName" data-width="820" value='<s:trans name="baseConfigVal" property="comId" codetype="company"/>'  data-toggle="lookup" data-url="${base}/common/CommonInfo/CompanyList" data-title="查询机构" data-rule ="required"  maxlength="30"  size="15">                                                                      
                   </td>
                   <td>                   		
                        <label for="configVal" class="control-label x85">配置值:</label>                       
                        <input type="text" name="configVal" id="configVal" value="${baseConfigVal.configVal}"     maxlength="50"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>       
                        <s:select name="baseConfigVal" codeType="valid" inputName="valid" property="valid"></s:select>                                                    
                   </td>
                </tr>
                <tr style="display: none;">
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy" value="${baseConfigVal.createBy}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime" value='<fmt:formatDate value="${baseConfigVal.createTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy" value="${baseConfigVal.updateBy}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime" value='<fmt:formatDate value="${baseConfigVal.updateTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                </tr>
                <tr>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>
    </ul>    
</div>
<script type="text/javascript">
$('#configCode').on('afterchange.bjui.lookup',function(){
	alert("123");
});
</script> 	