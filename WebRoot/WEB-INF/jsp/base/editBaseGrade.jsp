<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseGrade/${actionType}" id="edit_BaseGrade_form" data-toggle="validate" data-alertmsg="false" >   	
        <input type="hidden" name="createBy" id="createBy" value="${baseGrade.createBy}"     maxlength="30"  size="15">    
        <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${baseGrade.createTime}"  pattern="yyyy-MM-dd"/>'    data-rule ="date"  size="15">
        <input type="hidden" name="updateBy" id="updateBy" value="${baseGrade.updateBy}"     maxlength="30"  size="15">  
        <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${baseGrade.updateTime}"  pattern="yyyy-MM-dd"/>'    data-rule ="date"  size="15">
        <input type="hidden" name="gradeId" id="gradeId" value="${baseGrade.gradeId}"     maxlength="30"  size="15">     
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="gradeName" class="control-label x85">岗位名称:</label>                       
                        <input type="text" name="gradeName" id="gradeName" value="${baseGrade.gradeName}"     data-rule ="required"  maxlength="100"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构:</label>    
                        <input type="hidden" name="comId" id="comId"  value="${baseGrade.comId}"  size="15"> 
                        <input type="text" name="comName" id="comName" data-width="820" value='<s:trans name="baseGrade" property="comId" codetype="company"/>'  data-toggle="lookup" data-url="${base}/common/CommonInfo/CompanyList" data-title="查询机构" data-rule ="required"  maxlength="30"  size="15">                                                                                           
                   </td>
                   <td>                   		
                        <label for="location" class="control-label x85">顺序号:</label>                       
                        <input type="text" name="location" id="location" value="${baseGrade.location}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>                  
                        <s:select name="baseGrade" codeType="valid" inputName="valid" property="valid"></s:select>                             
                   </td>
                   <td colspan="2">                   		
                        <label for="valid" class="control-label x85">岗位权限:</label>
                        <input type="hidden" name="resCode" id="resCode" value="${baseGrade.resCode}">
                        <input type="text" name="resVal" id="resVal"  value='<s:trans name="baseGrade" property="resCode" codetype="baseRes"/>'  data-toggle="selectztree"  data-tree="#j_select_tree2" readonly  size="60">
                            <ul id="j_select_tree2" class="ztree hide" data-toggle="ztree" data-expand-all="true" data-check-enable="true" data-on-check="S_NodeCheck" data-on-click="S_NodeClick">
								<c:forEach var="item" items="${baseResList}">
										<li type="itemNode"  data-id="${item.resId}" data-pid="${item.upResId}" >${item.resName}</li>
								</c:forEach> 
                            </ul>                              
                   </td>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>
<script type="text/javascript">
//选择事件
function S_NodeCheck(e, treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj(treeId),
        nodes = zTree.getCheckedNodes(true);
    var ids = '', names = ''
    
    for (var i = 0; i < nodes.length; i++) {
        ids   += ','+ nodes[i].id;
        names += ','+ nodes[i].name;
    }
    if (ids.length > 0) {
        ids = ids.substr(1), names = names.substr(1)
    }
    debugger;
    $("#resCode").val(ids);
    $("#resVal").val(names);
}
//单击事件
function S_NodeClick(event, treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj(treeId);    
    zTree.checkNode(treeNode, !treeNode.checked, true, true);    
    event.preventDefault();
}
</script> 	