<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <div style="float:left; width:250px;">
	<ul id="ztree_com" class="ztree" data-toggle="ztree" data-expand-all="true" data-on-click="ZtreeClickCom">
	<c:forEach var="item" items="${baseCompanyList}">
		<li type="itemNode" data-id="${item.comId}" data-pid="${item.upComId}" data-url="${base}/base/BaseCompany/toedit?id=${item.PK}" data-divid="#itemDetailCom" data-tabid="table">${item.comName}</li>
	</c:forEach>                     
	</ul>
            <hr>
            <div  style="margin-top:5px;">
				<button type="button" class="btn-default btn-sm" data-toggle="refreshlayout" data-target="#itemDetailCom" data-id="add_baseCompany"  data-url="${base}/base/BaseCompany/toadd" data-title="新增部门">新增部门</button>                
            </div>
            <hr>                 
    </div>
    <div style="margin-left:255px; height:99.9%; overflow:hidden;">
        <div style="height:95%; overflow:hidden;">
            <fieldset style="height:100%;">
                <legend>详细信息</legend>
                <div id="itemDetailCom" style="height:94%; overflow:hidden;">
                </div>
            </fieldset>
        </div>
    </div>
</div>
<script type="text/javascript">
function ZtreeClickCom(event, treeId, treeNode) {
    if (treeNode.isParent) {
        var zTree = $.fn.zTree.getZTreeObj(treeId);        
        zTree.expandNode(treeNode);
    }
    $(event.target).bjuiajax('doLoad', {url:treeNode.url, target:treeNode.divid});
    event.preventDefault();
}
</script>
