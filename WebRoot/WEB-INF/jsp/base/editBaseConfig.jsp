<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseConfig/${actionType}" id="edit_BaseConfig_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="configCode" class="control-label x85">配置信息项:</label>                       
                        <input type="text" name="configCode" id="configCode" value="${baseConfig.configCode}"
                         <c:if test="${actionType=='view'}">readonly</c:if>
                        data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="configName" class="control-label x85">配置名称:</label>                       
                        <input type="text" name="configName" id="configName" value="${baseConfig.configName}"     maxlength="50"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="remark" class="control-label x85">配置说明:</label>                       
                        <input type="text" name="remark" id="remark" value="${baseConfig.remark}"     maxlength="100"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>     
                        <s:select name="baseConfig" codeType="valid" inputName="valid" property="valid"></s:select>                                            
                   </td>
                </tr>
                <tr style="display: none;">
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy" value="${baseConfig.createBy}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime" value='<fmt:formatDate value="${baseConfig.createTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy" value="${baseConfig.updateBy}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime" value='<fmt:formatDate value="${baseConfig.updateTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                </tr>
                <tr>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>