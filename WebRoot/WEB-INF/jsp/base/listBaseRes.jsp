<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <div style="float:left; width:250px;">
	<ul id="ztree_res" class="ztree" data-toggle="ztree" data-expand-all="true" data-on-click="ZtreeClickRes">
	<c:forEach var="item" items="${baseResList}">
		<li type="itemNode"  data-id="${item.resId}" data-pid="${item.upResId}" data-url="${base}/base/BaseRes/toedit?id=${item.PK}" data-divid="#itemDetailRes" data-tabid="table">${item.resName}</li>
	</c:forEach>                     
	</ul>
            <hr>
            <div  style="margin-top:5px;">
				<button type="button" class="btn-default btn-sm" data-toggle="refreshlayout" data-target="#itemDetailRes" data-id="add_BaseRes"  data-url="${base}/base/BaseRes/toadd" data-title="新增资源">新增资源</button>                
            </div>
            <hr>                 
    </div>
    <div style="margin-left:255px; height:99.9%; overflow:hidden;">
        <div style="height:95%; overflow:hidden;">
            <fieldset style="height:100%;">
                <legend>详细信息</legend>
                <div id="itemDetailRes" style="height:94%; overflow:hidden;">
                </div>
            </fieldset>
        </div>
    </div>
</div>
<script type="text/javascript">
function ZtreeClickRes(event, treeId, treeNode) {
    if (treeNode.isParent) {
        var zTree = $.fn.zTree.getZTreeObj(treeId);        
        zTree.expandNode(treeNode);
    }
    $(event.target).bjuiajax('doLoad', {url:treeNode.url, target:treeNode.divid});
    event.preventDefault();
}
</script>
