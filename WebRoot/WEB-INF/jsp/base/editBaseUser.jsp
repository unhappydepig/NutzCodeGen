<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseUser/${actionType}" id="edit_BaseUser_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
                        <%-- <label for="createTime" class="control-label x85">创建时间:</label>       --%>                    
                        <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${baseUser.createTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">        
                        <%-- <label for="createBy" class="control-label x85">创建人:</label>      --%>                     
                        <input type="hidden" name="createBy" id="createBy" value="${baseUser.createBy}"     maxlength="30"  size="15">    
        	            <%-- <label for="updateBy" class="control-label x85">更新人:</label>     --%>                      
                        <input type="hidden" name="updateBy" id="updateBy" value="${baseUser.updateBy}"     maxlength="30"  size="15">   
                        <%-- <label for="updateTime" class="control-label x85">更新时间:</label>  --%>                         
                        <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${baseUser.updateTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                        <%-- <label for="userId" class="control-label x85">员工ID:</label>    --%>                       
                        <input type="hidden" name="userId" id="userId" value="${baseUser.userId}"       maxlength="30"  size="15">          
                        <%-- <label for="sale" class="control-label x85">盐值:</label>       --%>                  
                        <input type="hidden" name="sale" id="sale" value="${baseUser.sale}"     maxlength="50"  size="15">  
            <tbody>
                <tr>
                   <td>                   		
                        <label for="userCode" class="control-label x85">员工代码:</label>                       
                        <input type="text" name="userCode" id="userCode" value="${baseUser.userCode}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="name" class="control-label x85">名称:</label>                       
                        <input type="text" name="name" id="name" value="${baseUser.name}"     data-rule ="required"  maxlength="50"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构:</label>    
                        <input type="hidden" name="com.comId" value="${baseUser.comId}">                 
                        <input type="text" name="com.comName" id="com.ComName" data-width="820" value='<s:trans name="baseUser" property="comId" codetype="company"/>' data-group="com" data-toggle="lookup" data-url="${base}/common/CommonInfo/CompanyList" data-title="查询机构" data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="deptId" class="control-label x85">部门:</label> 
                        <input type="hidden" name="dept.comId" value="${baseUser.deptId}">                 
                        <input type="text" name="dept.comName" id="dept.comName" data-width="820" value='<s:trans name="baseUser" property="deptId" codetype="company"/>' data-group="dept" data-toggle="lookup" data-url="${base}/common/CommonInfo/CompanyList" data-title="查询机构" data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="password" class="control-label x85">密码:</label> 
                        <input type="hidden" name="oldPassword" value="${baseUser.password}">                      
                        <input type="password" name="password" id="password" value="${baseUser.password}"     maxlength="50"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="userJob" class="control-label x85">员工职务:</label>                       
                        <input type="text" name="userJob" id="userJob" value="${baseUser.userJob}"     maxlength="50"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="sex" class="control-label x85">性别:</label>       
                        <s:select name="baseUser" codeType="sex" inputName="sex" property="sex"></s:select>                        
                   </td>
                   <td>                   		
                        <label for="birthday" class="control-label x85">生日:</label>                       
                        <input type="text" name="birthday" id="birthday" value='<fmt:formatDate value="${baseUser.birthday}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="mobile" class="control-label x85">手机:</label>                       
                        <input type="text" name="mobile" id="mobile" value="${baseUser.mobile}"     maxlength="50"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="mail" class="control-label x85">邮箱:</label>                       
                        <input type="text" name="mail" id="mail" value="${baseUser.mail}"  data-rule ="email"   maxlength="50"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="state" class="control-label x85">在职状态:</label>      
                        <s:select name="baseUser" codeType="userstate" inputName="state" property="state"></s:select>                                          
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>   
                        <s:select name="baseUser" codeType="valid" inputName="valid" property="valid"></s:select>                         
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="userPic" class="control-label x85">员工照片:</label>                       
                        <input type="text" name="userPic" id="userPic" value="${baseUser.userPic}"     maxlength="100"  size="15">                        
                   </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>