<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseGradeRes" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="gradeId" class="control-label x85">岗位ID:</label>                       
                        <input type="text" name="gradeId" id="gradeId" value="${baseGradeRes.gradeId}"  size="15">                        
                        <label for="resId" class="control-label x85">资源ID:</label>                       
                        <input type="text" name="resId" id="resId" value="${baseGradeRes.resId}"  size="15">                        
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy" value="${baseGradeRes.createBy}"  size="15">                        
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime" data-toggle="datepicker"  value='<fmt:formatDate value="${baseGradeRes.createTime}"  pattern="yyyy-MM-dd"></fmt:formatDate>' size="15">
                        <br/>
            <button type="submit" class="btn-default" data-icon="search">查询</button>         
	        <a href="${base}/base/BaseGradeRes/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseGradeRes" data-title="新增岗位资源表">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseGradeRes/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_GRADE_RES">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_GRADE_RES" data-toggle="icheck"></th>
				     <th >岗位ID</th>         
				     <th >资源ID</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseGradeResList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_GRADE_RES" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.gradeId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.resId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.createBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"></fmt:formatDate>
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseGradeRes/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseGradeRes/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseGradeRes" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑岗位资源表"></a>
	                    <a href="${base}/base/BaseGradeRes/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseGradeRes" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看岗位资源表"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
