<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseUserExt" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="userId" class="control-label x85">会员ID:</label>                       
                        <input type="text" name="userId" id="userId"  value="${baseUserExt.userId}"  size="15">                        
                        <label for="attriId" class="control-label x85">扩展属性ID2:</label>                       
                        <input type="text" name="attriId" id="attriId"  value="${baseUserExt.attriId}"  size="15">                        
                        <label for="attriVal" class="control-label x85">扩展属性值2:</label>                       
                        <input type="text" name="attriVal" id="attriVal"  value="${baseUserExt.attriVal}"  size="15">                        
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseUserExt/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseUserExt" data-title="新增员工扩展信息-t_base_user_ext">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseUserExt/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_USER_EXT">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_USER_EXT" data-toggle="icheck"></th>
				     <th >会员ID</th>         
				     <th >扩展属性ID2</th>         
				     <th >扩展属性值2</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseUserExtList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_USER_EXT" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.userId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.attriId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.attriVal}                        
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseUserExt/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseUserExt/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseUserExt" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑员工扩展信息-t_base_user_ext"></a>
	                    <a href="${base}/base/BaseUserExt/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseUserExt" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看员工扩展信息-t_base_user_ext"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
