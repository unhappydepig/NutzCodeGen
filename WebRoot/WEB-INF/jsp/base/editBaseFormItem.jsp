<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseFormItem/${actionType}" id="edit_BaseFormItem_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="formItemId" class="control-label x85">表单明细关联表主键:</label>                       
                        <input type="text" name="formItemId" id="formItemId" value="${baseFormItem.formItemId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="formId" class="control-label x85">表单:</label>                       
                        <input type="text" name="formId" id="formId" value="${baseFormItem.formId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId" value="${baseFormItem.comId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="userId" class="control-label x85">用户代码:</label>                       
                        <input type="text" name="userId" id="userId" value="${baseFormItem.userId}"     maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="attrCode" class="control-label x85">属性代码:</label>                       
                        <input type="text" name="attrCode" id="attrCode" value="${baseFormItem.attrCode}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="attrName" class="control-label x85">显示名称:</label>                       
                        <input type="text" name="attrName" id="attrName" value="${baseFormItem.attrName}"     data-rule ="required"  maxlength="100"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="dataType" class="control-label x85">数据类型:</label>                       
                        <input type="text" name="dataType" id="dataType" value="${baseFormItem.dataType}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="editFlag" class="control-label x85">是否可编辑:</label>                       
                        <input type="text" name="editFlag" id="editFlag" value="${baseFormItem.editFlag}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="showFlag" class="control-label x85">显示状态:</label>                       
                        <input type="text" name="showFlag" id="showFlag" value="${baseFormItem.showFlag}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="reqFlag" class="control-label x85">是否必填:</label>                       
                        <input type="text" name="reqFlag" id="reqFlag" value="${baseFormItem.reqFlag}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="location" class="control-label x85">顺序号:</label>                       
                        <input type="text" name="location" id="location" value="${baseFormItem.location}"     data-rule ="digits"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy" value="${baseFormItem.updateBy}"     maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime" value='<fmt:formatDate value="${baseFormItem.updateTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>