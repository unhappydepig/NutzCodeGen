<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseUserCompany" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="userId" class="control-label x85">员工:</label>                       
                        <input type="text" name="userId" id="userId"  value="${baseUserCompany.userId}"  size="15">                        
                        <label for="comId" class="control-label x85">机构:</label>                       
                        <input type="text" name="comId" id="comId"  value="${baseUserCompany.comId}"  size="15">                        
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseUserCompany/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseUserCompany" data-title="新增员工权限信息-t_base_user_company">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseUserCompany/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_USER_COMPANY">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_USER_COMPANY" data-toggle="icheck"></th>
				     <th >员工</th>         
				     <th >机构</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseUserCompanyList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_USER_COMPANY" data-toggle="icheck" value="${item.PK}"></td>
                   <td>          
                   		<s:trans name="item" property="userId" codetype="user"/> 
                   </td>                      
                   <td> 
                   		<s:trans name="item" property="comId" codetype="company"/>   
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseUserCompany/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseUserCompany/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseUserCompany" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑员工权限信息-t_base_user_company"></a>
	                    <a href="${base}/base/BaseUserCompany/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseUserCompany" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看员工权限信息-t_base_user_company"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
