<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseFormItem" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="formItemId" class="control-label x85">表单明细关联表主键:</label>                       
                        <input type="text" name="formItemId" id="formItemId"  value="${baseFormItem.formItemId}"  size="15">                        
                        <label for="formId" class="control-label x85">表单:</label>                       
                        <input type="text" name="formId" id="formId"  value="${baseFormItem.formId}"  size="15">                        
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId"  value="${baseFormItem.comId}"  size="15">                        
                        <label for="userId" class="control-label x85">用户代码:</label>                       
                        <input type="text" name="userId" id="userId"  value="${baseFormItem.userId}"  size="15">                        
                        <br/>
                        <label for="attrCode" class="control-label x85">属性代码:</label>                       
                        <input type="text" name="attrCode" id="attrCode"  value="${baseFormItem.attrCode}"  size="15">                        
                        <label for="attrName" class="control-label x85">显示名称:</label>                       
                        <input type="text" name="attrName" id="attrName"  value="${baseFormItem.attrName}"  size="15">                        
                        <label for="dataType" class="control-label x85">数据类型:</label>                       
                        <input type="text" name="dataType" id="dataType"  value="${baseFormItem.dataType}"  size="15">                        
                        <label for="editFlag" class="control-label x85">是否可编辑:</label>                       
                        <input type="text" name="editFlag" id="editFlag"  value="${baseFormItem.editFlag}"  size="15">                        
                        <br/>
                        <label for="showFlag" class="control-label x85">显示状态:</label>                       
                        <input type="text" name="showFlag" id="showFlag"  value="${baseFormItem.showFlag}"  size="15">                        
                        <label for="reqFlag" class="control-label x85">是否必填:</label>                       
                        <input type="text" name="reqFlag" id="reqFlag"  value="${baseFormItem.reqFlag}"  size="15">                        
                        <label for="location" class="control-label x85">顺序号:</label>                       
                        <input type="text" name="location" id="location"  value="${baseFormItem.location}"  size="15">                        
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy"  value="${baseFormItem.updateBy}"  size="15">                        
                        <br/>
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${baseFormItem.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseFormItem/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseFormItem" data-title="新增自定义表单-t_base_form_item">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseFormItem/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_FORM_ITEM">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_FORM_ITEM" data-toggle="icheck"></th>
				     <th >表单明细关联表主键</th>         
				     <th >表单</th>         
				     <th >机构代码</th>         
				     <th >用户代码</th>         
				     <th >属性代码</th>         
				     <th >显示名称</th>         
				     <th >数据类型</th>         
				     <th >是否可编辑</th>         
				     <th >显示状态</th>         
				     <th >是否必填</th>         
				     <th >顺序号</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseFormItemList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_FORM_ITEM" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.formItemId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.formId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.comId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.userId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.attrCode}                        
                   </td>                      
                   <td>                   		                   
                        ${item.attrName}                        
                   </td>                      
                   <td>                   		                   
                        ${item.dataType}                        
                   </td>                      
                   <td>                   		                   
                        ${item.editFlag}                        
                   </td>                      
                   <td>                   		                   
                        ${item.showFlag}                        
                   </td>                      
                   <td>                   		                   
                        ${item.reqFlag}                        
                   </td>                      
                   <td>                   		                   
                        ${item.location}                        
                   </td>                      
                   <td>                   		                   
                        ${item.updateBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseFormItem/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseFormItem/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseFormItem" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑自定义表单-t_base_form_item"></a>
	                    <a href="${base}/base/BaseFormItem/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseFormItem" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看自定义表单-t_base_form_item"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
