<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseTableExt" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="extId" class="control-label x85">扩展属性id:</label>                       
                        <input type="text" name="extId" id="extId"  value="${baseTableExt.extId}"  size="15">                        
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId"  value="${baseTableExt.comId}"  size="15">                        
                        <label for="tableCode" class="control-label x85">主表名:</label>                       
                        <input type="text" name="tableCode" id="tableCode"  value="${baseTableExt.tableCode}"  size="15">                        
                        <label for="extTableCode" class="control-label x85">扩展表名:</label>                       
                        <input type="text" name="extTableCode" id="extTableCode"  value="${baseTableExt.extTableCode}"  size="15">                        
                        <br/>
                        <label for="attrName" class="control-label x85">属性名称:</label>                       
                        <input type="text" name="attrName" id="attrName"  value="${baseTableExt.attrName}"  size="15">                        
                        <label for="dataType" class="control-label x85">数据类型:</label>                       
                        <input type="text" name="dataType" id="dataType"  value="${baseTableExt.dataType}"  size="15">                        
                        <label for="reqFlag" class="control-label x85">是否必填:</label>                       
                        <input type="text" name="reqFlag" id="reqFlag"  value="${baseTableExt.reqFlag}"  size="15">                        
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy"  value="${baseTableExt.createBy}"  size="15">                        
                        <br/>
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${baseTableExt.createTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy"  value="${baseTableExt.updateBy}"  size="15">                        
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${baseTableExt.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="valid" class="control-label x85">有效标示:</label>                       
                        <input type="text" name="valid" id="valid"  value="${baseTableExt.valid}"  size="15">                        
                        <br/>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseTableExt/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseTableExt" data-title="新增数据增强表t_base_table_ext">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseTableExt/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_TABLE_EXT">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_TABLE_EXT" data-toggle="icheck"></th>
				     <th >扩展属性id</th>         
				     <th >机构代码</th>         
				     <th >主表名</th>         
				     <th >扩展表名</th>         
				     <th >属性名称</th>         
				     <th >数据类型</th>         
				     <th >是否必填</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
				     <th >有效标示</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseTableExtList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_TABLE_EXT" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.extId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.comId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.tableCode}                        
                   </td>                      
                   <td>                   		                   
                        ${item.extTableCode}                        
                   </td>                      
                   <td>                   		                   
                        ${item.attrName}                        
                   </td>                      
                   <td>                   		                   
                        ${item.dataType}                        
                   </td>                      
                   <td>                   		                   
                        ${item.reqFlag}                        
                   </td>                      
                   <td>                   		                   
                        ${item.createBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.updateBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.valid}                        
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseTableExt/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseTableExt/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseTableExt" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑数据增强表t_base_table_ext"></a>
	                    <a href="${base}/base/BaseTableExt/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseTableExt" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看数据增强表t_base_table_ext"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
