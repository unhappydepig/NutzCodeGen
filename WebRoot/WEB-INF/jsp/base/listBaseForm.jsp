<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseForm" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">                     
                        <label for="formName" class="control-label x85">表单名称:</label>                       
                        <input type="text" name="formName" id="formName"  value="${baseForm.formName}"  size="15">                        
                        <label for="description" class="control-label x85">表单描述:</label>                       
                        <input type="text" name="description" id="description"  value="${baseForm.description}"  size="15">  
                        <label for="valid" class="control-label x85">有效标示:</label>                  
                        <s:select name="baseForm" withAll="true" codeType="valid" inputName="valid" property="valid"></s:select>  
                        <br/>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseForm/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseForm" data-title="新增自定义表单">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseForm/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK" data-toggle="icheck"></th>
				     <th >表单名称</th>         
				     <th >表单描述</th>          
				     <th >有效标示</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>        
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseFormList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.formName}                        
                   </td>                      
                   <td>                   		                   
                        ${item.description}                        
                   </td>                      
                   <td>      
                   		<s:trans name="item" property="valid" codetype="valid"/>    
                   </td>                      
                   <td> 
                   		<s:trans name="item" property="createBy" codetype="user"/> 
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td> 
                   		<s:trans name="item" property="updateBy" codetype="user"/> 
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseForm/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseForm/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseForm" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑自定义表单"></a>
	                  <a href="${base}/base/BaseForm/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseForm" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看自定义表单"></a>
	                  <a href="${base}/base/BaseForm/customForm?id=${item.PK}" class="btn btn-green"  data-toggle="navtab" data-id="customer_BaseForm" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑表单明细">自定义</a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
