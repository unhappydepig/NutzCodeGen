<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseUser/savegrade" id="grade_BaseUser_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
            <input type="hidden" name ="userId" value="${userId}"/>
        	<c:forEach var="item" items="${gradeList}" varStatus="status">
	            <tr data-id="${status.current}">
	               <td><input type="checkbox"  name="gradeId" data-toggle="icheck" value="${item.PK}" 
	               	<c:if test="${item.valid=='Y'}">checked</c:if>></td>
                   <td>                		                   
                         ${item.gradeName}
                   </td>  
	            </tr>        		
        	</c:forEach>          
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    </ul>
</div>