<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseTask" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="taskId" class="control-label x85">主键:</label>                       
                        <input type="text" name="taskId" id="taskId"  value="${baseTask.taskId}"  size="15">                        
                        <label for="taksName" class="control-label x85">名称:</label>                       
                        <input type="text" name="taksName" id="taksName"  value="${baseTask.taksName}"  size="15">                        
                        <label for="url" class="control-label x85">地址:</label>                       
                        <input type="text" name="url" id="url"  value="${baseTask.url}"  size="15">                        
                        <label for="target" class="control-label x85">指向地址:</label>                       
                        <input type="text" name="target" id="target"  value="${baseTask.target}"  size="15">                        
                        <br/>
                        <label for="location" class="control-label x85">顺序号:</label>                       
                        <input type="text" name="location" id="location"  value="${baseTask.location}"  size="15">                        
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy"  value="${baseTask.createBy}"  size="15">                        
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${baseTask.createTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy"  value="${baseTask.updateBy}"  size="15">                        
                        <br/>
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${baseTask.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="valid" class="control-label x85">有效标示:</label>                       
                        <input type="text" name="valid" id="valid"  value="${baseTask.valid}"  size="15">                        
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseTask/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseTask" data-title="新增功能代码-t_base_task">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/base/BaseTask/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_BASE_TASK">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_BASE_TASK" data-toggle="icheck"></th>
				     <th >主键</th>         
				     <th >名称</th>         
				     <th >地址</th>         
				     <th >指向地址</th>         
				     <th >顺序号</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
				     <th >有效标示</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${baseTaskList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_BASE_TASK" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.taskId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.taksName}                        
                   </td>                      
                   <td>                   		                   
                        ${item.url}                        
                   </td>                      
                   <td>                   		                   
                        ${item.target}                        
                   </td>                      
                   <td>                   		                   
                        ${item.location}                        
                   </td>                      
                   <td>                   		                   
                        ${item.createBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.updateBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.valid}                        
                   </td>                      
	                <td>
	                  <a href="${base}/base/BaseTask/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/base/BaseTask/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_BaseTask" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑功能代码-t_base_task"></a>
	                    <a href="${base}/base/BaseTask/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_BaseTask" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看功能代码-t_base_task"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
