<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseCompany/${actionType}" id="edit_BaseCompany_form" data-toggle="validate" data-alertmsg="false" > 
   	 	<input type="hidden" name="comId" id="comId" value="${baseCompany.comId}"      maxlength="30"  size="15">
        <input type="hidden" name="createBy" id="createBy" value="${baseCompany.createBy}"     maxlength="30"  size="15">    
        <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${baseCompany.createTime}"  pattern="yyyy-MM-dd"/>'    data-rule ="date"  size="15">
        <input type="hidden" name="updateBy" id="updateBy" value="${baseCompany.updateBy}"     maxlength="30"  size="15">  
        <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${baseCompany.updateTime}"  pattern="yyyy-MM-dd"/>'    data-rule ="date"  size="15">
                                                                   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="upComId" class="control-label x85">上级机构:</label>      
                        <input type="hidden" name="up.comId" value="${baseCompany.upComId}">                 
                        <input type="text" name="up.comName" id="upComName" data-width="820" value='<s:trans name="baseCompany" property="upComId" codetype="company"/>' data-group="up" data-toggle="lookup" data-url="${base}/common/CommonInfo/CompanyList" data-title="查询机构" data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="comName" class="control-label x85">机构名称:</label>                       
                        <input type="text" name="comName" id="comName" value="${baseCompany.comName}"  data-rule ="required"   maxlength="100"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="comType" class="control-label x85">机构类型:</label>
                        <s:select name="baseCompany" codeType="comType" inputName="comType" property="comType"></s:select>                                
                   </td>
                   <td>                   		
                        <label for="comTrade" class="control-label x85">所属行业:</label>     
                        <s:select name="baseCompany" codeType="comTrade" inputName="comTrade" property="comTrade"></s:select>                                  
                   </td>                   
                </tr>
                <tr>
                   <td>                   		
                        <label for="address" class="control-label x85">地址:</label>                       
                        <input type="text" name="address" id="address" value="${baseCompany.address}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="telephone" class="control-label x85">电话:</label>                       
                        <input type="text" name="telephone" id="telephone" value="${baseCompany.telephone}"     maxlength="50"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="mail" class="control-label x85">邮箱:</label>                       
                        <input type="text" name="mail" id="mail" value="${baseCompany.mail}"     maxlength="50" data-rule ="email"   size="15">                        
                   </td>
                   <td>                   		
                        <label for="location" class="control-label x85">顺序号:</label>                       
                        <input type="text" name="location" id="location" value="${baseCompany.location}"     data-rule ="required;digits"  size="15">                        
                   </td>                   
                </tr>
                <tr>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>       
                        <s:select name="baseCompany" codeType="valid" inputName="valid" property="valid"></s:select>                                      
                   </td>                   
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>