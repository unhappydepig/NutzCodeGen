<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/base/BaseCode" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">                    
                        <label for="codeCode" class="control-label x85">键:</label>                       
                        <input type="text" name="codeCode" id="codeCode"  value="${baseCode.codeCode}"  size="15">                        
                        <label for="codeVal" class="control-label x85">值:</label>                       
                        <input type="text" name="codeVal" id="codeVal"  value="${baseCode.codeVal}"  size="15">  
                        <label for="remark" class="control-label x85">备注:</label>                       
                        <input type="text" name="remark" id="remark"  value="${baseCode.remark}"  size="15">       
                        <label for="valid" class="control-label x85">有效标示:</label>     
                        <s:select name="baseCode" withAll="true" codeType="valid" inputName="valid" property="valid"></s:select>  
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/base/BaseCode/toadd?upCodeId=0000" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_BaseCode" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="新增基础代码表">新增 </a>            
        </div>
    </form>
</div>
<div class="bjui-pageContent">
<jsp:include page="/WEB-INF/jsp/base/listBaseCodeSub.jsp"></jsp:include>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>