<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>

<link href="${base}/common/plugin/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<script src="${base}/common/plugin/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript">
function do_open_layout_customerForm(event, treeId, treeNode) {
    if (treeNode.isParent) {
        var zTree = $.fn.zTree.getZTreeObj(treeId);
        zTree.expandNode(treeNode);
        return
    }
    $(event.target).bjuiajax('doLoad', {url:treeNode.url, target:treeNode.divid});
    event.preventDefault();
}
</script>
  <script>
  $(function() {
    debugger;
    $("#draggable").draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid"
    });
  });
  </script>
<div class="bjui-pageContent">
	    <div style="float:left; width:200px;">
        <ul id="layout-tree" class="ztree" data-toggle="ztree" data-expand-all="true" data-on-click="do_open_layout_customerForm">
            <li data-id="1" data-pid="0">排版组件</li>
            <li data-id="10" data-pid="1">插入一行</li>
            <li data-id="11" data-pid="1">插入一列</li>
            <li data-id="2" data-pid="0">表单组件</li>
            <li data-id="20" data-pid="2">普通输入域</li>
            <li data-id="21" data-pid="2">日期选择</li>
            <li data-id="22" data-pid="2">下拉列表</li>
            <li data-id="23" data-pid="2">点选列表</li>
            <li data-id="24" data-pid="2">文件上传</li>
        </ul>
    </div>
    <div style="margin-left:210px; height:99.9%; overflow:hidden;">
        <div style="height:100%; overflow:hidden;">
            <fieldset style="height:100%;">
                <legend>自定义表单</legend>
                <div id="layout-01" style="height:94%; overflow:hidden;">
<ul>
  <li id="draggable" class="ui-state-highlight">请拖拽我</li>
</ul>
 
<ul id="sortable">
  <li class="ui-state-default">Item 1</li>
  <li class="ui-state-default">Item 2</li>
  <li class="ui-state-default">Item 3</li>
  <li class="ui-state-default">Item 4</li>
  <li class="ui-state-default">Item 5</li>
</ul>                    
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class="bjui-pageFooter">
    <ul>
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    </ul>
</div>