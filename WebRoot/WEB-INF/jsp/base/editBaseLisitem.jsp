<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/base/BaseLisitem/${actionType}" id="edit_BaseLisitem_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="listItemId" class="control-label x85">关联表主键:</label>                       
                        <input type="text" name="listItemId" id="listItemId" value="${baseLisitem.listItemId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="listId" class="control-label x85">列表id:</label>                       
                        <input type="text" name="listId" id="listId" value="${baseLisitem.listId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId" value="${baseLisitem.comId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="userId" class="control-label x85">用户代码:</label>                       
                        <input type="text" name="userId" id="userId" value="${baseLisitem.userId}"     maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="attrCode" class="control-label x85">属性代码:</label>                       
                        <input type="text" name="attrCode" id="attrCode" value="${baseLisitem.attrCode}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="attrName" class="control-label x85">显示名称:</label>                       
                        <input type="text" name="attrName" id="attrName" value="${baseLisitem.attrName}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="format" class="control-label x85">格式化:</label>                       
                        <input type="text" name="format" id="format" value="${baseLisitem.format}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="orderFlag" class="control-label x85">是否可排序:</label>                       
                        <input type="text" name="orderFlag" id="orderFlag" value="${baseLisitem.orderFlag}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="editFlag" class="control-label x85">是否可编辑:</label>                       
                        <input type="text" name="editFlag" id="editFlag" value="${baseLisitem.editFlag}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="showFlag" class="control-label x85">显示状态:</label>                       
                        <input type="text" name="showFlag" id="showFlag" value="${baseLisitem.showFlag}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="reqFlag" class="control-label x85">是否必填:</label>                       
                        <input type="text" name="reqFlag" id="reqFlag" value="${baseLisitem.reqFlag}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="location" class="control-label x85">顺序号:</label>                       
                        <input type="text" name="location" id="location" value="${baseLisitem.location}"     data-rule ="required;digits"  size="15">                        
                   </td>
                </tr>
                <tr>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>