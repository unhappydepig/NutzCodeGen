<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/code/CodePlan/${actionType}" id="edit_CodePlan_form" data-toggle="validate" data-alertmsg="false" >
       	              <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${codePlan.createTime}"  pattern="yyyy-MM-dd"/>'  >
                     <input type="hidden" name="createBy" id="createBy" value="${codePlan.createBy}"     maxlength="30"  size="15">   
                     <input type="hidden" name="updateBy" id="updateBy" value="${codePlan.updateBy}"     maxlength="30"  size="15"> 
                     <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${codePlan.updateTime}"  pattern="yyyy-MM-dd"/>'>
					<input type="hidden" name="planId" id="planId" value="${codePlan.planId}">          	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="planName" class="control-label x85">方案名称:</label>                       
                        <input type="text" name="planName" id="planName" value="${codePlan.planName}"     data-rule ="required"  maxlength="500"  size="15">                        
                   </td>
                   <td colspan="2">                   		
                        <label for="dbMap" class="control-label x85">数据转换:</label>                       
                        <select data-toggle="selectpicker" multiple name="dbMap">
                        	<c:forEach items="${dbMapList}" var="item">
                        		<option value="${item.codeCode}" ${item.flag}>${item.codeName}</option>	
                        	</c:forEach>
                        </select>                      
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="genType" class="control-label x85">生成方式:</label>                    
                        <s:select name="codePlan" codeType="genType" inputName="genType" property="genType"/>                        
                   </td>                 
                   <td>                   		
                        <label for="shareFlag" class="control-label x85">是否共享:</label>                    
                        <s:select name="codePlan" codeType="valid" inputName="shareFlag" property="shareFlag"/>                        
                   </td>               
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标识:</label>                    
                        <s:select name="codePlan" codeType="valid" inputName="valid" property="valid"/>                        
                   </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>