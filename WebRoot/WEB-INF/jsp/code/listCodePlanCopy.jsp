<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/code/CodePlan/toCopy" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">                 
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;   
            </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_CODE_PLAN" data-toggle="icheck"></th>
				     <th >方案名称</th>
				     <th >生成方式</th>        
				     <th >是否共享</th>        
				     <th >有效标识</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>           
                	 <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${codePlanList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_CODE_PLAN" data-toggle="icheck" value="${item.PK}"></td>                    
                   <td>                   		                   
                        ${item.planName}                        
                   </td>                     
                   <td>                   
                        <s:trans name="item" property="genType" codetype="genType"/>                               
                   </td>                       
                   <td>                   
                        <s:trans name="item" property="shareFlag" codetype="valid"/>                               
                   </td>                        
                   <td>                   
                        <s:trans name="item" property="valid" codetype="valid"/>                               
                   </td>                      
                   <td>                        
                        <s:trans name="item" property="createBy" codetype="user"/>
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td> 
                        <s:trans name="item" property="updateBy" codetype="user"/>                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                  
	                <td>
	                  <a href="${base}/code/CodePlan/copy?id=${item.PK}" class="btn btn-green" data-icon="copy" data-toggle="doajax" data-confirm-msg="确定复制此方案?">方案</a>
	                  <br/>
	                  <a href="${base}/code/CodePlan/copy?id=${item.PK}" class="btn btn-green" data-icon="copy" data-toggle="doajax" data-confirm-msg="确定复制此方案?">数据模板</a>
	                  <br/>
	                  <a href="${base}/code/CodePlan/copy?id=${item.PK}" class="btn btn-green" data-icon="copy" data-toggle="doajax" data-confirm-msg="确定复制此方案?">文件模板</a>
	                  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
