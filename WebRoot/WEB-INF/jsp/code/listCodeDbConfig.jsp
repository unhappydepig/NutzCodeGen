<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/code/CodeDbConfig" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">                      
                        <label for="configName" class="control-label x85">配置名称:</label>                       
                        <input type="text" name="configName" id="configName"  value="${codeDbConfig.configName}"  size="15">                        
                        <label for="dbType" class="control-label x85">数据库类型:</label>                  
                        <s:select name="codeDbConfig" codeType="dbType" withNull="true" inputName="dbType" property="dbType"/>         
                        <label for="valid" class="control-label x85">有效标识:</label>                       
                        <s:select name="codeDbConfig" codeType="valid" withNull="true" inputName="valid" property="valid"/>  
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/code/CodeDbConfig/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_CodeDbConfig" data-title="新增数据库配置">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/code/CodeDbConfig/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_CODE_DB_CONFIG">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_CODE_DB_CONFIG" data-toggle="icheck"></th>
				     <th >配置名称</th>         
				     <th >数据库类型</th>         
				     <th >数据库驱动</th>         
				     <th >数据库地址</th>         
				     <th >用户名</th>         
				     <th >密码</th>         
				     <th >有效标识</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${codeDbConfigList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_CODE_DB_CONFIG" data-toggle="icheck" value="${item.PK}"></td>                    
                   <td>                   		                   
                        ${item.configName}                        
                   </td>                      
                   <td>                   		               
                        <s:trans name="item" property="dbType" codetype="dbType"/>                     
                   </td>                      
                   <td>                   		                   
                        ${item.dbDriver}                        
                   </td>                      
                   <td>                   		                   
                        ${item.dbAddress}                        
                   </td>                      
                   <td>                   		                   
                        ${item.dbUser}                        
                   </td>                      
                   <td>                   		                   
                        ${item.dbPass}                        
                   </td>                      
                   <td>                   
                        <s:trans name="item" property="valid" codetype="valid"/>                               
                   </td>                      
                   <td>                        
                        <s:trans name="item" property="createBy" codetype="user"/>
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td> 
                        <s:trans name="item" property="updateBy" codetype="user"/>                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
	                <td>
	                  <a href="${base}/code/CodeDbConfig/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/code/CodeDbConfig/test?id=${item.PK}" class="btn btn-green" data-icon="database" data-toggle="doajax">测</a>
	                  <a href="${base}/code/CodeDbConfig/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_CodeDbConfig" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑数据库配置"></a>
	                  <a href="${base}/code/CodeDbConfig/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_CodeDbConfig" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看数据库配置"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
