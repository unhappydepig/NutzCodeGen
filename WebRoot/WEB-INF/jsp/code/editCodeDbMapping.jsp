<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/code/CodeDbMapping/${actionType}" id="edit_CodeDbMapping_form" data-toggle="validate" data-alertmsg="false" >   
                     <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${codeDbMapping.createTime}"  pattern="yyyy-MM-dd"/>'  >
                     <input type="hidden" name="createBy" id="createBy" value="${codeDbMapping.createBy}"     maxlength="30"  size="15">   
                     <input type="hidden" name="updateBy" id="updateBy" value="${codeDbMapping.updateBy}"     maxlength="30"  size="15"> 
                     <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${codeDbMapping.updateTime}"  pattern="yyyy-MM-dd"/>'>
					<input type="hidden" name="dbMapId" id="dbMapId" value="${codeDbMapping.dbMapId}">        	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="mapTypeId" class="control-label x85">映射类型:</label>     
                        <s:select name="codeDbMapping" codeType="mapTypeId" inputName="mapTypeId" property="mapTypeId"/>                                         
                   </td>
                   <td>                   		
                        <label for="coverName" class="control-label x85">变量名:</label>                       
                        <input type="text" name="coverName" id="coverName" value="${codeDbMapping.coverName}"     maxlength="50"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="coverType" class="control-label x85">转换方式:</label>                       
                        <s:select name="codeDbMapping" codeType="coverType" inputName="coverType" property="coverType"/>                   
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="coverIn" class="control-label x85">入参:</label>
                        <textarea rows="5" cols="20" name="coverIn" id="coverIn" >${codeDbMapping.coverIn}</textarea>                                           
                   </td>
                   <td>                   		
                        <label for="coverVal" class="control-label x85">条件/关键字:</label>          
                        <textarea rows="5" cols="20" name="coverVal" id="coverVal" >${codeDbMapping.coverVal}</textarea>                                    
                   </td>
                   <td>                   		
                        <label for="coverOut" class="control-label x85">出参:</label>   
                        <textarea rows="5" cols="20" name="coverOut" id="coverOut" >${codeDbMapping.coverOut}</textarea>               
                   </td>
                </tr>                
                <tr>
                   <td>                   		
                        <label for="coverLevel" class="control-label x85">优先级:</label>    
                        <input type="text" name="coverLevel" id="coverLevel" value="${codeDbMapping.coverLevel}"   data-rule ="required,digits"  maxlength="5"  size="15">             
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标识:</label>    
                        <s:select name="codeDbMapping" codeType="valid" inputName="valid" property="valid"/>                                        
                   </td>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>