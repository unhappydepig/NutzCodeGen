<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/code/CodeMapType/${actionType}" id="edit_CodeMapType_form" data-toggle="validate" data-alertmsg="false" >  
       	              <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${codeMapType.createTime}"  pattern="yyyy-MM-dd"/>'  >
                     <input type="hidden" name="createBy" id="createBy" value="${codeMapType.createBy}"     maxlength="30"  size="15">   
                     <input type="hidden" name="updateBy" id="updateBy" value="${codeMapType.updateBy}"     maxlength="30"  size="15"> 
                     <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${codeMapType.updateTime}"  pattern="yyyy-MM-dd"/>'>
					<input type="hidden" name="mapTypeId" id="mapTypeId" value="${codeMapType.mapTypeId}"> 
					<input type="hidden" name="planId" id="planId" value="${codeMapType.planId}">  
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="dbType" class="control-label x85">数据库类型:</label>  
                        <s:select name="codeMapType" codeType="dbType" withNull="true" inputName="dbType" property="dbType"></s:select>                                        
                   </td>
                   <td>                   		
                        <label for="mapName" class="control-label x85">映射名称:</label>                       
                        <input type="text" name="mapName" id="mapName" value="${codeMapType.mapName}"  data-rule ="required"   maxlength="500"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="mapTarget" class="control-label x85">转换对象:</label>                       
                        <s:select name="codeMapType" codeType="mapTarget"  inputName="mapTarget" property="mapTarget"></s:select>                      
                   </td>

                   <td>                   		
                        <label for="valid" class="control-label x85">有效标识:</label>                       
                        <s:select name="codeMapType" codeType="valid"  inputName="valid" property="valid"></s:select>                         
                   </td>                   
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>