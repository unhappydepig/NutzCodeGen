<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/code/CodeMapType" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">                    
                        <input type="hidden" name="planId" id="planId"  value="${codeMapType.planId}"  size="15">      
                        <label for="dbType" class="control-label x85">数据库类型:</label>                
                        <s:select name="codeMapType" codeType="dbType" withAll="true" inputName="dbType" property="dbType"></s:select>                 
                        <label for="mapName" class="control-label x85">映射名称:</label>                       
                        <input type="text" name="mapName" id="mapName"  value="${codeMapType.mapName}"  size="15">                        
                        <label for="mapTarget" class="control-label x85">转换对象:</label>             
                        <s:select name="codeMapType" codeType="mapTarget" withAll="true" inputName="mapTarget" property="mapTarget"></s:select>   
                        <label for="valid" class="control-label x85">有效标识:</label>     
                        <s:select name="codeMapType" codeType="valid" withAll="true" inputName="valid" property="valid"></s:select>      
             <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/code/CodeMapType/toadd?planId=${codeMapType.planId}" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_CodeMapType" data-title="新增数据类型映射类型">新增 </a>            
	        <a href="${base}/code/CodeMapType/toCopy" class="btn btn-green" data-icon="copy" data-toggle="navtab" data-id="copy_CodeMapType" data-title="新增数据类型映射类型">复制模板</a>                        
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/code/CodeMapType/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_CODE_MAP_TYPE">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table  data-width="100%" data-nowrap="true" class="table table-bordered">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_CODE_MAP_TYPE" data-toggle="icheck"></th>
                	 <th >方案名称</th>
				     <th >数据库类型</th>         
				     <th >映射名称</th>         
				     <th >针对对象</th>         
				     <th >有效标识</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${codeMapTypeList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_CODE_MAP_TYPE" data-toggle="icheck" value="${item.PK}"></td>  
                   <td>                   		                   
                        <s:trans name="item" property="planId" codetype="codePlan"/>                        
                   </td>                                      
                   <td>             
                   		<s:trans name="item" property="dbType" codetype="dbType"/>  
                   </td>                      
                   <td>                   		                   
                        ${item.mapName}                        
                   </td>                      
                   <td> 
                   		<s:trans name="item" property="mapTarget" codetype="mapTarget"/> 
                   </td>                      
                   <td>                   		                   
                   		<s:trans name="item" property="valid" codetype="valid"/>
                   </td>                      
                   <td>                   		                   
                  		<s:trans name="item" property="createBy" codetype="user"/>
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                   		<s:trans name="item" property="updateBy" codetype="user"/>
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
	                <td>
	                  <a href="${base}/code/CodeMapType/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/code/CodeMapType/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_CodeMapType" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑数据类型映射类型"></a>
	                  <a href="${base}/code/CodeMapType/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_CodeMapType" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看数据类型映射类型"></a>
	                  <a  onclick="chageShow(this,'${item.PK}')" class="btn btn-blue" >展开</a>
	        		  <a href="${base}/code/CodeDbMapping/toadd?mapTypeId=${item.mapTypeId}" class="btn btn-blue" data-toggle="navtab" data-id="add_CodeDbMapping" data-title="新增数据类型映射关系">新增 </a>            
	                  
	                  <%--	                  <a href="${base}/code/CodeDbMapping?mapTypeId=${item.PK}" class="btn btn-green"  data-toggle="navtab" data-id="item_CodeMap" data-title="配置转换明细">列表</a>	                  	                  	   --%> 
	                </td>
	            </tr> 
	            <tr id ="${item.PK}" style="display: none;">
	            </tr>  	                   		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
<script type="text/javascript">
function chageShow(aLink,fileId) {
	var text = $(aLink).text().trim();
	debugger;
	if("展开"==text){
		$(aLink).text("收起");		
		$("#"+fileId).show();
		$(this).bjuiajax('refreshLayout', {url:'${base}/code/CodeMapType/subList?mapTypeId='+fileId,target:'#'+fileId});		
	}else if("收起"==text){
		$(aLink).text("展开");
		$("#"+fileId).hide();
	}
} 
</script> 	
