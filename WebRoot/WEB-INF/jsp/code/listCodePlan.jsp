<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/code/CodePlan" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">                 
                        <label for="planName" class="control-label x85">方案名称:</label>                       
                        <input type="text" name="planName" id="planName"  value="${codePlan.planName}"  size="15">                       
                        <label for="valid" class="control-label x85">有效标识:</label>                       
                        <s:select name="codePlan" codeType="valid" withNull="true" inputName="valid" property="valid"/>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/code/CodePlan/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_CodePlan" data-title="新增代码生成方案">新增 </a> 
	        <a href="${base}/code/CodePlan/toCopy" class="btn btn-green" data-icon="copy" data-toggle="navtab" data-id="copy_CodePlan" data-title="复制代码生成方案">复制 </a>            
	                   
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/code/CodePlan/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_CODE_PLAN">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_CODE_PLAN" data-toggle="icheck"></th>
				     <th >方案名称</th>
				     <th >生成方式</th>        
				     <th >是否共享</th>        
				     <th >有效标识</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
                	 <th width="80">方案明细</th>  
                	 <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${codePlanList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_CODE_PLAN" data-toggle="icheck" value="${item.PK}"></td>                    
                   <td>                   		                   
                        ${item.planName}                        
                   </td>                     
                   <td>                   
                        <s:trans name="item" property="genType" codetype="genType"/>                               
                   </td>                       
                   <td>                   
                        <s:trans name="item" property="shareFlag" codetype="valid"/>                               
                   </td>                        
                   <td>                   
                        <s:trans name="item" property="valid" codetype="valid"/>                               
                   </td>                      
                   <td>                        
                        <s:trans name="item" property="createBy" codetype="user"/>
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td> 
                        <s:trans name="item" property="updateBy" codetype="user"/>                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>
                   <td>
	                  <a href="${base}/code/CodeMapType?planId=${item.PK}" class="btn btn-green"  data-toggle="navtab" data-id="data_CodePlan" data-title="配置数据库模板">数据模版</a><br/>
	                  <a href="${base}/code/CodePlanItem?planId=${item.PK}" class="btn btn-green"  data-toggle="navtab" data-id="item_CodePlan" data-title="配置生成文件">文件列表</a>                   
                   </td>                     
	                <td>
	                  <a href="${base}/code/CodePlan/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/code/CodePlan/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_CodePlan" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑代码生成方案"></a>
	                  <a href="${base}/code/CodePlan/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_CodePlan" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看代码生成方案"></a>
	                  <br/>
	                  <a href="${base}/code/CodePlan/toGenerate?id=${item.PK}" class="btn btn-green"  data-toggle="navtab" data-id="codeGen_CodePlan" data-title="生成代码">生成代码</a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
