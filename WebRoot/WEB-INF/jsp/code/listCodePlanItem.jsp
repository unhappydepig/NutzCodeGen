<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/code/CodePlanItem" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">                     
                        <input type="hidden" name="planId" id="planId"  value="${codePlanItem.planId}"  size="15">                        
                        <label for="fileName" class="control-label x85">说明:</label>                       
                        <input type="text" name="name" id="name"  value="${codePlanItem.name}"  size="15">        
                        <label for="fileExt" class="control-label x85">后缀名:</label>                       
                        <input type="text" name="fileExt" id="fileExt"  value="${codePlanItem.fileExt}"  size="15">       
                        <label for="mapTarget" class="control-label x85">转换对象:</label>             
                        <s:select name="codePlanItem" codeType="mapTarget" withAll="true" inputName="mapTarget" property="mapTarget"></s:select>                      
                        <label for="valid" class="control-label x85">有效标识:</label>      
                        <s:select name="codePlanItem" codeType="valid" withNull="true" inputName="valid" property="valid"/> 
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/code/CodePlanItem/toadd?planId=${codePlanItem.planId}" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_CodePlanItem" data-title="新增方案明细">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/code/CodePlanItem/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_CODE_PLAN_ITEM">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_CODE_PLAN_ITEM" data-toggle="icheck"></th>
                	 <th >方案名称</th>
				     <th >说明</th> 
				     <th >后缀名</th>   
					 <th >文件名</th>				         
					 <th >文件路径</th>
				     <th >针对对象</th>        
				     <th >有效标识</th>        
				     <th >更新人</th>         
				     <th >更新时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${codePlanItemList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_CODE_PLAN_ITEM" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        <s:trans name="item" property="planId" codetype="codePlan"/>                        
                   </td>                      
                   <td>   		                   
                        ${item.name}                        
                   </td>                     
                   <td>                   		                   
                        ${item.fileExt}                        
                   </td>                         
                   <td>   		                   
                        ${item.fileName}                        
                   </td>                     
                   <td>                   		                   
                        ${item.filePath}                        
                   </td>                     
                   <td> 
                   		<s:trans name="item" property="mapTarget" codetype="mapTarget"/> 
                   </td>                      
                   <td>                   
                        <s:trans name="item" property="valid" codetype="valid"/>                               
                   </td>                       
                   <td> 
                        <s:trans name="item" property="updateBy" codetype="user"/>                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                  
	                <td>
	                  <a href="${base}/code/CodePlanItem/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/code/CodePlanItem/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_CodePlanItem" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑方案明细"></a>
	                    <a href="${base}/code/CodePlanItem/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_CodePlanItem" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看方案明细"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
