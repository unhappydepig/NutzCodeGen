<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/code/CodeDbMapping" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">                      
                        <label for="mapTypeId" class="control-label x85">映射类型:</label>        
                        <s:select name="codeDbMapping" codeType="mapTypeId" withAll="true" inputName="mapTypeId" property="mapTypeId"></s:select> 
                        <label for="coverName" class="control-label x85">变量名:</label>                       
                        <input type="text" name="coverName" id="coverName"  value="${codeDbMapping.coverName}"  size="15">                        
                        <label for="coverType" class="control-label x85">转换方式:</label>                   
                        <s:select name="codeDbMapping" codeType="coverType" withAll="true" inputName="coverType" property="coverType"></s:select>                      
                        <label for="valid" class="control-label x85">有效标识:</label> 
                        <s:select name="codeDbMapping" codeType="valid" withAll="true" inputName="valid" property="valid"></s:select>      
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/code/CodeDbMapping/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_CodeDbMapping" data-title="新增数据类型映射关系">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/code/CodeDbMapping/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_CODE_DB_MAPPING">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_CODE_DB_MAPPING" data-toggle="icheck"></th>
				     <th >映射类型</th>         
				     <th >变量名</th>         
				     <th >转换方式</th>         
				     <th >有效标识</th>          
				     <th >入参</th>         
				     <th >条件值</th>         
				     <th >出参</th>        
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${codeDbMappingList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_CODE_DB_MAPPING" data-toggle="icheck" value="${item.PK}"></td>                   
                   <td>                   	
                   		<s:trans name="item" property="mapTypeId" codetype="codeMapType"/>	 
                   </td>                      
                   <td>                   		                   
                        ${item.coverName}                        
                   </td>                      
                   <td>                   		             
                   		<s:trans name="item" property="coverType" codetype="coverType"/>	                  
                   </td>                       
                   <td>                   		                   
                        ${item.coverIn}                        
                   </td>                       
                   <td>                   		                   
                        ${item.coverVal}                        
                   </td>                       
                   <td>                   		                   
                        ${item.coverOut}                        
                   </td>                     
                   <td>                   		   
                   		<s:trans name="item" property="valid" codetype="valid"/>	              
                   </td>                      
                   <td>                   		 
                   		<s:trans name="item" property="createBy" codetype="user"/>	               
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   
                   		<s:trans name="item" property="updateBy" codetype="user"/>	                     
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
	                <td>
	                  <a href="${base}/code/CodeDbMapping/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/code/CodeDbMapping/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_CodeDbMapping" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑数据类型映射关系"></a>
	                    <a href="${base}/code/CodeDbMapping/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_CodeDbMapping" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看数据类型映射关系"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
