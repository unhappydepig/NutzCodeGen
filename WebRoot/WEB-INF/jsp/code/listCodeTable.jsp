<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
    <table  data-width="100%" data-nowrap="true" class="table table-bordered">
    	        <div class="bjui-searchBar">
                        <label class="control-label x85">名称:</label>                       
                        <input type="text"  value="${pdm.name}" disabled="disabled" size="15">   
                        <label  class="control-label x85">数据库类型:</label>         
                        <input type="text"  value="${pdm.DBMSCode}" disabled="disabled" size="15">                                 
        		</div>
        <thead>
            <tr>
                <th width="26"><input id="checkboxall" checktype="off" type="checkbox"  ></th>
				     <th >表名</th>         
				     <th >代码</th>         
				     <th >主键</th>       
                	<th width="150">操作</th>
            </tr>
        </thead>
        <tbody id="tableList">
        	<c:forEach var="item" items="${pdm.tables}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox"  name="table_id" value="${item.id}"></td>                    
                   <td>                   		                   
                        ${item.name}                        
                   </td>                      
                   <td>                   		               		                   
                        ${item.code}                     
                   </td>                      
                   <td>       
                   	    <c:forEach var="pkitem" items="${item.keys}">
                   	    	${pkitem.code}(                   	    	
	                   	    <c:forEach var="pkitemcol" items="${pkitem.columns}">
	                   	    	${pkitemcol.code} 
	                   	    </c:forEach>) 
                   	    </c:forEach>                           
                   </td>                    
	                <td>
	                  <a  onclick="chageShow(this,'${item.id}')" class="btn btn-blue" >展开</a>
	                </td>
	            </tr>    
	            <tr id ="${item.id}" style="display: none;">
	            	<td/>
	            	<td colspan="3">        		
    						<table data-width="100%" data-nowrap="true" class="table table-bordered">
    							        <thead>
				 				            <tr>
			  							     <th >代码</th>         
										     <th >名称</th>         
											 <th >说明</th>
											 <th >数据类型</th>      
										    </tr>
										</thead>										
								        <tbody>
        										<c:forEach var="column" items="${item.columns}">
        											<tr>
        												<td>${column.code}</td>
        												<td>${column.name}</td>
        												<td>${column.comment}</td>
        												<td>${column.dataType}</td>
        											</tr>
        										</c:forEach>
								        </tbody>
    						</table> 
	            	</td>
	            </tr>    		
        	</c:forEach>
        </tbody>
    </table>

<script type="text/javascript">
$("#checkboxall").click(function(){ 
	var checktype= $(this).attr("checktype");
	if("on"==checktype){
		$(this).attr("checktype","off");
		$("#tableList").find("input[name='table_id']").each(function(){
				var c=$(this)[0];
				c.checked=false;
			}
		); //.removeAttr("checked");//取消全选   
	}
	if("off"==checktype){
		$(this).attr("checktype","on");
		$("#tableList").find("input[name='table_id']").each(function(){
			var c=$(this)[0];
			c.checked=true;
		}
	); ////选中所有  
	}
}); 

function checkAll(file){
	var table = file;
	while(!$(table).is("table")){
		table = $(table).parent();
	}
	debugger;
	$(table).find("tbody").find("input[name='table_id']").attr("checked",'true');
}
function chageShow(aLink,fileId) {
	var text = $(aLink).text().trim();
	debugger;
	if("展开"==text){
		$(aLink).text("收起");
		$("#"+fileId).show();
	}else if("收起"==text){
		$(aLink).text("展开");
		$("#"+fileId).hide();
	}
} 
</script> 	