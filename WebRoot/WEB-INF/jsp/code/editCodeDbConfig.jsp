<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/code/CodeDbConfig/${actionType}" id="edit_CodeDbConfig_form" data-toggle="validate" data-alertmsg="false" > 
                     <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${codeDbConfig.createTime}"  pattern="yyyy-MM-dd"/>'  >
                     <input type="hidden" name="createBy" id="createBy" value="${codeDbConfig.createBy}"     maxlength="30"  size="15">   
                     <input type="hidden" name="updateBy" id="updateBy" value="${codeDbConfig.updateBy}"     maxlength="30"  size="15"> 
                     <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${codeDbConfig.updateTime}"  pattern="yyyy-MM-dd"/>'>
					<input type="hidden" name="dbConfigId" id="dbConfigId" value="${codeDbConfig.dbConfigId}">                  	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="configName" class="control-label x85">配置名称:</label>                       
                        <input type="text" name="configName" id="configName" value="${codeDbConfig.configName}"     maxlength="100"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="dbType" class="control-label x85">数据库类型:</label>                
                        <s:select name="codeDbConfig" codeType="dbType"  inputName="dbType" property="dbType"/>
                   </td>
                   <td>                   		
                        <label for="dbDriver" class="control-label x85">数据库驱动:</label>                       
                        <input type="text" name="dbDriver" id="dbDriver" value="${codeDbConfig.dbDriver}"     maxlength="100"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标识:</label>                       
                        <s:select name="codeDbConfig" codeType="valid" inputName="valid" property="valid"/>                        
                   </td>
                </tr>
                <tr>
                   <td colspan="2">                   		
                        <label for="dbAddress" class="control-label x85">数据库地址:</label>                       
                        <input type="text" name="dbAddress" id="dbAddress" value="${codeDbConfig.dbAddress}"     maxlength="100"  size="45">                        
                   </td>
                   <td>                   		
                        <label for="dbUser" class="control-label x85">用户名:</label>                       
                        <input type="text" name="dbUser" id="dbUser" value="${codeDbConfig.dbUser}"     maxlength="100"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="dbPass" class="control-label x85">密码:</label>                       
                        <input type="text" name="dbPass" id="dbPass" value="${codeDbConfig.dbPass}"     maxlength="100"  size="15">                        
                   </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>