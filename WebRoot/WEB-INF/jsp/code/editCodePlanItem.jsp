<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/code/CodePlanItem/${actionType}" id="edit_CodePlanItem_form" data-toggle="validate" data-alertmsg="false" >   
       	             <input type="hidden" name="createTime" id="createTime" value='<fmt:formatDate value="${codePlanItem.createTime}"  pattern="yyyy-MM-dd"/>'  >
                     <input type="hidden" name="createBy" id="createBy" value="${codePlanItem.createBy}"     maxlength="30"  size="15">   
                     <input type="hidden" name="updateBy" id="updateBy" value="${codePlanItem.updateBy}"     maxlength="30"  size="15"> 
                     <input type="hidden" name="updateTime" id="updateTime" value='<fmt:formatDate value="${codePlanItem.updateTime}"  pattern="yyyy-MM-dd"/>'>
					 <input type="hidden" name="planItemId" id="planItemId" value="${codePlanItem.planItemId}">
					 <input type="hidden" name="planId" id="planId" value="${codePlanItem.planId}">        	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="name" class="control-label x85">说明:</label>                       
                        <input type="text" name="name" id="name" value="${codePlanItem.name}"     maxlength="200"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="fileName" class="control-label x85">文件名称:</label>                       
                        <input type="text" name="fileName" id="fileName" value="${codePlanItem.fileName}"     maxlength="200"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="fileExt" class="control-label x85">后缀名:</label>                       
                        <input type="text" name="fileExt" id="fileExt" value="${codePlanItem.fileExt}"     maxlength="50"  size="15">                        
                   </td>                
                </tr>
                <tr>
                   <td>                   		
                        <label for="mapTarget" class="control-label x85">针对对象:</label>     
                        <s:select name="codePlanItem" codeType="mapTarget" inputName="mapTarget" property="mapTarget" />                                         
                   </td>                
                   <td>                   		
                        <label for="filePath" class="control-label x85">文件路径:</label>                       
                        <input type="text" name="filePath" id="filePath" value="${codePlanItem.filePath}"     maxlength="100"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标识:</label>         
                        <s:select name="codePlanItem" codeType="valid" inputName="valid" property="valid"/>                                   
                   </td>                   </tr>
                <tr>
                   <td colspan="5">                   		
                        <label for="fileContext" class="control-label x85">文件内容:</label>   
                        <textarea rows="10" cols="120" name="fileContext" data-toggle="autoheight" >${codePlanItem.fileContext}</textarea>                         
                   </td>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>