<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
	            	<td colspan="3"/>
	            	<td colspan="6">        		
    						<table data-width="100%" data-nowrap="true" class="table table-bordered">
    							        <thead>
				 				            <tr>       
										     <th >变量名</th>         
										     <th >转换方式</th>        
										     <th >优先级</th>        
										     <th >有效标识</th>          
										     <th >入参</th>         
										     <th >条件值</th>         
										     <th >出参</th>      
               								 <th width="150">操作</th>     
										    </tr>
										</thead>										
								        <tbody>
        										<c:forEach var="mapItem" items="${dbMappingList}">
        											<tr>
									                   <td>                   		                   
									                        ${mapItem.coverName}                        
									                   </td>                      
									                   <td>                   		             
									                   		<s:trans name="mapItem" property="coverType" codetype="coverType"/>	                  
									                   </td>                      
									                   <td>                   		                   
									                        ${mapItem.coverLevel}                        
									                   </td>                     
									                   <td>                   		   
									                   		<s:trans name="mapItem" property="valid" codetype="valid"/>	              
									                   </td>                      
									                   <td>                   		                   
									                        ${mapItem.coverIn}                        
									                   </td>                       
									                   <td>                   		                   
									                        ${mapItem.coverVal}                        
									                   </td>                       
									                   <td>                   		                   
									                        ${mapItem.coverOut}                        
									                   </td>
                     
										                <td>
										                  <a href="${base}/code/CodeDbMapping/delete?id=${mapItem.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
										                  <a href="${base}/code/CodeDbMapping/toedit?id=${mapItem.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_CodeDbMapping" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑数据类型映射关系"></a>
										                    <a href="${base}/code/CodeDbMapping/view?id=${mapItem.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_CodeDbMapping" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看数据类型映射关系"></a>	                  	  
										                </td>									                    
        											</tr>
        										</c:forEach>
								        </tbody>
    						</table> 
	            	</td>