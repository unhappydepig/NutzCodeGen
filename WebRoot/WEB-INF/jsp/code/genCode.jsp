<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/code/CodeGen" id="gen_CodePlan_form" data-toggle="validate" data-alertmsg="false" >
					<input type="hidden" name="planId" id="planId" value="${codePlan.planId}">
					<input type="hidden" name="genType" id="genType" value="${codePlan.genType}">          	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="planName" class="control-label x85">方案名称:</label>                       
                        ${codePlan.planName}                        
                   </td>
                   <td colspan="2">                   		
                        <label for="dbMap" class="control-label x85">数据模版:</label>                       
                        <select data-toggle="selectpicker" multiple name="dbMapList" >
                        	<c:forEach items="${dbMapList}" var="item">
                        		<option value="${item.codeCode}" ${item.flag}>${item.codeName}</option>	
                        	</c:forEach>
                        </select>                      
                   </td>
                </tr>
                <tr>
                   <td colspan="2">                   		
                        <label  class="control-label x85">生成文件:</label>                       
						<select data-toggle="selectpicker" multiple name="selectFile">
                        	<c:forEach items="${planItemList}" var="item">
                        		<option value="${item.planItemId}" selected="selected">${item.name}</option>	
                        	</c:forEach>
                        </select>                   
                   </td>         
                   <td>
                        <label for="encoding" class="control-label x85">编码格式:</label>                     
						<select data-toggle="selectpicker" name="encoding">
                        		<option value="UTF-8" selected="selected">UTF-8</option>
                        		<option value="GBK" >GBK</option>
                        </select>                   
                   </td>       	
                </tr>
                <tr>
                			<td align="center" colspan="3">
				   	        <button type="submit" class="btn-default btn-nm" >代码生成</button>
				   	        </td>
                </tr>
                <tr>
                <td colspan="3">
                        <label  class="control-label x85">生成方式:</label>                
                <div>
	                                <!-- Tabs -->
	                <ul class="nav nav-tabs" role="tablist" id="genTypeTab">
	                    <li class="active"><a href="#PDM" role="tab" data-toggle="tab" genType="PDM">PDM生成</a></li>
	                    <li><a href="#DB" role="tab" data-toggle="tab"  genType="DB">DB生成</a></li>
	                </ul>
	                <div class="tab-content">
	                    <div class="tab-pane fade active in" id="PDM">
	                       <div style="display: inline-block; vertical-align: middle;">
	                            <s:file successBack="uploadPDM" fileExt="*.pdm"/>
	                            <input id="fileName" type="text" readonly="readonly" name="fileName"/>
	                            <input id="pdmFileId" type="hidden" name="pdmFileId"/>
	                        </div>	                    		                    
	                    </div>
	                    <div class="tab-pane fade" id="DB">        
	                    <label  class="control-label">数据源:</label>          
						<select data-toggle="selectpicker" name="dbConFig" id="dbConFig">
                        		<c:forEach items="${dbConfigList}" var="dbConfig">
                        			<option value="${dbConfig.dbConfigId}" >${dbConfig.configName}</option>
                        		</c:forEach>
                        </select>                         
				   	    		<a class="btn btn-green" data-icon="database" onclick="testDB();">链接测试</a><br/>
                        		<label  class="control-label">关键字:</label>  <input id="table_key" type="text" name="table_key"/>
				   	    		<a class="btn btn-green"  onclick="getTableByDB();">获取表信息</a>                  
	                    </div>
	                </div>
                </div>
                </td>                
                </tr>
                <tr>
                <td colspan="3">
                <div id="tableList">
                </div>
                </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
    </ul>
</div>
<script type="text/javascript">
function uploadPDM(file, data) {
    var json = $.parseJSON(data);
    $("#fileName").val(json.filename);    
    $("#pdmFileId").val(json.fileid);
    $("#pdmFileId").bjuiajax('refreshLayout', {url:'${base}/code/CodeGen/getTable?pdmFileId='+json.fileid,target:'#tableList'});
    //debugger;
}
function testDB(){	
	var dbConFig = $("#dbConFig").find("option:selected").val();
	$("#pdmFileId").bjuiajax('doAjax', {url:'${base}/code/CodeDbConfig/test?id='+dbConFig});
}
function getTableByDB(){
	var table_key = $("#table_key").val();
	if(null==table_key||""==table_key){
		$("#table_key").alertmsg("warn", "请输入表名关键字!", {type:'warn'});
		return;
	}
	var dbConFig = $("#dbConFig").find("option:selected").val();
    $("#pdmFileId").bjuiajax('refreshLayout', {url:'${base}/code/CodeGen/getTable?dbConFig='+dbConFig+'&table_key='+table_key,target:'#tableList'});
}
function downFile(fileid){
  	window.open("${base}/common/file/down?fileId="+fileid,"_blank");  
}
$(document).ready(function(){ 
	var genType = $("#genType").val();
	$("#genTypeTab").find("a[genType="+genType+"]").trigger("click");
}); 
 $("#genTypeTab").find("a").click(function(){
 	$("#genType").val($(this).attr("genType"));
  });
</script> 	