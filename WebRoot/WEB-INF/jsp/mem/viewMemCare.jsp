<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div class="bjui-pageContent">
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="careId" class="control-label x85">关怀ID:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memCare.careId}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="memId" class="control-label x85">会员ID:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memCare.memId}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="careType" class="control-label x85">关怀渠道:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memCare.careType}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="careVal" class="control-label x85">渠道值mail-phone:</label>                       
                        <input type="text" readonly   maxlength="50"    value="${memCare.careVal}" size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="careTime" class="control-label x85">时间:</label>                       
                        <input type="text" readonly  data-toggle="datepicker"   data-rule ="date"   value='<fmt:formatDate value="${memCare.careTime}"  pattern="yyyy-MM-dd"/>' size="15">
                   </td>
                   <td>                   		
                        <label for="context" class="control-label x85">内容:</label>                       
                        <input type="text" readonly   maxlength="500"    value="${memCare.context}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="userId" class="control-label x85">员工:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memCare.userId}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memCare.comId}" size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="desc" class="control-label x85">描述:</label>                       
                        <input type="text" readonly   maxlength="500"    value="${memCare.desc}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memCare.createBy}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" readonly  data-toggle="datepicker"   data-rule ="date"   value='<fmt:formatDate value="${memCare.createTime}"  pattern="yyyy-MM-dd"/>' size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memCare.updateBy}" size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" readonly  data-toggle="datepicker"   data-rule ="date"   value='<fmt:formatDate value="${memCare.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
                   </td>
                </tr> 
            </tbody>
        </table>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
    </ul>
</div>