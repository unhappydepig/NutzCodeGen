<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/mem/MemExt/${actionType}" id="edit_MemExt_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="memId" class="control-label x85">会员ID:</label>                       
                        <input type="text" name="memId" id="memId" value="${memExt.memId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="attriId" class="control-label x85">扩展属性ID:</label>                       
                        <input type="text" name="attriId" id="attriId" value="${memExt.attriId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="attriVal" class="control-label x85">扩展属性值:</label>                       
                        <input type="text" name="attriVal" id="attriVal" value="${memExt.attriVal}"     maxlength="500"  size="15">                        
                   </td>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>