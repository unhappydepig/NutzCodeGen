<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div class="bjui-pageContent">
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="memId" class="control-label x85">会员ID:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memExt.memId}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="attriId" class="control-label x85">扩展属性ID:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memExt.attriId}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="attriVal" class="control-label x85">扩展属性值:</label>                       
                        <input type="text" readonly   maxlength="500"    value="${memExt.attriVal}" size="15">                        
                   </td>
                </tr> 
            </tbody>
        </table>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
    </ul>
</div>