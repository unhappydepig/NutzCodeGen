<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div class="bjui-pageContent">
    <form action="${ctx}/MemLevel/add" id="add_MemLevel_form" data-toggle="validate" data-alertmsg="false" >
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="memLevelId" class="control-label x85">等级代码:</label>
                        <input type="text" name="memLevelId" id="memLevelId" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>
                        <input type="text" name="comId" id="comId" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="levelCode" class="control-label x85">会员等级:</label>
                        <input type="text" name="levelCode" id="levelCode" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="name" class="control-label x85">等级名称:</label>
                        <input type="text" name="name" id="name" value=""   maxlength="100"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="discount" class="control-label x85">等级折扣:</label>
                        <input type="text" name="discount" id="discount" value=""   data-rule ="digits"  size="15">
                   </td>
                   <td>                   		
                        <label for="upContion" class="control-label x85">升级条件:</label>
                        <input type="text" name="upContion" id="upContion" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="upVar" class="control-label x85">条件数值:</label>
                        <input type="text" name="upVar" id="upVar" value=""   data-rule ="doubles"  maxlength="18"  size="15">
                   </td>
                   <td>                   		
                        <label for="desc" class="control-label x85">等级描述:</label>
                        <input type="text" name="desc" id="desc" value=""   maxlength="500"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>
                        <input type="text" name="createBy" id="createBy" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>
                        <input type="text" name="createTime" id="createTime" value=""  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>
                        <input type="text" name="updateBy" id="updateBy" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>
                        <input type="text" name="updateTime" id="updateTime" value=""  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>
                        <input type="text" name="valid" id="valid" value=""   maxlength="2"  size="15">
                   </td>
                </tr>                
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    </ul>
</div>