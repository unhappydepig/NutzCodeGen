<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div class="bjui-pageContent">
    <form action="${ctx}/MemMain/add" id="add_MemMain_form" data-toggle="validate" data-alertmsg="false" >
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="memId" class="control-label x85">会员ID:</label>
                        <input type="text" name="memId" id="memId" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="memCode" class="control-label x85">会员代码:</label>
                        <input type="text" name="memCode" id="memCode" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>
                        <input type="text" name="comId" id="comId" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="name" class="control-label x85">名称:</label>
                        <input type="text" name="name" id="name" value=""   maxlength="100"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="memLevelId" class="control-label x85">会员等级:</label>
                        <input type="text" name="memLevelId" id="memLevelId" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="sex" class="control-label x85">性别:</label>
                        <input type="text" name="sex" id="sex" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="birthday" class="control-label x85">生日:</label>
                        <input type="text" name="birthday" id="birthday" value=""  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="mobile" class="control-label x85">手机:</label>
                        <input type="text" name="mobile" id="mobile" value=""   maxlength="30"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="mail" class="control-label x85">邮箱:</label>
                        <input type="text" name="mail" id="mail" value=""   maxlength="50"  size="15">
                   </td>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>
                        <input type="text" name="createBy" id="createBy" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>
                        <input type="text" name="createTime" id="createTime" value=""  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>
                        <input type="text" name="updateBy" id="updateBy" value=""   maxlength="30"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>
                        <input type="text" name="updateTime" id="updateTime" value=""  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>
                        <input type="text" name="valid" id="valid" value=""   maxlength="2"  size="15">
                   </td>
                </tr>                
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    </ul>
</div>