<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div class="bjui-pageContent">
    <form action="${ctx}/MemCare/add" id="add_MemCare_form" data-toggle="validate" data-alertmsg="false" >
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="careId" class="control-label x85">关怀ID:</label>
                        <input type="text" name="careId" id="careId" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="memId" class="control-label x85">会员ID:</label>
                        <input type="text" name="memId" id="memId" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="careType" class="control-label x85">关怀渠道:</label>
                        <input type="text" name="careType" id="careType" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="careVal" class="control-label x85">渠道值mail-phone:</label>
                        <input type="text" name="careVal" id="careVal" value=""   maxlength="50"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="careTime" class="control-label x85">时间:</label>
                        <input type="text" name="careTime" id="careTime" value=""  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="context" class="control-label x85">内容:</label>
                        <input type="text" name="context" id="context" value=""   maxlength="500"  size="15">
                   </td>
                   <td>                   		
                        <label for="userId" class="control-label x85">员工:</label>
                        <input type="text" name="userId" id="userId" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>
                        <input type="text" name="comId" id="comId" value=""   maxlength="30"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="desc" class="control-label x85">描述:</label>
                        <input type="text" name="desc" id="desc" value=""   maxlength="500"  size="15">
                   </td>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>
                        <input type="text" name="createBy" id="createBy" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>
                        <input type="text" name="createTime" id="createTime" value=""  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>
                        <input type="text" name="updateBy" id="updateBy" value=""   maxlength="30"  size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>
                        <input type="text" name="updateTime" id="updateTime" value=""  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                </tr>                
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    </ul>
</div>