<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/mem/MemLevel" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="memLevelId" class="control-label x85">等级代码:</label>                       
                        <input type="text" name="memLevelId" id="memLevelId"  value="${memLevel.memLevelId}"  size="15">                        
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId"  value="${memLevel.comId}"  size="15">                        
                        <label for="levelCode" class="control-label x85">会员等级:</label>                       
                        <input type="text" name="levelCode" id="levelCode"  value="${memLevel.levelCode}"  size="15">                        
                        <label for="name" class="control-label x85">等级名称:</label>                       
                        <input type="text" name="name" id="name"  value="${memLevel.name}"  size="15">                        
                        <br/>
                        <label for="discount" class="control-label x85">等级折扣:</label>                       
                        <input type="text" name="discount" id="discount"  value="${memLevel.discount}"  size="15">                        
                        <label for="upContion" class="control-label x85">升级条件:</label>                       
                        <input type="text" name="upContion" id="upContion"  value="${memLevel.upContion}"  size="15">                        
                        <label for="upVar" class="control-label x85">条件数值:</label>                       
                        <input type="text" name="upVar" id="upVar"  value="${memLevel.upVar}"  size="15">                        
                        <label for="descr" class="control-label x85">等级描述:</label>                       
                        <input type="text" name="descr" id="descr"  value="${memLevel.descr}"  size="15">                        
                        <br/>
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy"  value="${memLevel.createBy}"  size="15">                        
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${memLevel.createTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy"  value="${memLevel.updateBy}"  size="15">                        
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${memLevel.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <br/>
                        <label for="valid" class="control-label x85">有效标示:</label>                       
                        <input type="text" name="valid" id="valid"  value="${memLevel.valid}"  size="15">                        
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/mem/MemLevel/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_MemLevel" data-title="新增会员等级信息表-t_mem_level">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/mem/MemLevel/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_MEM_LEVEL">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_MEM_LEVEL" data-toggle="icheck"></th>
				     <th >等级代码</th>         
				     <th >机构代码</th>         
				     <th >会员等级</th>         
				     <th >等级名称</th>         
				     <th >等级折扣</th>         
				     <th >升级条件</th>         
				     <th >条件数值</th>         
				     <th >等级描述</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
				     <th >有效标示</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${memLevelList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_MEM_LEVEL" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.memLevelId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.comId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.levelCode}                        
                   </td>                      
                   <td>                   		                   
                        ${item.name}                        
                   </td>                      
                   <td>                   		                   
                        ${item.discount}                        
                   </td>                      
                   <td>                   		                   
                        ${item.upContion}                        
                   </td>                      
                   <td>                   		                   
                        ${item.upVar}                        
                   </td>                      
                   <td>                   		                   
                        ${item.descr}                        
                   </td>                      
                   <td>                   		                   
                        ${item.createBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.updateBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.valid}                        
                   </td>                      
	                <td>
	                  <a href="${base}/mem/MemLevel/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/mem/MemLevel/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_MemLevel" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑会员等级信息表-t_mem_level"></a>
	                    <a href="${base}/mem/MemLevel/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_MemLevel" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看会员等级信息表-t_mem_level"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
