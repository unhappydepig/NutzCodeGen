<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/mem/MemCare/${actionType}" id="edit_MemCare_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="careId" class="control-label x85">关怀ID:</label>                       
                        <input type="text" name="careId" id="careId" value="${memCare.careId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="memId" class="control-label x85">会员ID:</label>                       
                        <input type="text" name="memId" id="memId" value="${memCare.memId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="careType" class="control-label x85">关怀渠道:</label>                       
                        <input type="text" name="careType" id="careType" value="${memCare.careType}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="careVal" class="control-label x85">渠道值mail-phone:</label>                       
                        <input type="text" name="careVal" id="careVal" value="${memCare.careVal}"     data-rule ="required"  maxlength="50"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="careTime" class="control-label x85">时间:</label>                       
                        <input type="text" name="careTime" id="careTime" value='<fmt:formatDate value="${memCare.careTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="required;date"  size="15">
                   </td>
                   <td>                   		
                        <label for="context" class="control-label x85">内容:</label>                       
                        <input type="text" name="context" id="context" value="${memCare.context}"     maxlength="500"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="userId" class="control-label x85">员工:</label>                       
                        <input type="text" name="userId" id="userId" value="${memCare.userId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId" value="${memCare.comId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="descr" class="control-label x85">描述:</label>                       
                        <input type="text" name="descr" id="descr" value="${memCare.descr}"     maxlength="500"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy" value="${memCare.createBy}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime" value='<fmt:formatDate value="${memCare.createTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy" value="${memCare.updateBy}"     maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime" value='<fmt:formatDate value="${memCare.updateTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>