<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div class="bjui-pageContent">
    <form action="${ctx}/MemExt/add" id="add_MemExt_form" data-toggle="validate" data-alertmsg="false" >
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="memId" class="control-label x85">会员ID:</label>
                        <input type="text" name="memId" id="memId" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="attriId" class="control-label x85">扩展属性ID:</label>
                        <input type="text" name="attriId" id="attriId" value=""   maxlength="30"  size="15">
                   </td>
                   <td>                   		
                        <label for="attriVal" class="control-label x85">扩展属性值:</label>
                        <input type="text" name="attriVal" id="attriVal" value=""   maxlength="500"  size="15">
                   </td>
                </tr>                
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    </ul>
</div>