<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageContent">
    <form action="${base}/mem/MemMain/${actionType}" id="edit_MemMain_form" data-toggle="validate" data-alertmsg="false" >   	
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="memId" class="control-label x85">会员ID:</label>                       
                        <input type="text" name="memId" id="memId" value="${memMain.memId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="memCode" class="control-label x85">会员代码:</label>                       
                        <input type="text" name="memCode" id="memCode" value="${memMain.memCode}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId" value="${memMain.comId}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="name" class="control-label x85">名称:</label>                       
                        <input type="text" name="name" id="name" value="${memMain.name}"     data-rule ="required"  maxlength="100"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="memLevelId" class="control-label x85">会员等级:</label>                       
                        <input type="text" name="memLevelId" id="memLevelId" value="${memMain.memLevelId}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="sex" class="control-label x85">性别:</label>                       
                        <input type="text" name="sex" id="sex" value="${memMain.sex}"     data-rule ="required"  maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="birthday" class="control-label x85">生日:</label>                       
                        <input type="text" name="birthday" id="birthday" value='<fmt:formatDate value="${memMain.birthday}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="mobile" class="control-label x85">手机:</label>                       
                        <input type="text" name="mobile" id="mobile" value="${memMain.mobile}"     maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="mail" class="control-label x85">邮箱:</label>                       
                        <input type="text" name="mail" id="mail" value="${memMain.mail}"     maxlength="50"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy" value="${memMain.createBy}"     maxlength="30"  size="15">                        
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime" value='<fmt:formatDate value="${memMain.createTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy" value="${memMain.updateBy}"     maxlength="30"  size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime" value='<fmt:formatDate value="${memMain.updateTime}"  pattern="yyyy-MM-dd"/>'  data-toggle="datepicker"   data-rule ="date"  size="15">
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>                       
                        <input type="text" name="valid" id="valid" value="${memMain.valid}"     data-rule ="required"  maxlength="2"  size="15">                        
                   </td>
                </tr> 
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
    	<c:if test="${actionType=='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
<script type="text/javascript">
$(function(){   
	readOnlyAll("${actionType}_MemMain_form");
}); 
</script> 	
    	</c:if>
    	<c:if test="${actionType!='view'}">
   	        <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
	        <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
    	</c:if>

    </ul>
</div>