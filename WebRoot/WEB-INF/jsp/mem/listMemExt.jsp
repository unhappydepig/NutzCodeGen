<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/mem/MemExt" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="memId" class="control-label x85">会员ID:</label>                       
                        <input type="text" name="memId" id="memId"  value="${memExt.memId}"  size="15">                        
                        <label for="attriId" class="control-label x85">扩展属性ID:</label>                       
                        <input type="text" name="attriId" id="attriId"  value="${memExt.attriId}"  size="15">                        
                        <label for="attriVal" class="control-label x85">扩展属性值:</label>                       
                        <input type="text" name="attriVal" id="attriVal"  value="${memExt.attriVal}"  size="15">                        
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/mem/MemExt/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_MemExt" data-title="新增会员扩展信息-t_mem_ext">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/mem/MemExt/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_MEM_EXT">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_MEM_EXT" data-toggle="icheck"></th>
				     <th >会员ID</th>         
				     <th >扩展属性ID</th>         
				     <th >扩展属性值</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${memExtList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_MEM_EXT" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.memId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.attriId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.attriVal}                        
                   </td>                      
	                <td>
	                  <a href="${base}/mem/MemExt/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/mem/MemExt/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_MemExt" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑会员扩展信息-t_mem_ext"></a>
	                    <a href="${base}/mem/MemExt/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_MemExt" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看会员扩展信息-t_mem_ext"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
