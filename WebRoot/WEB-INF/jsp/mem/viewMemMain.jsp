<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div class="bjui-pageContent">
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="memId" class="control-label x85">会员ID:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memMain.memId}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="memCode" class="control-label x85">会员代码:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memMain.memCode}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memMain.comId}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="name" class="control-label x85">名称:</label>                       
                        <input type="text" readonly   maxlength="100"    value="${memMain.name}" size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="memLevelId" class="control-label x85">会员等级:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memMain.memLevelId}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="sex" class="control-label x85">性别:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memMain.sex}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="birthday" class="control-label x85">生日:</label>                       
                        <input type="text" readonly  data-toggle="datepicker"   data-rule ="date"   value='<fmt:formatDate value="${memMain.birthday}"  pattern="yyyy-MM-dd"/>' size="15">
                   </td>
                   <td>                   		
                        <label for="mobile" class="control-label x85">手机:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memMain.mobile}" size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="mail" class="control-label x85">邮箱:</label>                       
                        <input type="text" readonly   maxlength="50"    value="${memMain.mail}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memMain.createBy}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" readonly  data-toggle="datepicker"   data-rule ="date"   value='<fmt:formatDate value="${memMain.createTime}"  pattern="yyyy-MM-dd"/>' size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memMain.updateBy}" size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" readonly  data-toggle="datepicker"   data-rule ="date"   value='<fmt:formatDate value="${memMain.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
                   </td>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>                       
                        <input type="text" readonly   maxlength="2"    value="${memMain.valid}" size="15">                        
                   </td>
                </tr> 
            </tbody>
        </table>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
    </ul>
</div>