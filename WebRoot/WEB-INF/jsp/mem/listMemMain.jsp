<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/mem/MemMain" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="memId" class="control-label x85">会员ID:</label>                       
                        <input type="text" name="memId" id="memId"  value="${memMain.memId}"  size="15">                        
                        <label for="memCode" class="control-label x85">会员代码:</label>                       
                        <input type="text" name="memCode" id="memCode"  value="${memMain.memCode}"  size="15">                        
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId"  value="${memMain.comId}"  size="15">                        
                        <label for="name" class="control-label x85">名称:</label>                       
                        <input type="text" name="name" id="name"  value="${memMain.name}"  size="15">                        
                        <br/>
                        <label for="memLevelId" class="control-label x85">会员等级:</label>                       
                        <input type="text" name="memLevelId" id="memLevelId"  value="${memMain.memLevelId}"  size="15">                        
                        <label for="sex" class="control-label x85">性别:</label>                       
                        <input type="text" name="sex" id="sex"  value="${memMain.sex}"  size="15">                        
                        <label for="birthday" class="control-label x85">生日:</label>                       
                        <input type="text" name="birthday" id="birthday"  data-toggle="datepicker"  value='<fmt:formatDate value="${memMain.birthday}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="mobile" class="control-label x85">手机:</label>                       
                        <input type="text" name="mobile" id="mobile"  value="${memMain.mobile}"  size="15">                        
                        <br/>
                        <label for="mail" class="control-label x85">邮箱:</label>                       
                        <input type="text" name="mail" id="mail"  value="${memMain.mail}"  size="15">                        
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy"  value="${memMain.createBy}"  size="15">                        
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${memMain.createTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy"  value="${memMain.updateBy}"  size="15">                        
                        <br/>
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${memMain.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="valid" class="control-label x85">有效标示:</label>                       
                        <input type="text" name="valid" id="valid"  value="${memMain.valid}"  size="15">                        
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/mem/MemMain/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_MemMain" data-title="新增会员信息表-t_mem_main">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/mem/MemMain/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_MEM_MAIN">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_MEM_MAIN" data-toggle="icheck"></th>
				     <th >会员ID</th>         
				     <th >会员代码</th>         
				     <th >机构代码</th>         
				     <th >名称</th>         
				     <th >会员等级</th>         
				     <th >性别</th>         
				     <th >生日</th>         
				     <th >手机</th>         
				     <th >邮箱</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
				     <th >有效标示</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${memMainList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_MEM_MAIN" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.memId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.memCode}                        
                   </td>                      
                   <td>                   		                   
                        ${item.comId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.name}                        
                   </td>                      
                   <td>                   		                   
                        ${item.memLevelId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.sex}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.birthday}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.mobile}                        
                   </td>                      
                   <td>                   		                   
                        ${item.mail}                        
                   </td>                      
                   <td>                   		                   
                        ${item.createBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.updateBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.valid}                        
                   </td>                      
	                <td>
	                  <a href="${base}/mem/MemMain/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/mem/MemMain/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_MemMain" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑会员信息表-t_mem_main"></a>
	                    <a href="${base}/mem/MemMain/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_MemMain" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看会员信息表-t_mem_main"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
