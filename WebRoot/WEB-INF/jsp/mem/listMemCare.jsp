<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${base}/mem/MemCare" method="post">
    <jsp:include page="/common/jsp/pageinput.jsp"></jsp:include>
        <div class="bjui-searchBar">
                        <label for="careId" class="control-label x85">关怀ID:</label>                       
                        <input type="text" name="careId" id="careId"  value="${memCare.careId}"  size="15">                        
                        <label for="memId" class="control-label x85">会员ID:</label>                       
                        <input type="text" name="memId" id="memId"  value="${memCare.memId}"  size="15">                        
                        <label for="careType" class="control-label x85">关怀渠道:</label>                       
                        <input type="text" name="careType" id="careType"  value="${memCare.careType}"  size="15">                        
                        <label for="careVal" class="control-label x85">渠道值mail-phone:</label>                       
                        <input type="text" name="careVal" id="careVal"  value="${memCare.careVal}"  size="15">                        
                        <br/>
                        <label for="careTime" class="control-label x85">时间:</label>                       
                        <input type="text" name="careTime" id="careTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${memCare.careTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="context" class="control-label x85">内容:</label>                       
                        <input type="text" name="context" id="context"  value="${memCare.context}"  size="15">                        
                        <label for="userId" class="control-label x85">员工:</label>                       
                        <input type="text" name="userId" id="userId"  value="${memCare.userId}"  size="15">                        
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" name="comId" id="comId"  value="${memCare.comId}"  size="15">                        
                        <br/>
                        <label for="descr" class="control-label x85">描述:</label>                       
                        <input type="text" name="descr" id="descr"  value="${memCare.descr}"  size="15">                        
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" name="createBy" id="createBy"  value="${memCare.createBy}"  size="15">                        
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" name="createTime" id="createTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${memCare.createTime}"  pattern="yyyy-MM-dd"/>' size="15">
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" name="updateBy" id="updateBy"  value="${memCare.updateBy}"  size="15">                        
                        <br/>
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" name="updateTime" id="updateTime"  data-toggle="datepicker"  value='<fmt:formatDate value="${memCare.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;        
	        <a href="${base}/mem/MemCare/toadd" class="btn btn-green" data-icon="plus" data-toggle="navtab" data-id="add_MemCare" data-title="新增会员关怀-t_mem_care">新增 </a>            
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">批量操作<span class="caret"></span></button>
                    <ul class="dropdown-menu right" role="menu">
                        <li><a href="${base}/mem/MemCare/deletes" data-toggle="doajaxchecked" data-confirm-msg="确定要删除所选项" data-idname="ids" data-group="PK_T_MEM_CARE">删除选中</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th width="26"><input type="checkbox" class="checkboxCtrl" data-group="PK_T_MEM_CARE" data-toggle="icheck"></th>
				     <th >关怀ID</th>         
				     <th >会员ID</th>         
				     <th >关怀渠道</th>         
				     <th >渠道值mail-phone</th>         
				     <th >时间</th>         
				     <th >内容</th>         
				     <th >员工</th>         
				     <th >机构代码</th>         
				     <th >描述</th>         
				     <th >创建人</th>         
				     <th >创建时间</th>         
				     <th >更新人</th>         
				     <th >更新时间</th>         
                <th width="150">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${memCareList}" varStatus="status">
	            <tr data-id="${status.current}">
	                <td><input type="checkbox" name="PK_T_MEM_CARE" data-toggle="icheck" value="${item.PK}"></td>
                   <td>                   		                   
                        ${item.careId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.memId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.careType}                        
                   </td>                      
                   <td>                   		                   
                        ${item.careVal}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.careTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.context}                        
                   </td>                      
                   <td>                   		                   
                        ${item.userId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.comId}                        
                   </td>                      
                   <td>                   		                   
                        ${item.descr}                        
                   </td>                      
                   <td>                   		                   
                        ${item.createBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.createTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
                   <td>                   		                   
                        ${item.updateBy}                        
                   </td>                      
                   <td>                   		                   
                        <fmt:formatDate value="${item.updateTime}"  pattern="yyyy-MM-dd"/>
                   </td>                      
	                <td>
	                  <a href="${base}/mem/MemCare/delete?id=${item.PK}" class="btn btn-red" data-icon="trash-o" data-toggle="doajax" data-confirm-msg="确定要删除该记录?"></a>
	                  <a href="${base}/mem/MemCare/toedit?id=${item.PK}" class="btn btn-green" data-icon="pencil" data-toggle="navtab" data-id="edit_MemCare" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="编辑会员关怀-t_mem_care"></a>
	                    <a href="${base}/mem/MemCare/view?id=${item.PK}" class="btn btn-green" data-icon="search-plus" data-toggle="navtab" data-id="view_MemCare" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续?" data-title="查看会员关怀-t_mem_care"></a>	                  	  
	                </td>
	            </tr>        		
        	</c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/jsp/pageinfo.jsp"></jsp:include>
