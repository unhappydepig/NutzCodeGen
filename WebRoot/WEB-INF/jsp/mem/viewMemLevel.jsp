<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div class="bjui-pageContent">
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                   <td>                   		
                        <label for="memLevelId" class="control-label x85">等级代码:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memLevel.memLevelId}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="comId" class="control-label x85">机构代码:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memLevel.comId}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="levelCode" class="control-label x85">会员等级:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memLevel.levelCode}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="name" class="control-label x85">等级名称:</label>                       
                        <input type="text" readonly   maxlength="100"    value="${memLevel.name}" size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="discount" class="control-label x85">等级折扣:</label>                       
                        <input type="text" readonly   data-rule ="digits"    value="${memLevel.discount}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="upContion" class="control-label x85">升级条件:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memLevel.upContion}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="upVar" class="control-label x85">条件数值:</label>                       
                        <input type="text" readonly   data-rule ="doubles"  maxlength="18"    value="${memLevel.upVar}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="desc" class="control-label x85">等级描述:</label>                       
                        <input type="text" readonly   maxlength="500"    value="${memLevel.desc}" size="15">                        
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="createBy" class="control-label x85">创建人:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memLevel.createBy}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="createTime" class="control-label x85">创建时间:</label>                       
                        <input type="text" readonly  data-toggle="datepicker"   data-rule ="date"   value='<fmt:formatDate value="${memLevel.createTime}"  pattern="yyyy-MM-dd"/>' size="15">
                   </td>
                   <td>                   		
                        <label for="updateBy" class="control-label x85">更新人:</label>                       
                        <input type="text" readonly   maxlength="30"    value="${memLevel.updateBy}" size="15">                        
                   </td>
                   <td>                   		
                        <label for="updateTime" class="control-label x85">更新时间:</label>                       
                        <input type="text" readonly  data-toggle="datepicker"   data-rule ="date"   value='<fmt:formatDate value="${memLevel.updateTime}"  pattern="yyyy-MM-dd"/>' size="15">
                   </td>
                </tr>
                <tr>
                   <td>                   		
                        <label for="valid" class="control-label x85">有效标示:</label>                       
                        <input type="text" readonly   maxlength="2"    value="${memLevel.valid}" size="15">                        
                   </td>
                </tr> 
            </tbody>
        </table>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
    </ul>
</div>