<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
     <div id="bjui-hnav">
        <button type="button" class="bjui-hnav-toggle btn-default" data-toggle="collapse" data-target="#bjui-hnav-navbar">
            <i class="fa fa-bars"></i>
        </button>
        <ul id="bjui-hnav-navbar" class="condensed">
        	<%--
            <li class="dropdown"><a href="javascript:;" data-toggle="slidebar"><i class="fa fa-table"></i> 销售管理</a>
            </li>
            <li class="dropdown"><a href="javascript:;" data-toggle="slidebar"><i class="fa fa-table"></i> 商品管理</a>
            </li>
            <li class="dropdown"><a href="javascript:;" data-toggle="slidebar"><i class="fa fa-user"></i> 员工管理</a>
            </li>
            <li class="userList"><a href="javascript:;" data-toggle="slidebar"><i class="fa fa-user"></i> 会员管理</a>
            </li>
            <li class="dropdown"><a href="javascript:;" data-toggle="slidebar"><i class="fa fa-coffee"></i> 薪酬管理</a>
            </li>
            <li class="dropdown"><a href="javascript:;" data-toggle="slidebar"><i class="fa fa-bug"></i> 考勤管理</a>
            </li>
            <li class="dropdown"><a href="javascript:;" data-toggle="slidebar"><i class="fa fa-cog"></i> 统计分析</a>
            </li>
             --%>
             
            <c:forEach items="${resList}" var="menu0">           	            	
            	<c:if test="${menu0.upResId eq '01'}">
            		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
            		<c:if test="${not empty  menu0.resIcon}">
            		<i class="${menu0.resIcon}"></i>
            		</c:if>  
            		 ${menu0.resName}<span class="caret"></span></a>
	                <ul class="dropdown-menu" role="menu">
	        			<c:forEach items="${resList}" var="menu1">
	        				<c:if test="${menu0.resId eq menu1.upResId}">
	                    		<li><a href="${base}${menu1.url}" data-toggle="${menu1.target}" data-id="${menu1.resId}"  data-title="${menu1.resName}">${menu1.resName}</a></li>
	        				</c:if>			
	        			</c:forEach>        	
	                </ul>            			            			
            	</c:if>
            </c:forEach>  
        </ul>
        <form class="hnav-form">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="搜索...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
    </div>