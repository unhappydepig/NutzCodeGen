<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/common/jsp/taglibs.jsp"%>
<div class="bjui-pageFooter">
    <div class="pages">
        <span>每页&nbsp;</span>
        <div class="selectPagesize">
            <select data-toggle="selectpicker" data-toggle-change="changepagesize">
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>                
                <option value="30">30</option>
            </select>
        </div>
        <span>&nbsp;条，共 ${baseCondition.recordCount} 条</span>
    </div>
    <div class="pagination-box" data-toggle="pagination" data-total='${baseCondition.recordCount}' data-page-size="${baseCondition.pageSize}" data-page-current="${baseCondition.pageNumber}">
    </div>
</div>